<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * AnnotateX Component Controller
 *
 * @since  0.0.1
 */
class AnnotateXController extends JControllerLegacy
{
	public function getreports($key = null, $urlVar = null)
	{
		if (!JSession::checkToken('get')) {
            echo new JResponseJson(null, JText::_('JINVALID_TOKEN'), true);
        } else {
			echo new JResponseJson(null, "hello, world!");
        }
	}
}