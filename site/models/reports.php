<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Reports API model
 *
 * @since  0.0.1
 */
class AnnotateXModelReports extends JModelItem
{
	/**
	 * Get the report with the specified id.
	 *
	 * @param  object  $user The user.
	 * @param  int     $page The page.
	 * @param  int     $size The size of a page
     *
	 * @return  object  The report.
	 */
	public function getReports($user, $collection, $search, $start, $size)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$user_id       = (int) $user->id;
		$collection_id = (int) $collection->id;
		$search        = $dbo->quote("%".$dbo->escape($search, true)."%", false);
		$limit         = (int) $size;
		$offset        = (int) $start;

		$dbo->setQuery("SELECT `ax_report`.`id` AS `id`
			            FROM `ax_report`
						-- Only the reports assigned to the user
						WHERE `ax_report`.`id` IN (
							SELECT `ax_report`.`id`
							FROM `ax_report`
							INNER JOIN `ax_collection_report`
							ON `ax_collection_report`.`report` = `ax_report`.`id`
							INNER JOIN `ax_assignment`
							ON `ax_assignment`.`collection` = `ax_collection_report`.`collection`
							WHERE `ax_assignment`.`collection` = ${collection_id})
						-- Search query filter
						AND `ax_report`.`id` IN (
							SELECT `ax_report`.`id`
							FROM `ax_report`
							LEFT OUTER JOIN (
								SELECT `ax_submission`.`report`           AS `report`,
								       `ax_submission`.`user`             AS `user`,
								       MAX(`ax_submission`.`submit_time`) AS `last_submit`
								FROM  `ax_submission`
								GROUP BY `ax_submission`.`report`, `ax_submission`.`user`
							) AS `submission`
							ON `submission`.`report` = `ax_report`.`id`
							AND `submission`.`user` = ${user_id}
							WHERE ( `ax_report`.`id` LIKE ${search}
							        OR `ax_report`.`content` LIKE ${search}
							        OR DATE_FORMAT(`submission`.`last_submit`, '%Y-%m-%d %T') LIKE ${search}
							        OR `ax_report`.`id` IN (
							            SELECT `ax_report`.`id`
							            FROM `ax_report`
							            INNER JOIN `ax_submission`
							            ON `ax_submission`.`report` = `ax_report`.`id`
							            INNER JOIN `ax_annotation`
							            ON `ax_annotation`.`submission` = `ax_submission`.`id`
							            INNER JOIN `ax_annotation_label`
							            ON `ax_annotation_label`.`annotation` = `ax_annotation`.`id`
							            INNER JOIN `ax_label`
							            ON `ax_label`.`id` = `ax_annotation_label`.`label`
							            WHERE `ax_submission`.`user` = ${user_id}
							            AND `ax_label`.`title` LIKE ${search})))
						ORDER BY `ax_report`.`id`
						LIMIT ${offset}, ${limit};");

		$report_ids = $dbo->loadColumn();

		if (count($report_ids) == 0) {
			return array();
		}

		for ($i = 0; $i < count($report_ids); $i++) {
			$report_ids[$i] = $dbo->quote($report_ids[$i]);
		}

		$report_ids = '('.implode($report_ids, ", ").')';

		$dbo->setQuery("SELECT `ax_report`.`id`                                                   AS `id`,
			                   GROUP_CONCAT(DISTINCT `ax_label`.`title` ORDER BY 1 SEPARATOR '#') AS `labels`
			            FROM `ax_report`
			            INNER JOIN `ax_submission`
			            ON `ax_submission`.`report` = `ax_report`.`id`
						AND `ax_submission`.`user` = ${user_id}
						INNER JOIN `ax_annotation`
						ON `ax_annotation`.`submission` = `ax_submission`.`id`
						INNER JOIN `ax_annotation_label`
						ON `ax_annotation_label`.`annotation` = `ax_annotation`.`id`
						INNER JOIN `ax_label`
						ON `ax_label`.`id` = `ax_annotation_label`.`label`
						WHERE `ax_report`.`id` IN ${report_ids}
						AND `ax_submission`.`submit_time` = (
							SELECT MAX(`ax_submission`.`submit_time`)
							FROM `ax_submission`
							WHERE `ax_submission`.`report` = `ax_report`.`id`
							AND `ax_submission`.`user` = ${user_id}
						)
						GROUP BY `ax_report`.`id`;");

		$labels = $dbo->loadAssocList('id');

        $dbo->setQuery("SELECT `ax_report`.`id`                    AS `id`,
        	                   `ax_report`.`content`               AS `text`,
								COUNT(`ax_submission`.`id`)        AS `n_submissions`,
								MAX(`ax_submission`.`submit_time`) AS `last_submission_date`
						FROM `ax_report`
						LEFT OUTER JOIN `ax_submission`
						ON `ax_submission`.`report` = `ax_report`.`id`
						AND `ax_submission`.`user` = ${user_id}
						WHERE `ax_report`.`id` IN ${report_ids}
						GROUP BY `ax_report`.`id`, `ax_report`.`content`
						ORDER BY `ax_report`.`id`;");

		$result = $dbo->loadObjectList();

		for ($i = 0; $i < count($result); $i++) {
			$result[$i]->labels = isset($labels[$result[$i]->id]) ? explode('#', $labels[$result[$i]->id]['labels']) : array();
		}

		return $result;
	}

	/**
	 * Get the count of the reports assigned to the specified user.
	 *
	 * @param  object  $user    The user.
	 * @param  string  $search  An optional filter
     *
	 * @return  object  The count of the reports assigned to the user.
	 */
	public function getReportCount($user, $collection, $search) {
		$dbo = $this->getDBO();

		// Escape query parameters
		$user_id       = (int) $user->id;
		$collection_id = (int) $collection->id;
		$search = $dbo->quote("%".$dbo->escape($search, true)."%", false);

		$dbo->setQuery("SELECT COUNT(*) AS `count`
						FROM `ax_report`
						-- Only the reports assigned to the user
						WHERE `ax_report`.`id` IN (
							SELECT `ax_report`.`id`
							FROM `ax_report`
							INNER JOIN `ax_collection_report`
							ON `ax_collection_report`.`report` = `ax_report`.`id`
							INNER JOIN `ax_assignment`
							ON `ax_assignment`.`collection` = `ax_collection_report`.`collection`
							WHERE `ax_assignment`.`collection` = ${collection_id})
						-- Search query filter
						AND `ax_report`.`id` IN (
							SELECT `ax_report`.`id`
							FROM `ax_report`
							LEFT OUTER JOIN (
								SELECT `ax_submission`.`report`           AS `report`,
								       `ax_submission`.`user`             AS `user`,
								       MAX(`ax_submission`.`submit_time`) AS `last_submit`
								FROM  `ax_submission`
								GROUP BY `ax_submission`.`report`, `ax_submission`.`user`
							) AS `submission`
							ON `submission`.`report` = `ax_report`.`id`
							AND `submission`.`user` = ${user_id}
							WHERE ( `ax_report`.`id` LIKE ${search}
							        OR `ax_report`.`content` LIKE ${search}
							        OR DATE_FORMAT(`submission`.`last_submit`, '%Y-%m-%d %T') LIKE ${search}
							        OR `ax_report`.`id` IN (
							            SELECT `ax_report`.`id`
							            FROM `ax_report`
							            INNER JOIN `ax_submission`
							            ON `ax_submission`.`report` = `ax_report`.`id`
							            INNER JOIN `ax_annotation`
							            ON `ax_annotation`.`submission` = `ax_submission`.`id`
							            INNER JOIN `ax_annotation_label`
							            ON `ax_annotation_label`.`annotation` = `ax_annotation`.`id`
							            INNER JOIN `ax_label`
							            ON `ax_label`.`id` = `ax_annotation_label`.`label`
							            WHERE `ax_submission`.`user` = ${user_id}
							            AND `ax_label`.`title` LIKE ${search})));");

		return $dbo->loadObject()->count;
	}

	public function setCurrentCollection($user, $collection_id)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$user_id       = (int) $user->id;
		$collection_id = (int) $collection_id;

		$dbo->setQuery("SELECT COUNT(*) AS `count`
			            FROM `ax_assignment`
			            WHERE `ax_assignment`.`user` = ${user_id}");
		$insert = $dbo->loadResult() == 0;

		if ($insert) {
			$dbo->setQuery("INSERT INTO `ax_user_option` (`user`, `collection`)
				            VALUES (${user_id}, ${collection_id});");
		} else {
			$dbo->setQuery("UPDATE `ax_user_option`
				            SET `ax_user_option`.`collection` = ${collection_id}
				            WHERE `ax_user_option`.`user` = ${user_id};");
		}

		$dbo->execute();
	}

	public function validateCurrentCollection($user, $collection_id)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$user_id       = (int) $user->id;
		$collection_id = (int) $collection_id;

		$dbo->setQuery("SELECT COUNT(*) AS `count`
			            FROM `ax_assignment`
			            WHERE `ax_assignment`.`user` = ${user_id}
			            AND `ax_assignment`.`collection` =  ${collection_id};");

		return $dbo->loadResult() == 1 ? TRUE : FALSE;
	}
}
