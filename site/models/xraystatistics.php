<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * X-Ray Statistics view model
 *
 * @since  0.0.1
 */
class AnnotateXModelXRayStatistics extends JModelItem
{
    public function getCollections()
    {
        $dbo = $this->getDBO();

        $dbo->setQuery("SELECT `id`          AS `id`,
                               `title`       AS `title`,
                               `description` AS `description`
                        FROM `ax_xray_collection`");

        return $dbo->loadObjectList();
    }

    /**
     * Get the report count for the specified users.
     *
     * @param  array  $users  The users.
     *
     * @return  array  The count of reports per user.
     */
    public function getSubmissionTimeStatistics($user_ids)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        for ($i = 0; $i < count($user_ids); $i++) {
            $user_ids[$i] = (int) $user_ids[$i];
        }

        $user_ids = implode(", ", $user_ids);

        $dbo->setQuery("SELECT `user`                                                                                           AS `user_id`,
                               MIN(TIMESTAMPDIFF(SECOND, `ax_xray_submission`.`load_time`, `ax_xray_submission`.`submit_time`)) AS `min`,
                               MAX(TIMESTAMPDIFF(SECOND, `ax_xray_submission`.`load_time`, `ax_xray_submission`.`submit_time`)) AS `max`,
                               AVG(TIMESTAMPDIFF(SECOND, `ax_xray_submission`.`load_time`, `ax_xray_submission`.`submit_time`)) AS `average`,
                               STD(TIMESTAMPDIFF(SECOND, `ax_xray_submission`.`load_time`, `ax_xray_submission`.`submit_time`)) AS `dev`
                        FROM `ax_xray_submission`
                        INNER JOIN `ax_labelled_xray`
                        ON `ax_labelled_xray`.`xray` = `ax_xray_submission`.`xray`
                        AND `ax_labelled_xray`.`collection` = `ax_xray_submission`.`collection`
                        WHERE `ax_xray_submission`.`user` IN (${user_ids})
                        GROUP BY `user`;");

        return $dbo->loadAssocList('user_id');
    }

    public function getSubmittedShapeCount($user_ids)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        for ($i = 0; $i < count($user_ids); $i++) {
            $user_ids[$i] = (int) $user_ids[$i];
        }

        $user_ids = implode(", ", $user_ids);

        $dbo->setQuery("SELECT `ax_xray_submission`.`user` AS `user_id`,
                               COUNT(*)                    AS `count`
                        FROM `ax_xray_submission`
                        INNER JOIN `ax_xray_annotation`
                        ON `ax_xray_submission`.`id` = `ax_xray_annotation`.`submission`
                        WHERE `ax_xray_submission`.`id` IN (
                            SELECT `id`
                            FROM `ax_xray_submission` AS `submission`
                            WHERE `submit_time` >= (
                                SELECT MAX(`submit_time`)
                                FROM `ax_xray_submission`
                                WHERE `xray` = `submission`.`xray`
                                AND `collection` = `submission`.`collection`
                                AND `user` = `submission`.`user`))
                        AND `ax_xray_submission`.`user` IN (${user_ids})
                        GROUP BY `user_id`;");

        return $dbo->loadAssocList('user_id');
    }

    public function getResubmittedXRayCount($user_ids)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        for ($i = 0; $i < count($user_ids); $i++) {
            $user_ids[$i] = (int) $user_ids[$i];
        }

        $user_ids = implode(", ", $user_ids);

        $dbo->setQuery("SELECT `user`   AS `user_id`,
                               COUNT(*) AS `count`
                        FROM `ax_xray_submission` AS `submissions`
                        WHERE `submit_time` < (
                            SELECT MAX(`submit_time`)
                            FROM `ax_xray_submission`
                            WHERE `xray` = `submissions`.`xray`
                            AND `collection` = `submissions`.`collection`
                            AND `user` = `submissions`.`user`)
                        GROUP BY `user`;");

        return $dbo->loadAssocList('user_id');
    }

    public function getEstimatedHours($user_ids)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        for ($i = 0; $i < count($user_ids); $i++) {
            $user_ids[$i] = (int) $user_ids[$i];
        }

        $user_ids = implode(", ", $user_ids);

        $dbo->setQuery("SELECT `sessions`.`user_id`                                                     AS `user_id`,
                               SUM(TIMESTAMPDIFF(SECOND, `sessions`.`login`, `sessions`.`last_action`)) AS `seconds`
                        FROM (
                            SELECT `ax_xray_submission`.`user`             AS `user_id`,
                                   `jmt_action_logs`.`log_date`            AS `login`,
                                   MAX(`ax_xray_submission`.`submit_time`) AS `last_action`
                            FROM `ax_xray_submission`
                            INNER JOIN `jmt_action_logs`
                            ON `jmt_action_logs`.`id` = `ax_xray_submission`.`login`
                            WHERE `ax_xray_submission`.`user` IN (${user_ids})
                            GROUP BY `ax_xray_submission`.`user`, `jmt_action_logs`.`log_date`)
                        AS `sessions`
                        GROUP BY `user_id`;");

        return $dbo->loadAssocList('user_id');
    }

    /**
     * Get the report count for the specified users.
     *
     * @param  array  $users  The users.
     *
     * @return  array  The count of reports per user.
     */
    public function getXRayCount($user_ids)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        for ($i = 0; $i < count($user_ids); $i++) {
            $user_ids[$i] = (int) $user_ids[$i];
        }

        $user_ids = implode(", ", $user_ids);

        $dbo->setQuery("SELECT `user` AS `user_id`,
                               COUNT(*)  AS `count`
                        FROM `ax_xray_assignment`
                        INNER JOIN `ax_xray_collection`
                        ON `ax_xray_collection`.`id` = `ax_xray_assignment`.`collection`
                        INNER JOIN `ax_labelled_xray`
                        ON `ax_labelled_xray`.`collection` = `ax_xray_collection`.`id`
                        WHERE `ax_xray_assignment`.`user` IN (${user_ids})
                        GROUP BY `user`;");

        return $dbo->loadAssocList('user_id');
    }

    public function getSessionCount($user_id)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $user_id = (int) $user_id;

        $dbo->setQuery("SELECT COUNT(DISTINCT `login`) AS `count`
                        FROM `ax_xray_submission`
                        WHERE `user` = ${user_id};");

        return $dbo->loadObject()->count;
    }

    public function getSubmissionsPerDayOfTheWeek($user_id)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $user_id = (int) $user_id;

        $dbo->setQuery("SELECT DAYNAME(submit_time) AS `day_of_week`,
                               COUNT(*)             AS `count`
                        FROM `ax_xray_submission`
                        WHERE `user` = ${user_id}
                        GROUP BY `day_of_week`;");

        $rows   = $dbo->loadAssocList();
        $result = array();

        for ($i = 0; $i < count($rows); $i++) {
            $result[strtolower($rows[$i]['day_of_week'])] = $rows[$i]['count'];
        }

        return $result;
    }

    public function getSubmissionsPerSession($user_id, $size = NULL, $offset = NULL)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $user_id = (int) $user_id;
        $size    = (int) $size;
        $offset  = (int) $offset;

        $dbo->setQuery("SELECT DATE_FORMAT(`sessions`.`login_action`, '%Y-%m-%d')                         AS `date`,
                               TIMESTAMPDIFF(SECOND, `sessions`.`login_action`, `sessions`.`last_action`) AS `duration`,
                               TIME(`sessions`.`login_action`)                                            AS `login_time`,
                               TIME(`sessions`.`last_action`)                                             AS `last_action_time`,
                               `sessions`.`submissions`                                                   AS `submissions`
                        FROM (
                            SELECT `jmt_action_logs`.`log_date`            AS `login_action`,
                                   MAX(`ax_xray_submission`.`submit_time`) AS `last_action`,
                                   COUNT(*)                                AS `submissions`
                            FROM `ax_xray_submission`
                            INNER JOIN `jmt_action_logs`
                            ON `ax_xray_submission`.`login` = `jmt_action_logs`.`id`
                            WHERE `ax_xray_submission`.`user` = ${user_id}
                            GROUP BY `jmt_action_logs`.`log_date`)
                        AS `sessions`
                        ORDER BY `date` DESC, `login_time` DESC".
                        ($size ? ' LIMIT '.($offset ? $offset.", ".$size : $size) : "").";");

        return $dbo->loadObjectList();
    }

    /**
     * Get the submitted report count for the specified users.
     *
     * @param  array  $users  The users.
     *
     * @return  array  The count of submitted reports per user.
     */
    public function getSubmittedXRayCount($user_ids)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        for ($i = 0; $i < count($user_ids); $i++) {
            $user_ids[$i] = (int) $user_ids[$i];
        }

        $user_ids = implode(", ", $user_ids);

        $dbo->setQuery("SELECT `user`                                AS `user_id`,
                                COUNT(DISTINCT `xray`, `collection`) AS `count`
                        FROM `ax_xray_submission`
                        WHERE `user` IN (${user_ids})
                        GROUP BY `user`;");

        return $dbo->loadAssocList('user_id');
    }
}
