<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * AnnotateReport view model
 *
 * @since  0.0.1
 */
class AnnotateXModelAnnotateReports extends JModelItem
{
    public function getCollections($user)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $user_id = (int) $user->id;

        $dbo->setQuery("SELECT `ax_collection`.`id`          AS `id`,
                               `ax_collection`.`title`       AS `title`,
                               `ax_collection`.`description` AS `description`
                        FROM `ax_collection`
                        INNER JOIN `ax_assignment`
                        ON `ax_assignment`.`collection` = `ax_collection`.`id`
                        AND `ax_assignment`.`user` = ${user_id}");

        return $dbo->loadObjectList();
    }

    public function getExpectedSentenceLabels($user, $sentences)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $normalized_sentences = array();

        for ($i = 0; $i < count($sentences); $i++) {
            array_push($normalized_sentences, $dbo->quote(strtolower(trim(str_replace('.', '', $sentences[$i])))));
        }

        $sentence_list = implode(', ', $normalized_sentences);
        $user_id       = (int) $user->id;

        $dbo->setQuery("SELECT DISTINCT `sentences`.`normalized_sentence` AS `normalized_sentence`,
                                        `label_count`.`label`             AS `label`
                        FROM (
                        	SELECT DISTINCT `normalized_sentence`
                            FROM `ax_submission`
                        	INNER JOIN `ax_annotation`
                            ON `ax_submission`.`id` = `ax_annotation`.`submission`
                        	WHERE `ax_annotation`.`normalized_sentence` IN (${sentence_list})
                            AND `ax_submission`.`user` = ${user_id})
                        AS `sentences`
                        INNER JOIN (
                        	SELECT `ax_annotation`.`normalized_sentence` AS `normalized_sentence`,
                        	       COUNT(*)                              AS `occurences`
                            FROM `ax_submission`
                            INNER JOIN `ax_annotation`
                            ON `ax_submission`.`id` = `ax_annotation`.`submission`
                        	WHERE `ax_annotation`.`normalized_sentence` IN (${sentence_list})
                            AND `ax_submission`.`user` = ${user_id}
                        	GROUP BY `ax_annotation`.`normalized_sentence`)
                        AS `total_count`
                        ON `sentences`.`normalized_sentence` = `total_count`.`normalized_sentence`
                        INNER JOIN (
                        	SELECT `ax_annotation`.`normalized_sentence` AS `normalized_sentence`,
                        	       `ax_annotation_label`.`label`         AS `label`,
                        	       COUNT(*)                              AS `occurences`
                            FROM `ax_submission`
                            INNER JOIN `ax_annotation`
                            ON `ax_submission`.`id` = `ax_annotation`.`submission`
                        	INNER JOIN `ax_annotation_label`
                        	ON `ax_annotation`.`id` = `ax_annotation_label`.`annotation`
                        	WHERE `ax_annotation`.`normalized_sentence` IN (${sentence_list})
                            AND `ax_submission`.`user` = ${user_id}
                        	GROUP BY `ax_annotation`.`normalized_sentence`,
                        	         `ax_annotation_label`.`label`)
                        AS `label_count`
                        ON `sentences`.`normalized_sentence` = `label_count`.`normalized_sentence`
                        WHERE `label_count`.`occurences` / `total_count`.`occurences` >= 0.8;");

        $confidences = $dbo->loadObjectList();
        $map = array();

        for ($i = 0; $i < count($sentences); $i++) {
            for ($j = 0; $j < count($confidences); $j++) {
                $sentence            = $sentences[$i];
                $normalized_sentence = strtolower(trim(str_replace('.', '', $sentences[$i])));

                if ($normalized_sentence == $confidences[$j]->normalized_sentence) {
                    if (!isset($map[$sentence])) {
                        $map[$sentence] = array();
                    }

                    // The same sentence could appear twince inside the report
                    if (!in_array($confidences[$j]->label, $map[$sentence])) {
                        array_push($map[$sentence], $confidences[$j]->label);
                    }
                }
            }
        }

        $result = array();

        foreach ($map as $key => $value) {
            array_push($result, array($key, $value));
        }

        return $result;
    }

    public function getCurrentCollection($user)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $user_id = (int) $user->id;

        $dbo->setQuery("SELECT `ax_collection`.`id`          AS `id`,
                               `ax_collection`.`title`       AS `title`,
                               `ax_collection`.`description` AS `description`
                        FROM `ax_collection`
                        INNER JOIN `ax_assignment`
                        ON `ax_assignment`.`collection` = `ax_collection`.`id`
                        INNER JOIN `ax_user_option`
                        ON `ax_user_option`.`collection` = `ax_assignment`.`collection`
                        AND `ax_user_option`.`user` = `ax_assignment`.`user`
                        WHERE `ax_user_option`.`user` = ${user_id}");

        $row = $dbo->loadObject();

        if (empty($row)) {
            $dbo->setQuery("SELECT `ax_collection`.`id`          AS `id`,
                                   `ax_collection`.`title`       AS `title`,
                                   `ax_collection`.`description` AS `description`
                            FROM `ax_collection`
                            INNER JOIN `ax_assignment`
                            ON `ax_assignment`.`collection` = `ax_collection`.`id`
                            WHERE `ax_assignment`.`user` = ${user_id}
                            ORDER BY `ax_collection`.`title`
                            LIMIT 1;");

            $row = $dbo->loadObject();
        }

        return $row;
    }

    /**
     * Return the list of labels by collection.
     *
     * @return  array  The list of labels.
     */
    public function getLabels() {
        $dbo = $this->getDBO();

        $dbo->setQuery("SELECT `id`          AS `id`,
                               `title`       AS `title`,
                               `description` AS `description`,
                               `assortment`  AS `assortment`,
                               `ordering`    AS `ordering`
                        FROM `ax_label`
                        ORDER BY `assortment`, `ordering`;");

        $rows   = $dbo->loadObjectList();
        $result = array();

        for ($i = 0; $i < count($rows); $i++) {
            if (empty($result[$rows[$i]->assortment])) {
                $result[$rows[$i]->assortment] = array();
            }

            array_push($result[$rows[$i]->assortment], $rows[$i]);
        }

        return $result;
    }

    /**
     * Get the latest submission of the specified report, for the specified user.
     *
     * @param  string  $report_id  The id of the report.
     * @param  object  $user       The user.
     *
     * @return  object  The submission.
     */
    public function getLatestSubmission($report_id, $user)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $report_id = $dbo->quote($report_id);
        $user_id   = (int) $user->id;

        $dbo->setQuery("SELECT `ax_submission`.`id`          AS `id`,
                               `ax_submission`.`user`        AS `user_id`,
                               `ax_submission`.`report`      AS `report_id`,
                               `ax_submission`.`load_time`   AS `load_time`,
                               `ax_submission`.`submit_time` AS `submit_time`
                        FROM `ax_submission`
                        WHERE `ax_submission`.`user` = ${user_id}
                        AND `ax_submission`.`report` = ${report_id}
                        AND `ax_submission`.`submit_time` = (
                            SELECT MAX(`submit_time`)
                            FROM `ax_submission`
                            WHERE `user` = ${user_id}
                            AND `report` = ${report_id}
                        );");

        $row = $dbo->loadObject();

        if (empty($row)) {
            return NULL;
        }

        $submission                   = new stdClass();
        $submission->id               = (int) $row->id;
        $submission->user_id          = (int) $row->user_id;
        $submission->report_id        = $row->report_id;
        $submission->load_timestamp   = $row->load_time;
        $submission->submit_timestamp = $row->submit_time;
        $submission->annotation       = array();

        $submission_id = $submission->id;

        $dbo->setQuery("SELECT `ax_annotation`.`sentence`                                AS `sentence`,
                               GROUP_CONCAT(`ax_annotation_label`.`label` SEPARATOR '#') AS `labels`
                        FROM `ax_annotation`
                        INNER JOIN `ax_annotation_label`
                        ON `ax_annotation_label`.`annotation` = `ax_annotation`.`id`
                        WHERE `ax_annotation`.`submission` = ${submission_id}
                        GROUP BY `ax_annotation`.`sentence`");

        $rows = $dbo->loadObjectList();

        for ($i = 0; $i < count($rows); $i++) {
            array_push($submission->annotation, array($rows[$i]->sentence, explode('#', $rows[$i]->labels)));
        }

        return $submission;
    }

    public function getNextUnsubmittedReport($user, $collection)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $user_id       = (int) $user->id;
        $collection_id = (int) $collection->id;

        $dbo->setQuery("SELECT `ax_report`.`id`      AS `id`,
                               `ax_report`.`content` AS `text`
                        FROM `ax_report`
                        INNER JOIN `ax_collection_report`
                        ON `ax_collection_report`.`report` = `ax_report`.`id`
                        INNER JOIN `ax_assignment`
                        ON `ax_assignment`.`collection` = `ax_collection_report`.`collection`
                        WHERE `ax_assignment`.`user` = ${user_id}
                        AND `ax_assignment`.`collection` = ${collection_id}
                        AND `ax_report`.`id` NOT IN (
                            SELECT `report`
                            FROM `ax_submission`
                            WHERE `user` = ${user_id}
                        )
                        ORDER BY `ax_report`.`id`
                        LIMIT 1;");

        return $dbo->loadObject();
    }

    public function getPreviousSubmissionId($user, $current_submission_id)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
		$user_id = (int) $user->id;

		if (isset($current_submission_id)) {
	        $current_submission_id = (int) $current_submission_id;

	        $dbo->setQuery("SELECT MAX(`id`)
	                        FROM (
								SELECT `report`  AS `report`,
								       MAX(`id`) AS `id`
							    FROM `ax_submission`
								WHERE `user` = ${user_id}
							    GROUP BY `report`)
							AS `_sq0`
	                        WHERE `_sq0`.`id` < ${current_submission_id};");
		} else {
			$dbo->setQuery("SELECT MAX(`id`)
							FROM `ax_submission`
							WHERE `user` = ${user_id};");
		}

        return $dbo->loadResult();
    }

    public function getPreviousSubmittedReportId($user, $previous_submission_id)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $user_id = (int) $user->id;

		if (isset($previous_submission_id)) {
			$previous_submission_id = (int) $previous_submission_id;

	        $dbo->setQuery("SELECT `report`
	                        FROM `ax_submission`
	                        WHERE `user` = ${user_id}
	                        AND `id` = ${previous_submission_id}");
		} else {
			$dbo->setQuery("SELECT `report`
	                        FROM `ax_submission`
	                        WHERE `user` = ${user_id}
	                        AND `id` >= ALL (
								SELECT MAX(`id`)
								FROM `ax_submission`
								WHERE `user` = ${user_id})");
		}

        return $dbo->loadResult();
    }

    /**
     * Get the report with the specified id.
     *
     * @param  string  $report_id  The id of the report.
     * @param  object  $user       The user.
     *
     * @return  object  The report.
     */
    public function getReport($report_id, $user)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $report_id = $dbo->quote($report_id);
        $user_id   = (int) $user->id;

        $dbo->setQuery("SELECT `ax_report`.`id`      AS `id`,
                               `ax_report`.`content` AS `text`
                        FROM `ax_report`
                        INNER JOIN `ax_collection_report`
                        ON `ax_collection_report`.`report` = `ax_report`.`id`
                        INNER JOIN `ax_assignment`
                        ON `ax_assignment`.`collection` = `ax_collection_report`.`collection`
                        WHERE `ax_assignment`.`user` = ${user_id}
                        AND `ax_report`.`id` = ${report_id};");

        return $dbo->loadObject();
    }

    /**
     * Get the report count for the specified user.
     *
     * @param  object  $user  The user.
     *
     * @return  int  The count of the reports.
     */
    public function getReportCount($collection)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $collection_id = (int) $collection->id;

        $dbo->setQuery("SELECT COUNT(*) AS `count`
                        FROM `ax_collection_report`
                        WHERE `ax_collection_report`.`collection` = ${collection_id};");

        return $dbo->loadObject()->count;
    }

    /**
     * Get the submitted report count for the specified user.
     *
     * @param  object $user  The user.
     *
     * @return  int  The count of the submitted reports.
     */
    public function getSubmittedReportCount($user, $collection)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $user_id       = (int) $user->id;
        $collection_id = (int) $collection->id;

        $dbo->setQuery("SELECT COUNT(DISTINCT `ax_report`.`id`) AS `count`
                        FROM `ax_submission`
                        INNER JOIN `ax_report`
                        ON `ax_report`.`id` = `ax_submission`.`report`
                        INNER JOIN `ax_collection_report`
                        ON `ax_collection_report`.`report` = `ax_report`.`id`
                        WHERE `ax_submission`.`user` = ${user_id}
                        AND `ax_collection_report`.`collection` = ${collection_id};");

        return $dbo->loadObject()->count;
    }

    public function saveSubmission($submission)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $user_id   = (int) $submission->user->id;
        $report_id = $dbo->quote($submission->report->id);
        $load_time = (int) $submission->load_time;

        try {
            $dbo->transactionStart();

            $dbo->setQuery("INSERT INTO `ax_submission` (
                                `report`,
                                `user`,
                                `login`,
                                `load_time`,
                                `submit_time`)
                            SELECT ${report_id},
                                   ${user_id},
                                   MAX(`jmt_action_logs`.`id`),
                                   FROM_UNIXTIME(${load_time}),
                                   NOW()
                            FROM `jmt_action_logs`
                            WHERE `extension` = 'com_users'
                            AND   `message` REGEXP '\"action\"[[:blank:]]*:[[:blank:]]*\"login\"'
                            AND   `message` REGEXP '\"app\"[[:blank:]]*:[[:blank:]]*\"PLG_ACTIONLOG_JOOMLA_APPLICATION_SITE\"'
                            AND   `user_id` = ${user_id};");
            $dbo->execute();

            $submission_id = (int) $dbo->insertid();

            for ($i = 0; $i < count($submission->tags); $i++) {
                $annotation          = $submission->tags[$i];
                $sentence            = $dbo->quote($annotation[0]);
                $normalized_sentence = $dbo->quote(strtolower(trim(str_replace('.', '', $annotation[0]))));

                $dbo->setQuery("INSERT INTO `ax_annotation` (`submission`,
                                                             `sentence`,
                                                             `normalized_sentence`)
                                VALUES (${submission_id},
                                        ${sentence},
                                        ${normalized_sentence});");
                $dbo->execute();

                $annotation_id = (int) $dbo->insertid();

                for ($j = 0; $j < count($annotation[1]); $j++) {
                    $label = $dbo->quote($annotation[1][$j]);

                    $dbo->setQuery("INSERT INTO `ax_annotation_label` (`annotation`, `label`) VALUES (${annotation_id}, ${label});");
                    $dbo->execute();
                }
            }

            $dbo->transactionCommit();
        } catch (Exception $e) {
            $dbo->transactionRollback();
            return false;
        }

        return true;
    }

    public function validateSubmission($submission)
    {
        if (!$submission->report) {
            return false;
        }

        if (!$submission->load_time or $submission->load_time >= (new DateTime())->getTimestamp()) {
            return false;
        }

        if (!$submission->tags or count($submission->tags) == 0) {
            return false;
        }

        return true;
    }
}
