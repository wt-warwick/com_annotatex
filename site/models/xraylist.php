<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * XRayList view model
 *
 * @since  0.0.1
 */
class AnnotateXModelXRayList extends JModelItem
{
	/**
	 * Return an x-ray collection given its id.
	 *
	 * @param   $user           The user
	 * @param   $collection_id  The collection id
	 *
	 * @return  The collection object.
	 */
	public function getCollection($user, $collection_id)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$user_id       = (int) $user->id;
		$collection_id = (int) $collection_id;

		$dbo->setQuery("SELECT `ax_xray_collection`.`id`          AS `id`,
			                   `ax_xray_collection`.`title`       AS `title`,
			                   `ax_xray_collection`.`description` AS `description`
		                FROM `ax_xray_collection`
		                INNER JOIN `ax_xray_assignment`
		                ON `ax_xray_assignment`.`collection` = `ax_xray_collection`.`id`
		                AND `ax_xray_assignment`.`user` = ${user_id}
		                WHERE `ax_xray_collection`.`id` = ${collection_id};");

		return $dbo->loadObject();
	}

	/**
	 * Get the x-ray collections assigned to a given user.
	 *
	 * @param   $user  The user
	 *
	 * @return  The list of collections assigned to a user.
	 */
	public function getCollections($user)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$user_id = (int) $user->id;

		$dbo->setQuery("SELECT `ax_xray_collection`.`id`          AS `id`,
			                   `ax_xray_collection`.`title`       AS `title`,
			                   `ax_xray_collection`.`description` AS `description`
		                FROM `ax_xray_collection`
		                INNER JOIN `ax_xray_assignment`
		                ON `ax_xray_assignment`.`collection` = `ax_xray_collection`.`id`
		                AND `ax_xray_assignment`.`user` = ${user_id}");

		return $dbo->loadObjectList();
	}

	/**
	 * Return the current collection of the user.
	 *
	 * @param  $user  The user
	 *
	 * @return The current collection of the user.
	 */
	public function getCurrentCollection($user)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$user_id = (int) $user->id;

		$dbo->setQuery("SELECT `ax_xray_collection`.`id`          AS `id`,
			                   `ax_xray_collection`.`title`       AS `title`,
			                   `ax_xray_collection`.`description` AS `description`
	                    FROM `ax_xray_collection`
	                    INNER JOIN `ax_xray_assignment`
	                    ON `ax_xray_assignment`.`collection` = `ax_xray_collection`.`id`
	                    INNER JOIN `ax_user_option`
	                    ON `ax_user_option`.`xray_collection` = `ax_xray_assignment`.`collection`
	                    AND `ax_user_option`.`user` = `ax_xray_assignment`.`user`
	                    WHERE `ax_user_option`.`user` = ${user_id}");

		$collection = $dbo->loadObject();

		if (empty($collection)) {
			$dbo->setQuery("SELECT `ax_xray_collection`.`id`          AS `id`,
				                   `ax_xray_collection`.`title`       AS `title`,
				                   `ax_xray_collection`.`description` AS `description`
		                    FROM `ax_xray_collection`
		                    INNER JOIN `ax_xray_assignment`
		                    ON `ax_xray_assignment`.`collection` = `ax_xray_collection`.`id`
		                    WHERE `ax_xray_assignment`.`user` = ${user_id}
		                    ORDER BY `ax_xray_collection`.`title`
		                    LIMIT 1;");

			$collection = $dbo->loadObject();
		}

		return $collection;
	}

	public function getLabelledXRayCount($user, $collection, $search)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$collection_id = (int) $collection->id;
		$search        = $dbo->quote("%".$dbo->escape($search, true)."%", false);
		$user_id       = (int) $user->id;

        if (strtolower($search) == "'%pending%'") {
            $dbo->setQuery("SELECT COUNT(*) AS `count`
                            FROM `ax_pending_xray`
                            INNER JOIN `ax_labelled_xray`
                            ON `ax_pending_xray`.`xray` = `ax_labelled_xray`.`xray`
                            INNER JOIN `ax_xray_assignment`
                            ON `ax_labelled_xray`.`collection` = `ax_xray_assignment`.`collection`
                            WHERE `ax_pending_xray`.`user` = ${user_id}
                            AND `ax_xray_assignment`.`user` = ${user_id}
                            AND `ax_xray_assignment`.`collection` = ${collection_id};");
        } else {
            $dbo->setQuery("SELECT COUNT(*) AS `count`
    			            FROM `ax_labelled_xray`
	    		            WHERE `ax_labelled_xray`.`collection` IN (
		    	            	SELECT `ax_xray_collection`.`id`
    			            	FROM `ax_xray_collection`
    			            	INNER JOIN `ax_xray_assignment`
    			            	ON `ax_xray_assignment`.`collection` = `ax_xray_collection`.`id`
    			            	WHERE `ax_xray_collection`.`id` = ${collection_id}
    			            	AND `ax_xray_assignment`.`user` = ${user_id})
    			            AND `ax_labelled_xray`.`xray` IN (
    			            	SELECT `ax_labelled_xray`.`xray`
    			            	FROM `ax_labelled_xray`
    			            	LEFT OUTER JOIN (
    			            		SELECT `ax_xray_submission`.`xray`             AS `xray`,
    			            		       `ax_xray_submission`.`collection`       AS `collection`,
    			            		       `ax_xray_submission`.`user`             AS `user`,
    			            		       MAX(`ax_xray_submission`.`submit_time`) AS `last_submit`
    			            		FROM `ax_xray_submission`
    			            		GROUP BY `ax_xray_submission`.`xray`,
    			            		         `ax_xray_submission`.`collection`,
    			            		         `ax_xray_submission`.`user`
    			            	) AS `submission`
    			            	ON `submission`.`xray` = `ax_labelled_xray`.`xray`
    			            	AND `submission`.`collection` = `ax_labelled_xray`.`collection`
    			            	AND `submission`.`user` = ${user_id}
    			            	WHERE `ax_labelled_xray`.`xray` LIKE ${search}
    			            	OR    DATE_FORMAT(`submission`.`last_submit`, '%Y-%m-%d %T') LIKE ${search}
    			            	OR    (`ax_labelled_xray`.`xray`, `ax_labelled_xray`.`collection`) IN (
    			            		SELECT `ax_labelled_xray`.`xray`,
    			            		       `ax_labelled_xray`.`collection`
    			            		FROM `ax_labelled_xray`
    			            		INNER JOIN `ax_xray_labelling`
    			            		ON `ax_xray_labelling`.`xray` = `ax_labelled_xray`.`xray`
    			            		AND `ax_xray_labelling`.`collection` = `ax_labelled_xray`.`collection`
    			            		INNER JOIN `ax_label`
    			            		ON `ax_label`.`id` = `ax_xray_labelling`.`label`
    			            		WHERE `ax_label`.`title` LIKE ${search}));");
        }

		return (int) $dbo->loadObject()->count;
	}

	public function getLabelledXRays($user, $collection, $search, $start, $size)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$collection_id = (int) $collection->id;
		$limit         = (int) $size;
		$offset        = (int) $start;
		$search        = $dbo->quote("%".$dbo->escape($search, true)."%", false);
		$user_id       = (int) $user->id;

        if (strtolower($search) == "'%pending%'") {
            $dbo->setQuery("SELECT CONCAT(`ax_pending_xray`.`xray`, '#', `ax_labelled_xray`.`collection`) AS `id`
                            FROM `ax_pending_xray`
                            INNER JOIN `ax_labelled_xray`
                            ON `ax_pending_xray`.`xray` = `ax_labelled_xray`.`xray`
                            INNER JOIN `ax_xray_assignment`
                            ON `ax_labelled_xray`.`collection` = `ax_xray_assignment`.`collection`
                            WHERE `ax_pending_xray`.`user` = ${user_id}
                            AND `ax_xray_assignment`.`user` = ${user_id}
                            AND `ax_xray_assignment`.`collection` = ${collection_id};");
        } else {
    		$dbo->setQuery("SELECT CONCAT(`ax_labelled_xray`.`xray`, '#', `ax_labelled_xray`.`collection`) AS `id`
    			            FROM `ax_labelled_xray`
    			            WHERE `ax_labelled_xray`.`collection` IN (
    			            	SELECT `ax_xray_collection`.`id`
    			            	FROM `ax_xray_collection`
    			            	INNER JOIN `ax_xray_assignment`
    			            	ON `ax_xray_assignment`.`collection` = `ax_xray_collection`.`id`
    			            	WHERE `ax_xray_collection`.`id` = ${collection_id}
    			            	AND `ax_xray_assignment`.`user` = ${user_id})
    			            AND `ax_labelled_xray`.`xray` IN (
    			            	SELECT `ax_labelled_xray`.`xray`
    			            	FROM `ax_labelled_xray`
    			            	LEFT OUTER JOIN (
    			            		SELECT `ax_xray_submission`.`xray`             AS `xray`,
    			            		       `ax_xray_submission`.`collection`       AS `collection`,
    			            		       `ax_xray_submission`.`user`             AS `user`,
    			            		       MAX(`ax_xray_submission`.`submit_time`) AS `last_submit`
    			            		FROM `ax_xray_submission`
    			            		GROUP BY `ax_xray_submission`.`xray`,
    			            		         `ax_xray_submission`.`collection`,
    			            		         `ax_xray_submission`.`user`
    			            	) AS `submission`
    			            	ON `submission`.`xray` = `ax_labelled_xray`.`xray`
    			            	AND `submission`.`collection` = `ax_labelled_xray`.`collection`
    			            	AND `submission`.`user` = ${user_id}
    			            	WHERE `ax_labelled_xray`.`xray` LIKE ${search}
    			            	OR    DATE_FORMAT(`submission`.`last_submit`, '%Y-%m-%d %T') LIKE ${search}
    			            	OR    (`ax_labelled_xray`.`xray`, `ax_labelled_xray`.`collection`) IN (
    			            		SELECT `ax_labelled_xray`.`xray`,
    			            		       `ax_labelled_xray`.`collection`
    			            		FROM `ax_labelled_xray`
    			            		INNER JOIN `ax_xray_labelling`
    			            		ON `ax_xray_labelling`.`xray` = `ax_labelled_xray`.`xray`
    			            		AND `ax_xray_labelling`.`collection` = `ax_labelled_xray`.`collection`
    			            		INNER JOIN `ax_label`
    			            		ON `ax_label`.`id` = `ax_xray_labelling`.`label`
    			            		WHERE `ax_label`.`title` LIKE ${search})
                                OR    (`ax_labelled_xray`.`xray`, `ax_labelled_xray`.`collection`) IN (
                                    SELECT `ax_xray_submission`.`xray`,
                                           `ax_xray_submission`.`collection`
                                    FROM `ax_xray_submission`
                                    INNER JOIN `ax_xray_annotation`
                                    ON `ax_xray_submission`.`id` = `ax_xray_annotation`.`submission`
                                    INNER JOIN `ax_label`
                                    ON `ax_xray_annotation`.`label` = `ax_label`.`id`
                                    WHERE `ax_xray_submission`.`user` = ${user_id}
                                    AND `ax_label`.`title` LIKE ${search}))
                            ORDER BY `ax_labelled_xray`.`xray`
    			            LIMIT ${offset}, ${limit};");
        }

		$labelled_xray_ids = $dbo->loadColumn();

		if (count($labelled_xray_ids) == 0) {
			return array();
		}

		for ($i = 0; $i < count($labelled_xray_ids); $i++) {
			$labelled_xray_ids[$i]    = explode('#', $labelled_xray_ids[$i]);

			$labelled_xray_ids[$i][0] = $dbo->quote($labelled_xray_ids[$i][0]);
			$labelled_xray_ids[$i][1] = (int) $labelled_xray_ids[$i][1];

			$labelled_xray_ids[$i]    = '('.implode(', ', $labelled_xray_ids[$i]).')';
		}

		$labelled_xray_ids = '('.implode(', ', $labelled_xray_ids).')';

		$dbo->setQuery("SELECT CONCAT(`ax_labelled_xray`.`xray`, '#', `ax_labelled_xray`.`collection`)    AS `id`,
							   GROUP_CONCAT(`ax_label`.`title` ORDER BY `ax_label`.`title` SEPARATOR '#') AS `labels`
						FROM `ax_labelled_xray`
						INNER JOIN `ax_xray`
						ON `ax_xray`.`id` = `ax_labelled_xray`.`xray`
						INNER JOIN `ax_xray_labelling`
						ON `ax_xray_labelling`.`xray` = `ax_labelled_xray`.`xray`
						AND `ax_xray_labelling`.`collection` = `ax_labelled_xray`.`collection`
						INNER JOIN `ax_label`
						ON `ax_label`.`id` = `ax_xray_labelling`.`label`
						WHERE (`ax_labelled_xray`.`xray`, `ax_labelled_xray`.`collection`) IN ${labelled_xray_ids}
						GROUP BY `ax_labelled_xray`.`xray`,
						         `ax_labelled_xray`.`collection`;");

		$labels = $dbo->loadAssocList('id');

		foreach($labels as $id => $list) {
			$labels[$id] = explode('#', $labels[$id]['labels']);
		}

        $dbo->setQuery("SELECT CONCAT(`submission`.`xray`, '#', `submission`.`collection`)                   AS `id`,
                               GROUP_CONCAT(DISTINCT `label`.`title` ORDER BY `label`.`title` SEPARATOR '#') AS `labels`
                        FROM `ax_xray_submission` AS `submission`
                        INNER JOIN `ax_xray_annotation` AS `annotation`
                        ON `submission`.`id` = `annotation`.`submission`
                        INNER JOIN `ax_label` AS `label`
                        ON `annotation`.`label` = `label`.`id`
                        WHERE `submission`.`user` = ${user_id}
                        AND (`submission`.`xray`, `submission`.`collection`) IN ${labelled_xray_ids}
                        AND `submission`.`submit_time` >= ALL (
                            SELECT MAX(`submit_time`)
                            FROM `ax_xray_submission`
                            WHERE `ax_xray_submission`.`user` = `submission`.`user`
                            AND `ax_xray_submission`.`xray` = `submission`.`xray`
                            AND `ax_xray_submission`.`collection` = `submission`.`collection`)
                        GROUP BY `submission`.`xray`,
                                 `submission`.`collection`;");

        $submissions = $dbo->loadAssocList('id');

        foreach ($submissions as $id => $list) {
            $submissions[$id] = explode('#', $submissions[$id]['labels']);
        }

        $dbo->setQuery("SELECT `ax_labelled_xray`.`xray`   AS `xray_id`,
                               `ax_labelled_xray`.`collection` AS `collection_id`,
                               IF(
                                   COUNT(`ax_xray_submission`.`id`) > 0,
                                   COUNT(`ax_xray_submission`.`id`),
                                   (
                                       SELECT -1 * COUNT(*)
                                       FROM `ax_pending_xray`
                                       WHERE `ax_pending_xray`.`user` = ${user_id}
                                       AND `ax_pending_xray`.`xray` = `ax_labelled_xray`.`xray`)) AS `n_submissions`,
                               MAX(`ax_xray_submission`.`submit_time`)                            AS `last_submit`
                        FROM `ax_labelled_xray`
                        LEFT OUTER JOIN `ax_xray_submission`
                        ON `ax_xray_submission`.`xray` = `ax_labelled_xray`.`xray`
                        AND `ax_xray_submission`.`collection` = `ax_labelled_xray`.`collection`
                        AND `ax_xray_submission`.`user` = ${user_id}
                        WHERE (`ax_labelled_xray`.`xray`, `ax_labelled_xray`.`collection`) IN ${labelled_xray_ids}
                        GROUP BY `ax_labelled_xray`.`xray`,
                                 `ax_labelled_xray`.`collection`
                        ORDER BY `ax_labelled_xray`.`xray`;");

		$labelled_xrays = $dbo->loadObjectList();

		for ($i = 0; $i < count($labelled_xrays); $i++) {
			$labelled_xrays[$i]->id             = new stdClass();
			$labelled_xrays[$i]->id->xray       = $labelled_xrays[$i]->xray_id;
			$labelled_xrays[$i]->id->collection = $labelled_xrays[$i]->collection_id;

			$id                                 = $labelled_xrays[$i]->id->xray.'#'.$labelled_xrays[$i]->id->collection;
			$labelled_xrays[$i]->labels         = isset($labels[$id]) ? $labels[$id] : array();
            $labelled_xrays[$i]->submission     = isset($submissions[$id]) ? $submissions[$id] : array();

			unset($labelled_xrays[$i]->xray_id);
			unset($labelled_xrays[$i]->collection_id);
		}

		return $labelled_xrays;
	}

	public function setCurrentCollection($user, $collection_id)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$user_id       = (int) $user->id;
		$collection_id = (int) $collection_id;

		$dbo->setQuery("SELECT COUNT(*) AS `count`
			            FROM `ax_xray_assignment`
			            WHERE `ax_xray_assignment`.`user` = ${user_id}");
		$insert = $dbo->loadResult() == 0;

		if ($insert) {
			$dbo->setQuery("INSERT INTO `ax_user_option` (`user`, `xray_collection`)
				            VALUES (${user_id}, ${collection_id});");
		} else {
			$dbo->setQuery("UPDATE `ax_user_option`
				            SET `ax_user_option`.`xray_collection` = ${collection_id}
				            WHERE `ax_user_option`.`user` = ${user_id};");
		}

		$dbo->execute();
	}

	public function validateCurrentCollection($user, $collection_id)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$user_id       = (int) $user->id;
		$collection_id = (int) $collection_id;

		$dbo->setQuery("SELECT COUNT(*) AS `count`
			            FROM `ax_xray_assignment`
			            WHERE `ax_xray_assignment`.`user` = ${user_id}
			            AND `ax_xray_assignment`.`collection` =  ${collection_id};");

		return $dbo->loadResult() == 1 ? TRUE : FALSE;
	}
}