<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Statistics view model
 *
 * @since  0.0.1
 */
class AnnotateXModelStatistics extends JModelItem
{
    public function getCollections()
    {
        $dbo = $this->getDBO();

        $dbo->setQuery("SELECT `ax_collection`.`id`          AS `id`,
                               `ax_collection`.`title`       AS `title`,
                               `ax_collection`.`description` AS `description`
                        FROM `ax_collection`");

        return $dbo->loadObjectList();
    }

    public function getAgreementByLabel($user_ids)
    {
        $dbo = $this->getDBO();

        $users = $user_ids;

        // Escape query parameters
        $user_id_count = count($user_ids);

        if ($user_id_count > 0) {
            for ($i = 0; $i < $user_id_count; $i++) {
                $user_ids[$i] = (int) $user_ids[$i];
            }

            $user_ids = implode(", ", $user_ids);
        } else {
            $user_ids = "-1";
        }

        $dbo->setQuery("SELECT `label_id` AS `label_id`,
                               COUNT(*)   AS `count`
                        FROM (
                            SELECT DISTINCT `submission_left`.`report`      AS `report_id`,
                                            `annotation_label_left`.`label` AS `label_id`
                            FROM `ax_submission` AS `submission_left`
                            INNER JOIN `ax_annotation` AS `annotation_left`
                            ON `annotation_left`.`submission` = `submission_left`.`id`
                            INNER JOIN `ax_annotation_label` AS `annotation_label_left`
                            ON `annotation_label_left`.`annotation` = `annotation_left`.`id`
                            WHERE `submission_left`.`id` IN (
                                SELECT `id`
                                FROM `ax_submission` AS `submissions`
                                WHERE `submit_time` >= (
                                    SELECT MAX(`submit_time`)
                                    FROM `ax_submission`
                                    WHERE `report` = `submissions`.`report`
                                    AND `user` = `submissions`.`user`))
                            AND `submission_left`.`user` IN (${user_ids})
                            AND `submission_left`.`report` IN (
                                SELECT `report_id`
                                FROM (
                                    SELECT `report`               AS `report_id`,
                                           COUNT(DISTINCT `user`) AS `count`
                                    FROM `ax_submission`
                                    WHERE `user` IN (${user_ids})
                                    GROUP BY `report`
                                    HAVING `count` = ${user_id_count})
                                AS `_sq0`)
                            AND `annotation_label_left`.`label` IN (
                                SELECT `annotation_label_right`.`label`
                                FROM `ax_submission` AS `submission_right`
                                INNER JOIN `ax_annotation` AS `annotation_right`
                                ON `annotation_right`.`submission` = `submission_right`.`id`
                                INNER JOIN `ax_annotation_label` AS `annotation_label_right`
                                ON `annotation_label_right`.`annotation` = `annotation_right`.`id`
                                WHERE `submission_right`.`id` IN (
                                    SELECT `id`
                                    FROM `ax_submission` AS `submissions`
                                    WHERE `submit_time` >= (
                                        SELECT MAX(`submit_time`)
                                        FROM `ax_submission`
                                        WHERE `report` = `submissions`.`report`
                                        AND `user` = `submissions`.`user`))
                                AND `submission_right`.`report` = `submission_left`.`report`
                                AND `submission_right`.`user` <> `submission_left`.`user`
                                AND `submission_right`.`user` IN (${user_ids})))
                        AS `_sq1`
                        GROUP BY `label_id`;");

        $label_agreement = $dbo->loadAssocList('label_id');

        foreach ($label_agreement as $label_id => $_) {
            $label_agreement[$label_id] = (int) $label_agreement[$label_id]['count'];
        }

        $dbo->setQuery("SELECT `user_id`  AS `user_id`,
                               `label_id` AS `label_id`,
                               COUNT(*)   AS `count`
                        FROM (
                            SELECT DISTINCT `submission_left`.`user`        AS `user_id`,
                                            `submission_left`.`report`      AS `report_id`,
                                            `annotation_label_left`.`label` AS `label_id`
                            FROM `ax_submission` AS `submission_left`
                            INNER JOIN `ax_annotation` AS `annotation_left`
                            ON `annotation_left`.`submission` = `submission_left`.`id`
                            INNER JOIN `ax_annotation_label` AS `annotation_label_left`
                            ON `annotation_label_left`.`annotation` = `annotation_left`.`id`
                            WHERE `submission_left`.`report` IN (
                                SELECT `report_id`
                                FROM (
                                    SELECT `report`               AS `report_id`,
                                           COUNT(DISTINCT `user`) AS `count`
                                    FROM `ax_submission`
                                    WHERE `user` IN (${user_ids})
                                    GROUP BY `report`
                                    HAVING `count` = ${user_id_count})
                                AS `_sq0`)
                            AND `submission_left`.`user` IN (${user_ids})
                            AND `submission_left`.`id` IN (
                                SELECT `id`
                                FROM `ax_submission` AS `submissions`
                                WHERE `submit_time` >= (
                                    SELECT MAX(`submit_time`)
                                    FROM `ax_submission`
                                    WHERE `report` = `submissions`.`report`
                                    AND `user` = `submissions`.`user`)))
                        AS `_sq1`
                        GROUP BY `user_id`,
                                 `label_id`
                        ORDER BY `user_id`,
                                 `label_id`;");

         $user_labelling = $dbo->loadObjectList();

         $dbo->setQuery("SELECT `id`
                         FROM `ax_label`
                         ORDER BY `id`;");

        $labels = $dbo->loadColumn();
        $result = array();

        for ($i = 0; $i < count($labels); $i++) {
            $result[$labels[$i]] = array();
            $result[$labels[$i]]['agreement'] = 0;

            for ($j = 0; $j < count($users); $j++) {
                $result[$labels[$i]][$users[$j]] = 0;
            }
        }

        for ($i = 0; $i < count($user_labelling); $i++) {
            $user_id  = $user_labelling[$i]->user_id;
            $label_id = $user_labelling[$i]->label_id;
            $count    = (int) $user_labelling[$i]->count;

            $result[$label_id][$user_id] = $count;
        }

        foreach ($label_agreement as $label_id => $count) {
            $result[$label_id]['agreement'] = $count;
        }

        return $result;
    }

    public function getReportsInDisagreement($offset, $limit)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $limit  = (int) $limit;
        $offset = (int) $offset;

        $dbo->setQuery("SELECT `report_left`.`id`      AS `id`,
                               `report_left`.`content` AS `content`,
                               COUNT(*)                AS `count`
                        FROM `ax_report` AS `report_left`
                        INNER JOIN `ax_submission` AS `submission_left`
                        ON `submission_left`.`report` = `report_left`.`id`
                        INNER JOIN `ax_annotation` AS `annotation_left`
                        ON `annotation_left`.`submission` = `submission_left`.`id`
                        INNER JOIN `ax_annotation_label` AS `annotation_label_left`
                        ON `annotation_label_left`.`annotation` = `annotation_left`.`id`
                        WHERE `annotation_label_left`.`label` NOT IN (
                            SELECT `annotation_label_right`.`label`
                            FROM `ax_submission` AS `submission_right`
                            INNER JOIN `ax_annotation` AS `annotation_right`
                            ON `annotation_right`.`submission` = `submission_right`.`id`
                            INNER JOIN `ax_annotation_label` AS `annotation_label_right`
                            ON `annotation_label_right`.`annotation` = `annotation_right`.`id`
                            WHERE `submission_right`.`id` IN (
                                SELECT `id`
                                FROM `ax_submission` AS `submissions`
                                WHERE `submit_time` >= (
                                    SELECT MAX(`submit_time`)
                                    FROM `ax_submission`
                                    WHERE `report` = `submissions`.`report`
                                    AND `user` = `submissions`.`user`))
                            AND `submission_left`.`report` = `submission_right`.`report`
                            AND `submission_left`.`user` <> `submission_right`.`user`)
                        AND 0 < (
                            SELECT COUNT(*)
                            FROM `ax_submission` AS `submission_right`
                            WHERE `submission_left`.`report` = `submission_right`.`report`
                            AND `submission_right`.`user` <> `submission_left`.`user`)
                        AND `submission_left`.`id` IN (
                            SELECT `id`
                            FROM `ax_submission` AS `submission_right`
                            WHERE `submit_time` >= (
                                SELECT MAX(`submit_time`)
                                FROM `ax_submission`
                                WHERE `report` = `submission_right`.`report`
                                AND `user` = `submission_right`.`user`))
                        GROUP BY `report_left`.`id`,
                                 `report_left`.`content`
                        ORDER BY `report`
                        LIMIT ${offset}, ${limit};");

        return $dbo->loadObjectList();
    }

    public function getReportsInDisagreementCount()
    {
        $dbo = $this->getDBO();

        $dbo->setQuery("SELECT COUNT(DISTINCT `submission_left`.`report`) AS `count`
                        FROM `ax_submission` AS `submission_left`
                        INNER JOIN `ax_annotation` AS `annotation_left`
                        ON `annotation_left`.`submission` = `submission_left`.`id`
                        INNER JOIN `ax_annotation_label` AS `annotation_label_left`
                        ON `annotation_label_left`.`annotation` = `annotation_left`.`id`
                        WHERE `annotation_label_left`.`label` NOT IN (
                            SELECT `annotation_label_right`.`label`
                            FROM `ax_submission` AS `submission_right`
                            INNER JOIN `ax_annotation` AS `annotation_right`
                            ON `annotation_right`.`submission` = `submission_right`.`id`
                            INNER JOIN `ax_annotation_label` AS `annotation_label_right`
                            ON `annotation_label_right`.`annotation` = `annotation_right`.`id`
                            WHERE `submission_right`.`id` IN (
                                SELECT `id`
                                FROM `ax_submission` AS `submissions`
                                WHERE `submit_time` >= (
                                    SELECT MAX(`submit_time`)
                                    FROM `ax_submission`
                                    WHERE `report` = `submissions`.`report`
                                    AND `user` = `submissions`.`user`))
                            AND `submission_left`.`report` = `submission_right`.`report`
                            AND `submission_left`.`user` <> `submission_right`.`user`)
                        AND 0 < (
                            SELECT COUNT(*)
                            FROM `ax_submission` AS `submission_right`
                            WHERE `submission_left`.`report` = `submission_right`.`report`
                            AND `submission_right`.`user` <> `submission_left`.`user`)
                        AND `submission_left`.`id` IN (
                            SELECT `id`
                            FROM `ax_submission` AS `submission_right`
                            WHERE `submit_time` >= (
                                SELECT MAX(`submit_time`)
                                FROM `ax_submission`
                                WHERE `report` = `submission_right`.`report`
                                AND `user` = `submission_right`.`user`));");

        return $dbo->loadObject()->count;
    }

    /**
     * Get the report count for the specified users.
     *
     * @param  array  $users  The users.
     *
     * @return  array  The count of reports per user.
     */
    public function getSubmissionTimeStatistics($user_ids)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        for ($i = 0; $i < count($user_ids); $i++) {
            $user_ids[$i] = (int) $user_ids[$i];
        }

        $user_ids = implode(", ", $user_ids);

        $dbo->setQuery("SELECT `user`                                                                                 AS `user_id`,
                               MIN(TIMESTAMPDIFF(SECOND, `ax_submission`.`load_time`, `ax_submission`.`submit_time`)) AS `min`,
                               MAX(TIMESTAMPDIFF(SECOND, `ax_submission`.`load_time`, `ax_submission`.`submit_time`)) AS `max`,
                               AVG(TIMESTAMPDIFF(SECOND, `ax_submission`.`load_time`, `ax_submission`.`submit_time`)) AS `average`,
                               STD(TIMESTAMPDIFF(SECOND, `ax_submission`.`load_time`, `ax_submission`.`submit_time`)) AS `dev`
                        FROM `ax_submission`
                        INNER JOIN `ax_report`
                        ON `ax_report`.`id` = `ax_submission`.`report`
                        WHERE `ax_submission`.`user` IN (${user_ids})
                        GROUP BY `user`;");

        return $dbo->loadAssocList('user_id');
    }

    public function getSubmittedSentenceCount($user_ids)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        for ($i = 0; $i < count($user_ids); $i++) {
            $user_ids[$i] = (int) $user_ids[$i];
        }

        $user_ids = implode(", ", $user_ids);

        $dbo->setQuery("SELECT `ax_submission`.`user` AS `user_id`,
                               COUNT(*)               AS `count`
                        FROM `ax_submission`
                        INNER JOIN `ax_annotation`
                        ON `ax_submission`.`id` = `ax_annotation`.`submission`
                        WHERE `ax_submission`.`id` IN (
                            SELECT `id`
                            FROM `ax_submission` AS `submission`
                            WHERE `submit_time` >= (
                                SELECT MAX(`submit_time`)
                                FROM `ax_submission`
                                WHERE `report` = `submission`.`report`
                                AND `user` = `submission`.`user`))
                        AND `ax_submission`.`user` IN (${user_ids})
                        GROUP BY `user_id`;");

        return $dbo->loadAssocList('user_id');
    }

    public function getResubmittedReportCount($user_ids)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        for ($i = 0; $i < count($user_ids); $i++) {
            $user_ids[$i] = (int) $user_ids[$i];
        }

        $user_ids = implode(", ", $user_ids);

        $dbo->setQuery("SELECT `user`   AS `user_id`,
                               COUNT(*) AS `count`
                        FROM `ax_submission` AS `submissions`
                        WHERE `submit_time` < (
                            SELECT MAX(`submit_time`)
                            FROM `ax_submission`
                            WHERE `report` = `submissions`.`report`
                            AND `user` = `submissions`.`user`)
                        GROUP BY `user`;");

        return $dbo->loadAssocList('user_id');
    }

    public function getSubmittedLabelCount($user_ids)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        for ($i = 0; $i < count($user_ids); $i++) {
            $user_ids[$i] = (int) $user_ids[$i];
        }

        $user_ids = implode(", ", $user_ids);

        $dbo->setQuery("SELECT `ax_submission`.`user` AS `user_id`,
                               COUNT(*)               AS `count`
                        FROM `ax_submission`
                        INNER JOIN `ax_annotation`
                        ON `ax_submission`.`id` = `ax_annotation`.`submission`
                        INNER JOIN `ax_annotation_label`
                        ON `ax_annotation`.`id` = `ax_annotation_label`.`annotation`
                        WHERE `ax_submission`.`id` IN (
                            SELECT `id`
                            FROM `ax_submission` AS `submission`
                            WHERE `submit_time` >= (
                                SELECT MAX(`submit_time`)
                                FROM `ax_submission`
                                WHERE `report` = `submission`.`report`
                                AND `user` = `submission`.`user`))
                        AND `ax_submission`.`user` IN (${user_ids})
                        GROUP BY `user_id`;");

        return $dbo->loadAssocList('user_id');
    }

    public function getOverallDisagreement($dates) {
        $dbo = $this->getDBO();

        // Escape query parameters
        for ($i = 0; $i < count($dates); $i++) {

        }
    }

    public function getEstimatedHours($user_ids)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        for ($i = 0; $i < count($user_ids); $i++) {
            $user_ids[$i] = (int) $user_ids[$i];
        }

        $user_ids = implode(", ", $user_ids);

        $dbo->setQuery("SELECT `sessions`.`user_id`                                                     AS `user_id`,
                               SUM(TIMESTAMPDIFF(SECOND, `sessions`.`login`, `sessions`.`last_action`)) AS `seconds`
                        FROM (
                            SELECT `ax_submission`.`user`             AS `user_id`,
                                   `jmt_action_logs`.`log_date`       AS `login`,
                                   MAX(`ax_submission`.`submit_time`) AS `last_action`
                            FROM `ax_submission`
                            INNER JOIN `jmt_action_logs`
                            ON `jmt_action_logs`.`id` = `ax_submission`.`login`
                            WHERE `ax_submission`.`user` IN (${user_ids})
                            GROUP BY `ax_submission`.`user`, `jmt_action_logs`.`log_date`)
                        AS `sessions`
                        GROUP BY `user_id`;");

        return $dbo->loadAssocList('user_id');
    }

    /**
     * Get the report count for the specified users.
     *
     * @param  array  $users  The users.
     *
     * @return  array  The count of reports per user.
     */
    public function getReportCount($user_ids)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        for ($i = 0; $i < count($user_ids); $i++) {
            $user_ids[$i] = (int) $user_ids[$i];
        }

        $user_ids = implode(", ", $user_ids);

        $dbo->setQuery("SELECT `user` AS `user_id`,
                               COUNT(*)  AS `count`
                        FROM `ax_assignment`
                        INNER JOIN `ax_collection`
                        ON `ax_collection`.`id` = `ax_assignment`.`collection`
                        INNER JOIN `ax_collection_report`
                        ON `ax_collection_report`.`collection` = `ax_collection`.`id`
                        WHERE `ax_assignment`.`user` IN (${user_ids})
                        GROUP BY `user`;");

        return $dbo->loadAssocList('user_id');
    }

    public function getSessionCount($user_id)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $user_id = (int) $user_id;

        $dbo->setQuery("SELECT COUNT(DISTINCT `login`) AS `count`
                        FROM `ax_submission`
                        WHERE `user` = ${user_id};");

        return $dbo->loadObject()->count;
    }

    public function getSubmissionsPerDayOfTheWeek($user_id)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $user_id = (int) $user_id;

        $dbo->setQuery("SELECT DAYNAME(submit_time) AS `day_of_week`,
                               COUNT(*)             AS `count`
                        FROM `ax_submission`
                        WHERE `user` = ${user_id}
                        GROUP BY `day_of_week`;");

        $rows   = $dbo->loadAssocList();
        $result = array();

        for ($i = 0; $i < count($rows); $i++) {
            $result[strtolower($rows[$i]['day_of_week'])] = $rows[$i]['count'];
        }

        return $result;
    }

    public function getSubmissionsPerSession($user_id, $size = NULL, $offset = NULL)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $user_id = (int) $user_id;
        $size    = (int) $size;
        $offset  = (int) $offset;

        $dbo->setQuery("SELECT DATE_FORMAT(`sessions`.`login_action`, '%Y-%m-%d')                         AS `date`,
                               TIMESTAMPDIFF(SECOND, `sessions`.`login_action`, `sessions`.`last_action`) AS `duration`,
                               TIME(`sessions`.`login_action`)                                            AS `login_time`,
                               TIME(`sessions`.`last_action`)                                             AS `last_action_time`,
                               `sessions`.`submissions`                                                   AS `submissions`
                        FROM (
                            SELECT `jmt_action_logs`.`log_date`       AS `login_action`,
                                   MAX(`ax_submission`.`submit_time`) AS `last_action`,
                                   COUNT(*)                           AS `submissions`
                            FROM `ax_submission`
                            INNER JOIN `jmt_action_logs`
                            ON `ax_submission`.`login` = `jmt_action_logs`.`id`
                            WHERE `ax_submission`.`user` = ${user_id}
                            GROUP BY `jmt_action_logs`.`log_date`)
                        AS `sessions`
                        ORDER BY `date` DESC, `login_time` DESC".
                        ($size ? ' LIMIT '.($offset ? $offset.", ".$size : $size) : "").";");

        return $dbo->loadObjectList();
    }

    public function getLabelDistribution($collection_id) {
        $dbo = $this->getDBO();

        // Escape query parameters
        $collection_id = (int) $collection_id;

        $dbo->setQuery("SELECT `ax_annotation_label`.`label` AS `label`,
                               COUNT(*)                      AS `count`
                        FROM `ax_collection`
                        INNER JOIN `ax_collection_report`
                        ON `ax_collection_report`.`collection` = `ax_collection`.`id`
                        INNER JOIN `ax_report`
                        ON `ax_report`.`id` = `ax_collection_report`.`report`
                        INNER JOIN `ax_submission`
                        ON `ax_submission`.`report` = `ax_report`.`id`
                        INNER JOIN `ax_annotation`
                        ON `ax_annotation`.`submission` = `ax_submission`.`id`
                        INNER JOIN `ax_annotation_label`
                        ON `ax_annotation_label`.`annotation` = `ax_annotation`.`id`
                        WHERE `ax_collection`.`id` = ${collection_id}
                        AND `ax_submission`.`id` IN (
                            SELECT `ax_submission`.`id`
                            FROM `ax_submission` AS `submission`
                            WHERE `ax_submission`.`submit_time` = (
                                SELECT MAX(`ax_submission`.`submit_time`)
                                FROM `ax_submission`
                                WHERE `ax_submission`.`id` = `submission`.`id`))
                        GROUP BY `ax_annotation_label`.`label`;");

        $objects = $dbo->loadObjectList();
        $result  = array();

        for ($i = 0; $i < count($objects); $i++) {
            $result[$objects[$i]->label] = $objects[$i]->count;
        }

        return $result;
    }

    public function getAverageTimePerReportLength($user_id) {
        $dbo = $this->getDBO();

        // Escape query parameters
        $user_id = (int) $user_id;

        $dbo->setQuery("SELECT IF(`ax_submission`.`user` = ${user_id}, 'user', 'others')                                        AS `subject`,
                               CAST(LENGTH(`ax_report`.`content`) / 50 AS INT) * 50 + 50                                        AS `length_class`,
                               ROUND(AVG(TIMESTAMPDIFF(SECOND, `ax_submission`.`load_time`, `ax_submission`.`submit_time`)), 2) AS `average_time`
                        FROM `ax_submission`
                        INNER JOIN `ax_report`
                        ON `ax_report`.`id` = `ax_submission`.`report`
                        GROUP BY `subject`, `length_class`
                        ORDER BY `subject`, `length_class`;");

        $rows = $dbo->loadAssocList();

        $result         = new stdClass();
        $result->others = array();
        $result->user   = array();

        for ($i = 0; $i < count($rows); $i++) {
            $value               = new stdClass();
            $value->length_class = $rows[$i]['length_class'];
            $value->average_time = $rows[$i]['average_time'];

            if ($rows[$i]['subject'] === 'user') {
                array_push($result->user, $value);
            } else {
                array_push($result->others, $value);
            }
        }

        return $result;
    }

    /**
     * Get the submitted report count for the specified users.
     *
     * @param  array  $users  The users.
     *
     * @return  array  The count of submitted reports per user.
     */
    public function getSubmittedReportCount($user_ids)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        for ($i = 0; $i < count($user_ids); $i++) {
            $user_ids[$i] = (int) $user_ids[$i];
        }

        $user_ids = implode(", ", $user_ids);

        $dbo->setQuery("SELECT `user`                    AS `user_id`,
                                COUNT(DISTINCT `report`) AS `count`
                        FROM `ax_submission`
                        WHERE `user` IN (${user_ids})
                        GROUP BY `user`;");

        return $dbo->loadAssocList('user_id');
    }
}
