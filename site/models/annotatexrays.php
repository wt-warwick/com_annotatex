<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * AnnotateXRays view model
 *
 * @since  0.0.1
 */
class AnnotateXModelAnnotateXRays extends JModelItem
{
    public function getAnnotations($labelled_xray)
    {
		$dbo = $this->getDBO();

		// Escape query parameters
        $xray_id = $dbo->quote($labelled_xray->id);

        $dbo->setQuery("SELECT `ax_annotation`.`sentence`                                AS `sentence`,
                               GROUP_CONCAT(`ax_annotation_label`.`label` SEPARATOR '#') AS `labels`
                        FROM `ax_annotation`
                        INNER JOIN `ax_annotation_label`
                        ON `ax_annotation_label`.`annotation` = `ax_annotation`.`id`
                        WHERE `ax_annotation`.`submission` IN (
                            SELECT `ax_submission`.`id`
                            FROM `ax_submission`
                            WHERE `ax_submission`.`report` = ${xray_id}
                            AND `ax_submission`.`submit_time` = (
                                SELECT MAX(`submit_time`)
                                FROM `ax_submission`
                                WHERE `report` = ${xray_id}))
                        GROUP BY `ax_annotation`.`sentence`");

        $rows = $dbo->loadObjectList();
        $result = array();

        for ($i = 0; $i < count($rows); $i++) {
            $labels = explode('#', $rows[$i]->labels);
            $sentence = $rows[$i]->sentence;

            for ($j = 0; $j < count($labels); $j++) {
                $label = $labels[$j];

                if (!isset($result[$label])) {
                    $result[$label] = array();
                }

                array_push($result[$label], $sentence);
            }
        }

        return $result;
    }

    public function getPreviousSubmissionId($user, $current_submission_id)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
		$user_id = (int) $user->id;

		if (isset($current_submission_id)) {
	        $current_submission_id = (int) $current_submission_id;

	        $dbo->setQuery("SELECT MAX(`id`)
	                        FROM (
								SELECT `xray`       AS `xray`,
                                       `collection` AS `collection`,
								       MAX(`id`)    AS `id`
							    FROM `ax_xray_submission`
								WHERE `user` = ${user_id}
							    GROUP BY `xray`,
                                         `collection`)
							AS `_sq0`
	                        WHERE `_sq0`.`id` < ${current_submission_id};");
		} else {
			$dbo->setQuery("SELECT MAX(`id`)
							FROM `ax_xray_submission`
							WHERE `user` = ${user_id};");
		}

        return $dbo->loadResult();
    }

    public function getPreviousSubmittedXRayIdAndCollection($user, $previous_submission_id)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $user_id = (int) $user->id;

		if (isset($previous_submission_id)) {
			$previous_submission_id = (int) $previous_submission_id;

	        $dbo->setQuery("SELECT `xray`,
                                   `collection`
	                        FROM `ax_xray_submission`
	                        WHERE `user` = ${user_id}
	                        AND `id` = ${previous_submission_id}");
		} else {
			$dbo->setQuery("SELECT `xray`,
                                   `collection`
	                        FROM `ax_xray_submission`
	                        WHERE `user` = ${user_id}
	                        AND `id` >= ALL (
								SELECT MAX(`id`)
								FROM `ax_xray_submission`
								WHERE `user` = ${user_id})");
		}

        return $dbo->loadObject();
    }

	/**
	 * Return an x-ray collection given its id.
	 *
	 * @param   $user           The user
	 * @param   $collection_id  The collection id
	 *
	 * @return  The collection object.
	 */
	public function getCollection($user, $collection_id)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$user_id       = (int) $user->id;
		$collection_id = (int) $collection_id;

		$dbo->setQuery("SELECT `ax_xray_collection`.`id`          AS `id`,
			                   `ax_xray_collection`.`title`       AS `title`,
			                   `ax_xray_collection`.`description` AS `description`
		                FROM `ax_xray_collection`
		                INNER JOIN `ax_xray_assignment`
		                ON `ax_xray_assignment`.`collection` = `ax_xray_collection`.`id`
		                AND `ax_xray_assignment`.`user` = ${user_id}
		                WHERE `ax_xray_collection`.`id` = ${collection_id};");

		return $dbo->loadObject();
	}

	public function getReport($report_id, $user)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$report_id = $dbo->quote($report_id);
		$user_id   = (int) $user->id;

		$dbo->setQuery("SELECT `ax_report`.`id`      AS `id`,
							   `ax_report`.`content` AS `text`
						FROM `ax_report`
						INNER JOIN `ax_labelled_xray`
						ON `ax_report`.`id` = `ax_labelled_xray`.`xray`
						INNER JOIN `ax_xray_assignment`
						ON `ax_xray_assignment`.`collection` = `ax_labelled_xray`.`collection`
						WHERE `ax_xray_assignment`.`user` = ${user_id}
						AND `ax_report`.`id` = ${report_id};");

		return $dbo->loadObject();
	}

	/**
	 * Get the x-ray collections assigned to a given user.
	 *
	 * @param   $user  The user
	 *
	 * @return  The list of collections assigned to a user.
	 */
	public function getCollections($user)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$user_id = (int) $user->id;

		$dbo->setQuery("SELECT `ax_xray_collection`.`id`          AS `id`,
			                   `ax_xray_collection`.`title`       AS `title`,
			                   `ax_xray_collection`.`description` AS `description`
		                FROM `ax_xray_collection`
		                INNER JOIN `ax_xray_assignment`
		                ON `ax_xray_assignment`.`collection` = `ax_xray_collection`.`id`
		                AND `ax_xray_assignment`.`user` = ${user_id}");

		return $dbo->loadObjectList();
	}

	/**
	 * Return the current collection of the user.
	 *
	 * @param  $user  The user
	 *
	 * @return The current collection of the user.
	 */
	public function getCurrentCollection($user)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$user_id = (int) $user->id;

		$dbo->setQuery("SELECT `ax_xray_collection`.`id`          AS `id`,
			                   `ax_xray_collection`.`title`       AS `title`,
			                   `ax_xray_collection`.`description` AS `description`
	                    FROM `ax_xray_collection`
	                    INNER JOIN `ax_xray_assignment`
	                    ON `ax_xray_assignment`.`collection` = `ax_xray_collection`.`id`
	                    INNER JOIN `ax_user_option`
	                    ON `ax_user_option`.`xray_collection` = `ax_xray_assignment`.`collection`
	                    AND `ax_user_option`.`user` = `ax_xray_assignment`.`user`
	                    WHERE `ax_user_option`.`user` = ${user_id}");

		$collection = $dbo->loadObject();

		if (empty($collection)) {
			$dbo->setQuery("SELECT `ax_xray_collection`.`id`          AS `id`,
				                   `ax_xray_collection`.`title`       AS `title`,
				                   `ax_xray_collection`.`description` AS `description`
		                    FROM `ax_xray_collection`
		                    INNER JOIN `ax_xray_assignment`
		                    ON `ax_xray_assignment`.`collection` = `ax_xray_collection`.`id`
		                    WHERE `ax_xray_assignment`.`user` = ${user_id}
		                    ORDER BY `ax_xray_collection`.`title`
		                    LIMIT 1;");

			$collection = $dbo->loadObject();
		}

		return $collection;
	}

    public function getDefaultLabels() {
		$dbo = $this->getDBO();

        $default_ids = array('object', 'mark');

        for ($i = 0; $i < count($default_ids); $i++) {
            $default_ids[$i] = $dbo->quote($default_ids[$i]);
        }

        $default_ids = '('.implode(',', $default_ids).')';

		$dbo->setQuery("SELECT `id`          AS `id`,
			                   `title`       AS `title`,
			                   `description` AS `description`
			            FROM `ax_label`
                        WHERE `id` IN ${default_ids}
			            ORDER BY `assortment`, `ordering`;");

		$rows = $dbo->loadObjectList();

		return $rows;
    }

	/**
	 * Get a labelled x-ray given the x-ray id and collection id.
	 *
	 * @param  $user The user  The user
	 * @param  $xray_id        The x-ray id
	 * @param  $collection_id  The collection id
	 *
	 * @return  A labelled x-ray object.
	 */
	public function getLabelledXRay($user, $xray_id, $collection)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$collection_id = (int) $collection->id;
		$user_id       = (int) $user->id;
		$xray_id       = $dbo->quote($xray_id);

		$dbo->setQuery("SELECT `ax_xray`.`id`                  AS `id`,
							   `ax_labelled_xray`.`collection` AS `collection`,
							   `ax_xray`.`image_path`          AS `image_path`
						FROM `ax_labelled_xray`
						INNER JOIN `ax_xray`
						ON `ax_xray`.`id` = `ax_labelled_xray`.`xray`
						WHERE `ax_labelled_xray`.`xray` = ${xray_id}
						AND `ax_labelled_xray`.`collection` = ${collection_id}
						AND `ax_labelled_xray`.`collection` IN (
							SELECT `ax_xray_collection`.`id`
							FROM `ax_xray_collection`
							INNER JOIN `ax_xray_assignment`
							ON `ax_xray_assignment`.`collection` = `ax_xray_collection`.`id`
							WHERE `ax_xray_assignment`.`user` = ${user_id});");

		$labelled_xray             = $dbo->loadObject();
		$labelled_xray->collection = $this->getCollection($user, $labelled_xray->collection);
		$labelled_xray->labels     = $this->getLabelledXRayLabels($labelled_xray);

		return $labelled_xray;
	}

	public function getLabelledXRayCount($collection, $search)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$collection_id = (int) $collection->id;
		$search        = $dbo->quote("%".$dbo->escape($search, true)."%", false);

		$dbo->setQuery("SELECT COUNT(*) AS `count`
						FROM `ax_labelled_xray`
						INNER JOIN `ax_xray`
						ON `ax_xray`.`id` = `ax_labelled_xray`.`xray`
						WHERE `ax_labelled_xray`.`collection` = ${collection_id};");

		return (int) $dbo->loadObject()->count;
	}

	public function getLabelledXRayLabels($labelled_xray)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$xray_id       = $dbo->quote($labelled_xray->id);
		$collection_id = (int) $labelled_xray->collection->id;

		$dbo->setQuery("SELECT `ax_label`.`id`          AS `id`,
			                   `ax_label`.`title`       AS `title`,
			                   `ax_label`.`description` AS `description`
			            FROM `ax_labelled_xray`
			            INNER JOIN `ax_xray_labelling`
			            ON `ax_xray_labelling`.`xray` = `ax_labelled_xray`.`xray`
			            AND `ax_xray_labelling`.`collection` = `ax_labelled_xray`.`collection`
			            INNER JOIN `ax_label`
			            ON `ax_label`.`id` = `ax_xray_labelling`.`label`
			            WHERE `ax_labelled_xray`.`xray` = ${xray_id}
			            AND `ax_labelled_xray`.`collection` = ${collection_id}
			            ORDER BY `ax_label`.`title` ASC;");

		return $dbo->loadObjectList();
	}

	/**
	 * Return the list of labels by collection.
	 *
	 * @return  array  The list of labels.
	 */
	public function getLabels() {
		$dbo = $this->getDBO();

		$dbo->setQuery("SELECT `id`          AS `id`,
			                   `title`       AS `title`,
			                   `description` AS `description`,
			                   `assortment`  AS `assortment`,
			                   `ordering`    AS `ordering`
			            FROM `ax_label`
			            ORDER BY `assortment`, `ordering`;");

		$rows   = $dbo->loadObjectList();
		$result = array();

		for ($i = 0; $i < count($rows); $i++) {
			if (empty($result[$rows[$i]->assortment])) {
				$result[$rows[$i]->assortment] = array();
			}

			array_push($result[$rows[$i]->assortment], $rows[$i]);
		}

		return $result;
	}

	public function getLatestSubmission($user, $labelled_xray)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$collection_id = (int) $labelled_xray->collection->id;
		$user_id       = (int) $user->id;
		$xray_id       = $dbo->quote($labelled_xray->id);

		$dbo->setQuery("SELECT `ax_xray_annotation`.`label`  AS `label`,
			                   `ax_xray_annotation`.`shapes` AS `shape`
			            FROM `ax_xray_annotation`
			            INNER JOIN `ax_xray_submission`
			            ON `ax_xray_submission`.`id` = `ax_xray_annotation`.`submission`
			            WHERE `ax_xray_submission`.`xray` = ${xray_id}
			            AND `ax_xray_submission`.`collection` = ${collection_id}
			            AND `ax_xray_submission`.`user` = ${user_id}
			            AND `ax_xray_submission`.`submit_time` >= ALL (
			            	SELECT `submit_time`
			            	FROM `ax_xray_submission`
				            WHERE `xray` = ${xray_id}
				            AND `collection` = ${collection_id}
				            AND `user` = ${user_id});");

		$submissions = $dbo->loadObjectList();

		for ($i = 0; $i < count($submissions); $i++) {
			$submissions[$i]->shape = json_decode($submissions[$i]->shape);
		}

		return $submissions;
	}

	public function getNextUnsubmittedLabelledXRay($user, $collection)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$collection_id = (int) $collection->id;
		$user_id       = (int) $user->id;

		$dbo->setQuery("SELECT `ax_labelled_xray`.`xray`       AS `id`,
							   `ax_labelled_xray`.`collection` AS `collection`,
							   `ax_xray`.`image_path`          AS `image_path`
						FROM `ax_labelled_xray`
						INNER JOIN `ax_xray`
						ON `ax_xray`.`id` = `ax_labelled_xray`.`xray`
						WHERE `ax_labelled_xray`.`collection` = ${collection_id}
						AND `ax_labelled_xray`.`collection` IN (
							SELECT `ax_xray_collection`.`id`
							FROM `ax_xray_collection`
							INNER JOIN `ax_xray_assignment`
							ON `ax_xray_assignment`.`collection` = `ax_xray_collection`.`id`
							WHERE `ax_xray_assignment`.`user` = ${user_id})
						AND (`ax_labelled_xray`.`xray`, `ax_labelled_xray`.`collection`) NOT IN (
							SELECT `xray`,
							       `collection`
							FROM `ax_xray_submission`
							WHERE `user` = ${user_id})
                        AND (${user_id}, `ax_labelled_xray`.`xray`) NOT IN (
                            SELECT `user`,
                                   `xray`
                            FROM `ax_pending_xray`)
						LIMIT 1;");

		$labelled_xray = $dbo->loadObject();

		if (!$labelled_xray) {
			return NULL;
		}

		$labelled_xray->collection = $this->getCollection($user, $collection_id);
		$labelled_xray->labels     = $this->getLabelledXRayLabels($labelled_xray);

		return $labelled_xray;
	}

	public function getPreloadXRays($user, $collection)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$collection_id = (int) $collection->id;
		$user_id       = (int) $user->id;

		$dbo->setQuery("SELECT `ax_xray`.`image_path` AS `image_path`
						FROM `ax_labelled_xray`
						INNER JOIN `ax_xray`
						ON `ax_xray`.`id` = `ax_labelled_xray`.`xray`
						WHERE `ax_labelled_xray`.`collection` = ${collection_id}
						AND `ax_labelled_xray`.`collection` IN (
							SELECT `ax_xray_collection`.`id`
							FROM `ax_xray_collection`
							INNER JOIN `ax_xray_assignment`
							ON `ax_xray_assignment`.`collection` = `ax_xray_collection`.`id`
							WHERE `ax_xray_assignment`.`user` = ${user_id})
						AND (`ax_labelled_xray`.`xray`, `ax_labelled_xray`.`collection`) NOT IN (
							SELECT `xray`,
							       `collection`
							FROM `ax_xray_submission`
							WHERE `user` = ${user_id})
						LIMIT 1, 5;");

		return $dbo->loadColumn();
	}

	public function getSubmittedLabelledXRayCount($user, $collection)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$collection_id = (int) $collection->id;
		$user_id       = (int) $user->id;

		$dbo->setQuery("SELECT COUNT(DISTINCT `xray`) AS `count`
			            FROM `ax_xray_submission`
			            WHERE `xray` IN (
			            	SELECT `xray`
			            	FROM `ax_xray_collection`
			            	WHERE `id` = ${collection_id})
			            AND `user` = ${user_id};");

		return $dbo->loadObject()->count;
	}

    public function savePostponeAction($action)
    {
        $dbo = $this->getDBO();

        // Escape query parameters
        $user_id       = (int) $action->user->id;
        $collection_id = (int) $action->collection->id;
        $xray_id       = $dbo->quote($action->xray->id);

        try {
			$dbo->transactionStart();

	        $dbo->setQuery("INSERT INTO `ax_pending_xray` (
                                `user`,
                                `xray`)
                            VALUES (
                                ${user_id},
                                ${xray_id});");

			$dbo->execute();
            $dbo->transactionCommit();
        } catch (Exception $e) {
    		$dbo->transactionRollback();
    		return false;
        }

        return true;
    }

	public function saveSubmission($submission)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$user_id       = (int) $submission->user->id;
		$xray_id       = $dbo->quote($submission->xray->id);
		$collection_id = (int) $submission->collection->id;
		$load_time     = (int) $submission->load_time;

		try {
			$dbo->transactionStart();

	        $dbo->setQuery("INSERT INTO `ax_xray_submission` (
	        	                `xray`,
	        	                `collection`,
	        	                `user`,
	        	                `login`,
	        	                `load_time`,
	        	                `submit_time`)
	        	            SELECT ${xray_id},
	        	            	   ${collection_id},
	        	            	   ${user_id},
	        	            	   MAX(`jmt_action_logs`.`id`),
	                               FROM_UNIXTIME(${load_time}),
	                               NOW()
	                        FROM `jmt_action_logs`
	                        WHERE `extension` = 'com_users'
						    AND   `message` REGEXP '\"action\"[[:blank:]]*:[[:blank:]]*\"login\"'
						    AND   `message` REGEXP '\"app\"[[:blank:]]*:[[:blank:]]*\"PLG_ACTIONLOG_JOOMLA_APPLICATION_SITE\"'
						    AND   `user_id` = ${user_id};");

			$dbo->execute();

		    $submission_id = (int) $dbo->insertid();

		    for ($i = 0; $i < count($submission->tags); $i++) {
		    	$annotation = $submission->tags[$i];
		    	$label      = $dbo->quote($annotation->label);
		    	$shapes     = $dbo->quote(json_encode($annotation->handles));

		    	$dbo->setQuery("INSERT INTO `ax_xray_annotation` (`submission`, `label`, `shapes`) VALUES (${submission_id}, ${label}, ${shapes});");
		    	$dbo->execute();
		    }

            // If it was a pending xray, remove it from the list.
            $dbo->setQuery("DELETE FROM `ax_pending_xray` WHERE `user` = ${user_id} AND `xray` = ${xray_id};");
            $dbo->execute();

	        $dbo->transactionCommit();
    	} catch (Exception $e) {
    		$dbo->transactionRollback();
    		return false;
    	}

    	return true;
	}

	public function validateSubmission($submission)
	{
		return true;
	}

    public function validatePostponeAction($submission) {
        return true;
    }
}
