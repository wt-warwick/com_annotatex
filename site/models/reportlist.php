<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * FullList view model
 *
 * @since  0.0.1
 */
class AnnotateXModelReportList extends JModelItem
{
	public function getCollections($user)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$user_id   = (int) $user->id;

		$dbo->setQuery("SELECT `ax_collection`.`id`          AS `id`,
			                   `ax_collection`.`title`       AS `title`,
			                   `ax_collection`.`description` AS `description`
		                FROM `ax_collection`
		                INNER JOIN `ax_assignment`
		                ON `ax_assignment`.`collection` = `ax_collection`.`id`
		                AND `ax_assignment`.`user` = ${user_id}");

		return $dbo->loadObjectList();
	}

	public function getCurrentCollection($user)
	{
		$dbo = $this->getDBO();

		// Escape query parameters
		$user_id   = (int) $user->id;

		$dbo->setQuery("SELECT `ax_collection`.`id`          AS `id`,
			                   `ax_collection`.`title`       AS `title`,
			                   `ax_collection`.`description` AS `description`
	                    FROM `ax_collection`
	                    INNER JOIN `ax_assignment`
	                    ON `ax_assignment`.`collection` = `ax_collection`.`id`
	                    INNER JOIN `ax_user_option`
	                    ON `ax_user_option`.`collection` = `ax_assignment`.`collection`
	                    AND `ax_user_option`.`user` = `ax_assignment`.`user`
	                    WHERE `ax_user_option`.`user` = ${user_id};");

		$row = $dbo->loadObject();

		if (empty($row)) {
			$dbo->setQuery("SELECT `ax_collection`.`id`          AS `id`,
				                   `ax_collection`.`title`       AS `title`,
				                   `ax_collection`.`description` AS `description`
		                    FROM `ax_collection`
		                    INNER JOIN `ax_assignment`
		                    ON `ax_assignment`.`collection` = `ax_collection`.`id`
		                    WHERE `ax_assignment`.`user` = ${user_id}
		                    ORDER BY `ax_collection`.`title`
		                    LIMIT 1;");

			$row = $dbo->loadObject();
		}

		return $row;
	}
}