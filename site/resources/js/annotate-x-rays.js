var onPostponeButtonClick;
var onSubmitButtonClick;
var onPreviousSubmissionButtonClick;

$(function() {
	var imageId    = $('#image-id').val();
	var preloadIds = JSON.parse($('#preload-ids').val() || '[]');

	var xrayboard   = $('#enabled-element');
	var radioboard  = $('.radioboard');
	var switchboard = $('.switchboard');

	xrayboard.height($(window).height() - xrayboard.offset().top);
	xrayboard.width($(window).width() - xrayboard.offset().left);
	xrayboard.xrayboard(imageId, preloadIds);
	xrayboard.on('display', function() {
		var buttons = radioboard.find('.label');
		var state   = [];
		var tags    = JSON.parse($('#latest-submission').val());

		if (!tags) {
			return;
		}

		for (let i = 0; i < tags.length; i++) {
			state.push({
				color: buttons.filter(function(index, element) {
					return $(element).data('label') == tags[i].label;
				}).data('color'),
				handles: {
					start: tags[i].shape.start,
					end: tags[i].shape.end
				},
				label: tags[i].label,
			});
		}

		xrayboard.state(state);
	});

	radioboard.radioboard();
	radioboard.on('select', function(event, button) {
		var color = $(button).data('color');
		var label = $(button).data('label');

		xrayboard.color($(button).data('color'));
		xrayboard.label($(button).data('label'));
		xrayboard.startSelection(color, label);
	});
	radioboard.on('unselect', function(event) {
		xrayboard.stopSelection();
	});
	radioboard.on('button-remove', function(event, button) {
		radioboard.clear();
		xrayboard.removeSelections(function(selection) {
			return selection.label === $(button).data('label');
		});
		xrayboard.stopSelection();
	});
	(function() {
		var labels = $.map(radioboard.getElements(), function(button) {
			return $(button).data('label');
		});
		var labelMap = JSON.parse($('#labels').val());
		var tags = JSON.parse($('#latest-submission').val());

		if (!tags) {
			return;
		}

		for (let i = 0; i < tags.length; i++) {
			if (labels.indexOf(tags[i].label) == -1) {
				labels.push(tags[i].label);
				radioboard.addButton(tags[i].label, labelMap[tags[i].label].title, labelMap[tags[i].label].description);
			}
		}
	})();

	switchboard.switchboard();

	onPostponeButtonClick = function onPostponeButtonClick() {
		$('#postpone-form').submit();
	}

        onPreviousSubmissionButtonClick = function onPreviousSubmissionButtonClick() {
            var xrayId = $('#previous-xray-id').val();
	    var collectionId = $('#previous-collection-id').val();
            var previousSubmissionId = $('#previous-submission-id').val();
            var returnUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;

            window.location = $('#base-link').val() + '&xray_id=' + encodeURIComponent(xrayId)
                                                    + '&collection_id=' + encodeURIComponent(collectionId)
                                                    + "&previous_submission_id=" + encodeURIComponent(previousSubmissionId)
                                                    + "&return_url=" + encodeURIComponent(returnUrl);
        }

	onSubmitButtonClick = function onSubmitButtonClick() {
		var labels = $.map(radioboard.getElements(), function(button) {
			return $(button).data('label');
		});
		var state  = xrayboard.state();

		function isCompleted() {
			for (let i = 0; i < labels.length; i++) {
				let found = false;

				for (let j = 0; j < state.length; j++) {
					if (state[j].label === labels[i]) {
						found = true;
						break;
					}
				}

				if (!found) {
					return false;
				}
			}

			return true;
		}

		if (!isCompleted()) {
			alert('You need to make a selection for each one of the labels.');
			return;
		}

		if (state) {
			for (let i = 0; i < state.length; i++) {
				delete state[i].color;
			}
		}

		$('#tags').val(JSON.stringify(state));
		$('#main-form').submit();
	}

	$(window).on('keyup', function(event) {
		if (event.key === 'Escape') {
			radioboard.clear();
			xrayboard.stopSelection();
		}
	});

	$('#collection').change(function(event) {
        $.ajax({
            data: {
                [$('#token').attr('name')]: 1,
                collection: $('#collection').val(),
                format: 'json',
                task: 'xrays.setcollection'
            },
            success: function(response) {
                window.location.reload();
            },
            error: function(response) {
                console.log(response);
            }
        });
    });

    $('.more').click(function() {
    	var labels = $.map(radioboard.find('.label'), function(button) {
			return $(button).data('label');
		});

    	switchboard.clear();
    	switchboard.enable();
    	switchboard.disable(function(button) {
    		return labels.indexOf($(button).data('label')) !== -1;
    	});

    	$('#more-modal').modal();
    });
    $('#more-modal .ok-button').click(function() {
    	switchboard.find('.active').each(function(index, button) {
			radioboard.addButton($(button).data('label'), $(button).text(), $(button).data('content'));
    	});

    	$('#more-modal').modal('hide');
    });
	$('.info').click(function() {
		$('#info-modal').modal();
	});
});
