$(function() {
    const PAGE_LENGTH = 10;

    var page;
    var search;
    var table;
    var urlParams;

    urlParams = new URLSearchParams(window.location.search);
    page = urlParams.get('page') || 1;
    page = Number.isInteger(Number(page)) && page > 0 ? page : 1;
    search = urlParams.get('search') || '';

    table = $('#main-table').DataTable({
        ajax: function(data, callback, settings) {
            $.ajax({
                data: {
                    [$('#token').attr('name')]: 1,
                    contains: data.search.value,
                    format: 'json',
                    size: data.length,
                    start: data.start,
                    task: 'xrays.list'
                },
                success: function(response) {
                    callback({
                        draw: data.draw,
                        data: $.map(response.data.xrays, function(xray, i) {
                            return [['<button class="btn btn-primary btn-sm fa fa-pencil" data-xray="' + xray.id.xray + '" data-collection="' + xray.id.collection + '"></button>',
                                     xray.id.xray,
                                     (function() {
                                         var i;
                                         var text = '';

                                         if (xray.labels.length) {
                                             for (i = 0; i < xray.labels.length; i++) {
                                                 text += '<span class="badge badge-secondary">' + xray.labels[i] + '</span> ';
                                             }

					     text += '<div></div>';
                                         }
					 
					 if (xray.submission.length) {
					     for (i = 0; i < xray.submission.length; i++) {
						 text += '<span class="badge badge-primary">' + xray.submission[i] + '</span> ';
					     }
					 }

                                        return text;
                                     })(),
                                     (function() {
                                        switch (parseInt(xray.n_submissions || 0)) {
                                            case -1: return '<i class="fa fa-calendar" style="color:#ff8a65"></i>';
                                            case  0:  return '';
                                            case  1:  return '<i class="fa fa-check" style="color:#007bff"></i>';
                                            default: return '<span class="fa-stack" style="color:#007bff">' +
                                                                '<i class="fa fa-check fa-stack-1x" style="margin-left:4px"></i>' +
                                                                '<i class="fa fa-check fa-inverse fa-stack-1x" style="margin-left:-3px;"></i>' +
                                                                '<i class="fa fa-check  fa-stack-1x" style="margin-left:-4px"></i>' +
                                                            '</span>';
                                        }
                                     })(),
                                     xray.last_submit || null]];
                        }),
                        recordsTotal: response.data.total,
                        recordsFiltered: response.data.filtered
                    });

                    $('#main-table').find('button').click(function() {
                    	var xrayId       = encodeURIComponent($(this).data('xray'));
                        var collectionId = $(this).data('collection');
                    	var returnUrl    = encodeURIComponent(window.location.protocol + '//' + window.location.host + window.location.pathname + '?page=' + page + '&search=' + search);
                        
                        window.location = $('#base-link').val() + '&xray_id=' + xrayId + "&collection_id=" + collectionId + "&return_url=" + returnUrl;

                        return false;
                    });
                },
                error: function(response) {
                    console.log(response);
                }
            });
        },
        columns: [
            {className: 'text-center align-middle'},
            {className: 'text-left align-middle'},
            {className: 'text-left align-middle'},
            {className: 'text-center align-middle'},
            {className: 'text-center align-middle'}
        ],
        displayStart: (page - 1) * PAGE_LENGTH,
        ordering: false,
        pageLength: PAGE_LENGTH,
	sPaginationType: 'listbox',
        sDom: '<"top"f>rt<"bottom"p>',
        search: { search: search },
        serverSide: true
    });

    $('#main-table').on('page.dt search.dt', function(event) {
	    page = table.page() + 1;
        search = table.search();
	});

    $('#collection').change(function(event) {
        $.ajax({
            data: {
                [$('#token').attr('name')]: 1,
                collection: $('#collection').val(),
                format: 'json',
                task: 'xrays.setcollection'
            },
            success: function(response) {
                window.location.reload();
            },
            error: function(response) {
                console.log(response);
            }
        });
    });
});

$.fn.dataTableExt.oPagination.listbox = {
    "fnInit": function (oSettings, nPaging, fnCallbackDraw) {
        var nInput = document.createElement('select');
        var nPage = document.createElement('span');
        var nOf = document.createElement('span');
        nOf.className = "paginate_of";
        nPage.className = "paginate_page";
        if (oSettings.sTableId !== '') {
            nPaging.setAttribute('id', oSettings.sTableId + '_paginate');
        }
        nInput.style.display = "inline";
        nPage.innerHTML = "Page ";
        nPaging.appendChild(nPage);
        nPaging.appendChild(nInput);
        nPaging.appendChild(nOf);
        $(nInput).change(function (e) {
            window.scroll(0,0);
            if (this.value === "" || this.value.match(/[^0-9]/)) {
                return;
            }
            var iNewStart = oSettings._iDisplayLength * (this.value - 1);
            if (iNewStart > oSettings.fnRecordsDisplay()) {
                oSettings._iDisplayStart = (Math.ceil((oSettings.fnRecordsDisplay() - 1) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
                fnCallbackDraw(oSettings);
                return;
            }
            oSettings._iDisplayStart = iNewStart;
            fnCallbackDraw(oSettings);
        });
        $('span', nPaging).bind('mousedown', function () {
            return false;
        });
        $('span', nPaging).bind('selectstart', function () {
            return false;
        });
    },
      
    "fnUpdate": function (oSettings, fnCallbackDraw) {
        if (!oSettings.aanFeatures.p) {
            return;
        }
        var iPages = Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength);
        var iCurrentPage = Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
        var an = oSettings.aanFeatures.p;
        for (var i = 0, iLen = an.length; i < iLen; i++) {
            var spans = an[i].getElementsByTagName('span');
            var inputs = an[i].getElementsByTagName('select');
            var elSel = inputs[0];
            if(elSel.options.length != iPages) {
                elSel.options.length = 0;
                for (var j = 0; j < iPages; j++) {
                    var oOption = document.createElement('option');
                    oOption.text = j + 1;
                    oOption.value = j + 1;
                    try {
                        elSel.add(oOption, null);
                    } catch (ex) {
                        elSel.add(oOption); // IE only
                    }
                }
                spans[1].innerHTML = "&nbsp;of&nbsp;" + iPages;
            }
          elSel.value = iCurrentPage;
        }
    }
};
