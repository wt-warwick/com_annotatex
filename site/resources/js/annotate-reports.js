var onFullListButtonClick;
var onPreviousButtonClick;
var onResetLabelsButtonClick;
var onRestartButtonClick;
var onSubmitButtonClick;

$(function() {
    var loadTime;
    var current = null;

    var selectable;
    var banner;
    var switchboard;

    var latestSubmission = $('#latest-submission').val() ? JSON.parse($('#latest-submission').val()) : null;
    var expectedLabels = $('#expected-labels').val() ? JSON.parse($('#expected-labels').val()) : null;

    var downArrow    = '<i class="fa fa-mail-forward arrow-down"></i>';
    var instructions = '<span>To start, select a piece of text <mark class="selection">from above</mark><i class="fa fa-i-cursor cursor"></i><i class="fa fa-mail-forward arrow-up"></i></span>';

    onPreviousButtonClick = function() {
        var reportId = $('#previous-report-id').val();
        var previousSubmissionId = $('#previous-submission-id').val();
        var returnUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;

        window.location = $('#base-link').val() + '&report_id=' + encodeURIComponent(reportId)
                                                + "&previous_submission_id=" + encodeURIComponent(previousSubmissionId)
                                                + "&return_url=" + encodeURIComponent(returnUrl);
    };

    onResetLabelsButtonClick = function() {
        if (current) {
            selectable.unmark(current.data('id'));
            banner.element($(instructions));
            switchboard.clear();
            current = null;
        }
    };

    onRestartButtonClick = function() {
        selectable.unmark();
        banner.element($(instructions));
        switchboard.clear();
        switchboard.disable();
        current = null;
    };

    onSubmitButtonClick = function() {
        var annotation = [];
        var reviewed = true;

        current = null;
        banner.element($(instructions));
        switchboard.clear();
        switchboard.disable();

        selectable.unmark(function(mark) {
            return !$(mark).data('labels') || !$(mark).data('labels').length;
        });
        selectable.getElements().each(function(index, mark) {
            if (selectable.isDisabled($(mark).data('id'))) {
                reviewed = false;
            }

            annotation.push([$(mark).text().trim(), $(mark).data('labels')]);
        });

        if (!reviewed) {
            alert("You should review all the auto-generated labels.");
            return;
        }

        if (annotation.length == 0) {
            alert("You can't submit a non-annotated report.");
            return;
        }

        $('#main-form #tags').val(JSON.stringify(annotation));
        $('#main-form').submit();
    };

    $(function() {
        var currentTarget = null;
        var popoverTarget = null;

        selectable  = $('.report').selectable();
        banner      = $('.instructions').banner();
        switchboard = $('.switchboard').switchboard();

        function digest(mark) {
            var digest = $(mark).clone().css({backgroundColor: $(mark).data('color')});

            if (digest.text().length > 48) {
                digest.text($(mark).text().substring(0, 43).trim() + '[...]');
            }

            return $('<span>You selected </span>').add(digest).add('<span>; now, click the labels below.</span>');
        }

        function explain() {
            if (currentTarget && $(currentTarget).data('toggle') === 'popover') {
                popoverTarget = currentTarget;
                jQuery(currentTarget).popover('toggle');
            }
        }

        function save() {
            onSubmitButtonClick();
        }

        banner.element($(instructions));
        switchboard.disable();

        selectable.on('select', function(event, mark, id) {
            current = $(mark);
            banner.element(digest(current).add($(downArrow)));
            switchboard.enable();
            switchboard.color(current.data('color'));
            switchboard.clear();
        });
        selectable.on('mousedown', function() {
            selectable.unmark(function(mark) {
                return !$(mark).data('labels') || !$(mark).data('labels').length;
            });
            banner.element($(instructions));
            switchboard.clear();
            switchboard.disable();
            current = null;
        });
        selectable.on('mark-click', function(event, mark, id) {
            current = $(mark);
            banner.element(digest(current).add($(downArrow)));
            switchboard.clear();
            switchboard.color(current.data('color'));
            switchboard.enable();
            switchboard.activate(function(button) {
                return current.data('labels') && current.data('labels').indexOf($(button).data('label')) > -1;
            });
        });
        switchboard.on('change', function (event, button) {
            var labels = current.data('labels') || [];

            if ($(button).hasClass('active')) {
                labels.push($(button).data('label'));
            } else {
                labels.splice(labels.indexOf($(button).data('label')), 1);
            }

            current.data('labels', labels);
        });

        $(window).keydown(function(event) {
            var c;

            if (event.ctrlKey || event.metaKey) {
                c = String.fromCharCode(event.which).toLowerCase();

                if (c === 'v') {
                    explain();
                    event.preventDefault();
                    return false;
                } else if (c === 's') {
                    save();
                    event.preventDefault();
                    return false;
                }
            }

            return true;
        });
        $(window).mousemove(function(event) {
            currentTarget = event.target;

            if (popoverTarget) {
                jQuery(popoverTarget).popover('hide');
                popoverTarget = null;
            }

            return true;
        });

        (function initLabels() {
            var disabled = !latestSubmission && expectedLabels;
            var end;
            var i;
            var annotation = latestSubmission ? latestSubmission : expectedLabels ? expectedLabels : null;
            var start;

            if (annotation) {
                for (i = 0; i < annotation.length; i++) {
                    start = selectable.content().indexOf(annotation[i][0]);
                    end = start + annotation[i][0].length;

                    if (end - start) {
                        selectable.mark(start, end, {
                            labels: annotation[i][1]
                        }, disabled ? true : false);
                    }
                }
            }
        })();
    });

    $('#collection').change(function(event) {
        $.ajax({
            data: {
                [$('#token').attr('name')]: 1,
                collection: $('#collection').val(),
                format: 'json',
                task: 'reports.setcollection'
            },
            success: function(response) {
                window.location.reload();
            },
            error: function(response) {
                console.log(response);
            }
        });
    });
});
