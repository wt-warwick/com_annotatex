$.fn.radioboard = function radioboard() {
	var self = this;

	var colors  = ['#e57373', '#4db6ac', '#ba68c8', '#90ca4d', '#7986cb', '#ffd54f', '#ff8a65'];
	var color   = 0; 
	var element = $(this);

	function __construct() {
		element.find('.label').each(function(i, element) {
			var color = nextColor();

			$(this).css({backgroundColor: color});
			$(this).data('color', color);
		});
		addDOMCallbacks(element.find('.label'));
	}

	self.addButton = function addButton(label, text, description) {
		var color    = nextColor();
		var content  = ('&lt;ul&gt;&lt;li&gt;' + description.split('#').join('&lt;/li&gt;&lt;li&gt;') + '&lt;/li&gt;&lt;/ul&gt;').replace(/"/g, "&quot;").replace(/'/g, "&#039;");
		var textNode = $(document.createTextNode(' '));

		button = $(`<button class="btn btn-default label"
			                data-color="${color}"
			                data-content="${content}"
			                data-delay="{&quot;show&quot;:0,&quot;hide&quot;:0}"
		                    data-html="true"
		                    data-label="${label}"
		                    data-placement="bottom"
		                    data-toggle="popover"
		                    data-trigger="manual"
			                type="button">${text}<i class="delete fa fa-close"></i></button>`);

		button.css({backgroundColor: color});

		if (self.find('.label').length) {
			self.find('.label').last().after(textNode);
			textNode.after(button);
		} else {
			self.find('.more').parent().prepend(button);
			button.after(textNode);
		}

		addDOMCallbacks(button);

		return this;
	}

	function addDOMCallbacks(buttons) {
		buttons.click(function(event) {
			var button = $(this);

			if (button.hasClass('selected')) {
				button.removeClass('selected');
				self.trigger('unselect', this);
			} else {
				element.find('.label').removeClass('selected');
				button.addClass('selected');
				self.trigger('select', this);
			}
		});
		buttons.find('.delete').click(function() {
			var button = $(this).parent('button');

			button.remove();
			self.trigger('button-remove', button);
		});
	}

	self.clear = function clear() {
		element.find('.label').each(function(i, button) {
			$(button).removeClass('selected');
		});
	}

	self.getElements = function getElements() {
		return self.find('.label');
	}

	function nextColor() {
		var i;
		var min = ['transparent', Number.MAX_VALUE];
		var prop;
		var usage = {};

		for (i = 0; i < colors.length; i++) {
			usage[colors[i]] = 0;
		}

		self.getElements().each(function(index, button) {
			usage[$(button).data('color')]++;
		});

		for (prop in usage) {
			if (usage[prop] < min[1]) {
				min[0] = prop;
				min[1] = usage[prop];
			}
		}

		return min[0];
	}

	__construct();
	return self;
};