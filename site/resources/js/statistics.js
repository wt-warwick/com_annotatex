function AverageTimeChart(canvas, token, userId) {
	var self = this;

	var chart;
	var data;

	function construct() {
		data = { labels: (function() {
			var array = [];
			var i;

			for (i = 0; i < 1000; i += 50) {
				array.push(i);
			}

			return array;
		})()};
		chart = new Chart(canvas.get(0).getContext('2d'), {
			type: 'line',
			data: data
		});

		self.refresh(token, userId);
	}

	self.refresh = function refresh(token, userId) {
		$.ajax({
            data: {
                [token]: 1,
                format: 'json',
                task: 'statistics.averagetimeperreportlength',
                userId: userId
            },
            success: function(response) {
            	var i;
            	var others = {};
            	var user   = {};

            	response = response.data;

            	for (i = 0; i < response.user.length; i++) {
            		user[response.user[i].length_class.toString()] = response.user[i].average_time;
            	}

            	for (i = 0; i < response.others.length; i++) {
					others[response.others[i].length_class.toString()] = response.others[i].average_time;
            	}

            	data.datasets = [];
            	data.datasets.push({
					label: 'User',
					backgroundColor: '#007bff',
					borderColor: '#007bff',
					data: (function() {
						var array = [];
						var i;

						for (i = 0; i < 1000; i += 50) {
							array.push(user[i.toString()] || null);
						}

						return array;
					})(),
					fill: false,
					tooltips: false
				});
				data.datasets.push({
					label: 'Others',
					backgroundColor: '#e64d00',
					borderColor: '#e64d00',
					data: (function() {
						var array = [];
						var i;

						for (i = 0; i < 1000; i += 50) {
							array.push(others[i.toString()] || null);
						}

						return array;
					})(),
					fill: false,
					tooltips: false
				});
				chart.update();
            },
            error: function(response) {
            	console.log(response);
            }
        });
	}

	construct();
}

function AgreementByLabelChart(canvas, token, users, userIds) {
	var self = this;

	var chart;
	var colors = ['#4db6ac', '#e57373', '#ba68c8', '#90ca4d', '#7986cb', '#ffd54f', '#ff8a65'];
	var data;

	function construct() {
		data = {
			labels: ['Abn. Non Clinic. Important',
				 'Airspace Opacification',
			         'Aortic Calcification',
				 'Apical Fibrosis',
			         'Atelectasis',
			         'Axillary Abnormality',
			         'Bone Lesion',
			         'Bronchial Changes',
			         'Bulla',
			         'Cardiomegaly',
			         'Cavitating Lung Lesion',
			         'Clavicle Fracture',
			         'Comparison',
			         'Coronary Calcification',
			         'Dextrocardia',
			         'Dilated Bowel',
			         'Emphysema',
			         'Ground Glass Opacification',
			         'Hemidiaphragm elevated',
			         'Hernia',
			         'Hyperexpanded Lungs',
			         'Interstitial Shadowing',
			         'Left Lower Lobe Collapse',
			         'Left Upper Lobe Collapse',
			         'Mediastinum Displaced',
			         'Medical Devices',
			         'Normal',
			         'Other Non-Findings',
				 'Paraspinal Mass',
			         'Paratracheal Hilar Enlarg.',
			         'Parenchymal Lesion',
			         'Pleural Abnormality',
			         'Pleural Effusion',
			         'Pneumomediastinum',
			         'Pneumoperitoneum',
			         'Pnemothorax',
			         'Possible diagnosis',
			         'Recommendation',
			         'Rib Fracture',
		         	 'Right Lower Lobe Collapse',
		         	 'Right Middle Lobe Collapse',
		         	 'Right Upper Lobe Collapse',
		         	 'Scoliosis',
		         	 'Subcutaneous Emphysema',
		         	 'Technical Issue',
		         	 'Undefined Sentence',
		         	 'Unfolded Aorta',
				 'Upper Lobe Blood Diversion',
				 'Volume Loss',
			         'Widened Mediastinum',],
		};
		chart = new Chart(canvas.get(0).getContext('2d'), {
			type: 'horizontalBar',
			data: data,
		});

		self.refresh(token, userIds);
	}

	self.refresh = function refresh(token, userIds) {
		$.ajax({
            data: {
                [token]: 1,
                format: 'json',
                task: 'statistics.agreementbylabel',
				userIds: userIds
            },
            success: function(response) {
				response = response.data;
            	data.datasets = (function() {
					var indexes = ['agreement'].concat((userIds));
					var result = [];

					for (let i = 0; i < indexes.length; i++) {
						result.push({
							label: (function() {
								if (indexes[i] == 'agreement') {
									return 'Agreement';
								}

								for (let j = 0; j < users.length; j++) {
									if (users[j].id == indexes[i]) {
										return users[j].name;
									}
								}
							})(),
							backgroundColor: colors[i % colors.length],
							borderColor: colors[i % colors.length],
							borderWidth: 1,
							data: [
								response.abnormal_non_clinically_important[indexes[i]],
								response.consolidation[indexes[i]],
								response.aortic_calcification[indexes[i]],
							        response.apical_fibrosis[indexes[i]],
								response.atelectasis[indexes[i]],
								response.axillary_abnormality[indexes[i]],
								response.rib_lesion[indexes[i]],
								response.bronchial_wall_thickening[indexes[i]],
								response.bulla[indexes[i]],
								response.cardiomegaly[indexes[i]],
								response.cavitating_lung_lesion[indexes[i]],
								response.clavicle_fracture[indexes[i]],
								response.comparison[indexes[i]],
								response.coronary_calcification[indexes[i]],
								response.dextrocardia[indexes[i]],
								response.dilated_bowel[indexes[i]],
								response.emphysema[indexes[i]],
								response.ground_glass_opacification[indexes[i]],
								response.hemidiaphragm_elevated[indexes[i]],
								response.hernia[indexes[i]],
								response.hyperexpanded_lungs[indexes[i]],
								response.interstitial_shadowing[indexes[i]],
								response.left_lower_lobe_collapse[indexes[i]],
								response.left_upper_lobe_collapse[indexes[i]],
								response.mediastinum_displaced[indexes[i]],
								response.object[indexes[i]],
								response.normal[indexes[i]],
							        response.other[indexes[i]],
							        response.paraspinal_mass[indexes[i]],
								response.paratracheal_hilar_enlargement[indexes[i]],
								response.parenchymal_lesion[indexes[i]],
								response.pleural_abnormality[indexes[i]],
								response.pleural_effusion[indexes[i]],
								response.pneumomediastinum[indexes[i]],
								response.pneumoperitoneum[indexes[i]],
								response.pneumothorax[indexes[i]],
								response.possible_diagnosis[indexes[i]],
								response.recommendation[indexes[i]],
								response.rib_fracture[indexes[i]],
								response.right_lower_lobe_collapse[indexes[i]],
								response.right_middle_lobe_collapse[indexes[i]],
								response.right_upper_lobe_collapse[indexes[i]],
								response.scoliosis[indexes[i]],
								response.subcutaneous_emphysema[indexes[i]],
								response.technical_issue[indexes[i]],
								response.undefined_sentence[indexes[i]],
							        response.unfolded_aorta[indexes[i]],
							        response.upper_lobe_blood_diversion[indexes[i]],
							        response.volume_loss[indexes[i]],
								response.mediastinum_widened[indexes[i]],
							]
						});
					}

					return result;
				})();

				chart.update();
            },
            error: function(response) {
            	console.log(response);
            }
        })
	};

	construct();
}

function ReportsInDisagreementTable(element, token_, userId_) {
	var self = this;

	var table;
	var token = token_;
	var userId = userId_;

	function construct() {
		table = element.DataTable({
		    ajax: function(data, callback, settings) {
		        $.ajax({
		            data: {
		                [token]: 1,
		                format: 'json',
		                size: data.length,
		                start: data.start,
		                task: 'statistics.reportsindisagreement',
		                userId: userId
		            },
		            success: function(response) {
		                callback({
		                    draw: data.draw,
		                    data: $.map(response.data.reports, function(report, i) {
		                        return [[report.id,
		                                 report.content,
		                                 report.count]];
		                    }),
		                    recordsTotal: response.data.report_count,
		                    recordsFiltered: response.data.report_count
		                });
		            },
		            error: function(response) {
		            	console.log(response);
		            }
		        });
		    },
		    columns: [
		    	{className: 'text-left'},
		    	{className: 'text-left'},
		    	{className: 'text-center'},
		    ],
		    ordering: false,
		    pageLength: 5,
		    sDom: 't<"bottom"p>',
		    serverSide: true
		});
	}

	self.refresh = function refresh(token_, userId_) {
		token  = token_;
		userId = userId_;

		table.ajax.reload();
	}

	construct();
}

function SubmissionsPerDayChart(canvas, token, userId) {
	var self = this;

	var chart;
	var color = '#007bff';
	var data;

	function construct() {
		data = {
			labels: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
		};
		chart = new Chart(canvas.get(0).getContext('2d'), {
			type: 'bar',
			data: data
		});

		self.refresh(token, userId);
	}

	self.refresh = function refresh(token, userId) {
		$.ajax({
            data: {
                [token]: 1,
                format: 'json',
                task: 'statistics.submissionsperdayoftheweek',
                userId: userId
            },
            success: function(response) {
            	response = $.extend({sunday: 0, monday: 0, tuesday: 0, wednesday: 0, thursday: 0, friday: 0, saturday: 0}, response.data);;

            	data.datasets = [{
					label: 'Submissions',
					backgroundColor: color,
					borderColor: color,
					borderWidth: 1,
					data: [	response.sunday, response.monday, response.tuesday, response.wednesday, response.thursday, response.friday, response.saturday]
				}];
				chart.update();
            },
            error: function(response) {
            	console.log(response);
            }
        })
	}

	construct();
}

function SubmissionsPerSessionTable(element, token_, userId_) {
	var self = this;

	var table;
	var token = token_;
	var userId = userId_;

	function construct() {
		table = element.DataTable({
		    ajax: function(data, callback, settings) {
		        $.ajax({
		            data: {
		                [token]: 1,
		                format: 'json',
		                size: data.length,
		                start: data.start,
		                task: 'statistics.submissionspersession',
		                userId: userId
		            },
		            success: function(response) {
		                callback({
		                    draw: data.draw,
		                    data: $.map(response.data.sessions, function(session, i) {
		                        return [[session.date,
		                                 session.login_time,
		                                 session.last_action_time,
		                                 session.duration,
		                                 session.submissions]];
		                    }),
		                    recordsTotal: response.data.session_count,
		                    recordsFiltered: response.data.session_count
		                });
		            },
		            error: function(response) {
		            	console.log(response);
		            }
		        });
		    },
		    columns: [
		    	{className: 'text-center'},
		    	{className: 'text-center'},
		    	{className: 'text-center'},
		    	{className: 'text-right'},
		    	{className: 'text-right'},
		    ],
		    ordering: false,
		    pageLength: 5,
		    sDom: 't<"bottom"p>',
		    serverSide: true
		});
	}

	self.refresh = function refresh(token_, userId_) {
		token  = token_;
		userId = userId_;

		table.ajax.reload();
	}

	construct();
}

function LabelDistributionChart(canvas, token, userId) {
	var self = this;

	var chart;
	var color = '#007bff';
	var data;

	function construct() {
		data = {
			labels: ['Abn. Non Clinic. Important',
				 'Airspace Opacification',
			         'Aortic Calcification',
				 'Apical Fibrosis',
			         'Atelectasis',
			         'Axillary Abnormality',
			         'Bone Lesion',
			         'Bronchial Changes',
			         'Bulla',
			         'Cardiomegaly',
			         'Cavitating Lung Lesion',
			         'Clavicle Fracture',
			         'Comparison',
			         'Coronary Calcification',
			         'Dextrocardia',
			         'Dilated Bowel',
			         'Emphysema',
			         'Ground Glass Opacification',
			         'Hemidiaphragm elevated',
			         'Hernia',
			         'Hyperexpanded Lungs',
			         'Interstitial Shadowing',
			         'Left Lower Lobe Collapse',
			         'Left Upper Lobe Collapse',
			         'Mediastinum Displaced',
			         'Medical Devices',
			         'Normal',
			         'Other Non-Findings',
				 'Paraspinal Mass',
			         'Paratracheal Hilar Enlarg.',
			         'Parenchymal Lesion',
			         'Pleural Abnormality',
			         'Pleural Effusion',
			         'Pneumomediastinum',
			         'Pneumoperitoneum',
			         'Pnemothorax',
			         'Possible diagnosis',
			         'Recommendation',
			         'Rib Fracture',
		         	 'Right Lower Lobe Collapse',
		         	 'Right Middle Lobe Collapse',
		         	 'Right Upper Lobe Collapse',
		         	 'Scoliosis',
		         	 'Subcutaneous Emphysema',
		         	 'Technical Issue',
		         	 'Undefined Sentence',
		         	 'Unfolded Aorta',
				 'Upper Lobe Blood Diversion',
				 'Volume Loss',
			         'Widened Mediastinum',],
		};
		chart = new Chart(canvas.get(0).getContext('2d'), {
			type: 'horizontalBar',
			data: data
		});

		self.refresh(token, userId);
	}

	self.refresh = function refresh(token, collectionId) {
		$.ajax({
            data: {
                [token]: 1,
                format: 'json',
                task: 'statistics.labeldistribution',
                collectionId: collectionId
            },
            success: function(response) {
            	response = $.extend({
		    abnormal_non_clinically_important:0,
		    consolidation:0,
		    aortic_calcification:0,
		    apical_fibrosis:0,
		    atelectasis:0,
		    axillary_abnormality:0,
		    rib_lesion:0,
		    bronchial_wall_thickening:0,
		    bulla:0,
		    cardiomegaly:0,
		    cavitating_lung_lesion:0,
		    clavicle_fracture:0,
		    comparison:0,
		    coronary_calcification:0,
		    dextrocardia:0,
		    dilated_bowel:0,
		    emphysema:0,
		    ground_glass_opacification:0,
		    hemidiaphragm_elevated:0,
		    hernia:0,
		    hyperexpanded_lungs:0,
		    interstitial_shadowing:0,
		    left_lower_lobe_collapse:0,
		    left_upper_lobe_collapse:0,
		    mediastinum_displaced:0,
		    object:0,
		    normal:0,
		    other:0,
		    paraspinal_mass:0,
		    paratracheal_hilar_enlargement:0,
		    parenchymal_lesion:0,
		    pleural_abnormality:0,
		    pleural_effusion:0,
		    pneumomediastinum:0,
		    pneumoperitoneum:0,
		    pneumothorax:0,
		    possible_diagnosis:0,
		    recommendation:0,
		    rib_fracture:0,
		    right_lower_lobe_collapse:0,
		    right_middle_lobe_collapse:0,
		    right_upper_lobe_collapse:0,
		    scoliosis:0,
		    subcutaneous_emphysema:0,
		    technical_issue:0,
		    undefined_sentence:0,
		    unfolded_aorta:0,
		    upper_lobe_blood_diversion:0,
		    volume_loss:0,
		    mediastinum_widened:0,
            	}, response.data);;

            	data.datasets = [{
					label: 'Occurrences',
					backgroundColor: color,
					borderColor: color,
					borderWidth: 1,
					data: [
					    response.abnormal_non_clinically_important,
					    response.consolidation,
					    response.aortic_calcification,
					    response.apical_fibrosis,
					    response.atelectasis,
					    response.axillary_abnormality,
					    response.rib_lesion,
					    response.bronchial_wall_thickening,
					    response.bulla,
					    response.cardiomegaly,
					    response.cavitating_lung_lesion,
					    response.clavicle_fracture,
					    response.comparison,
					    response.coronary_calcification,
					    response.dextrocardia,
					    response.dilated_bowel,
					    response.emphysema,
					    response.ground_glass_opacification,
					    response.hemidiaphragm_elevated,
					    response.hernia,
					    response.hyperexpanded_lungs,
					    response.interstitial_shadowing,
					    response.left_lower_lobe_collapse,
					    response.left_upper_lobe_collapse,
					    response.mediastinum_displaced,
					    response.object,
					    response.normal,
					    response.other,
					    response.paraspinal_mass,
					    response.paratracheal_hilar_enlargement,
					    response.parenchymal_lesion,
					    response.pleural_abnormality,
					    response.pleural_effusion,
					    response.pneumomediastinum,
					    response.pneumoperitoneum,
					    response.pneumothorax,
					    response.possible_diagnosis,
					    response.recommendation,
					    response.rib_fracture,
					    response.right_lower_lobe_collapse,
					    response.right_middle_lobe_collapse,
					    response.right_upper_lobe_collapse,
					    response.scoliosis,
					    response.subcutaneous_emphysema,
					    response.technical_issue,
					    response.undefined_sentence,
					    response.unfolded_aorta,
					    response.upper_lobe_blood_diversion,
					    response.volume_loss,
					    response.mediastinum_widened,
					]
				}];
				chart.update();
            },
            error: function(response) {
            	console.log(response);
            }
        })
	};

	construct();
}

$(function () {
	var token        = $('#token').attr('name');
	var collectionId = $('#collection-combo').val();
    var userId       = $('#user-combo').val();
	var users        = JSON.parse($('#users').val());

	var avgChart      = new AverageTimeChart($('#average-time-per-report-length'), token, userId);
	var dayChart      = new SubmissionsPerDayChart($('#submissions-per-day-of-week'), token, userId);
	var agrLabelChart = new AgreementByLabelChart($('#agreement-by-label'), token, users, []);
	var disagTable    = new ReportsInDisagreementTable($('#reports-in-disagreement'), token, userId);
	var labelChart    = new LabelDistributionChart($('#label-distribution'), token, collectionId);
	var table         = new SubmissionsPerSessionTable($('#submissions-per-session'), token, userId);

    $('#user-combo').change(function() {
    	var userId = $('#user-combo').val();

    	table.refresh(token, userId);
    	avgChart.refresh(token, userId);
    	dayChart.refresh(token, userId);
    });

	$('#agreement-by-label-combo').on('change', function() {
		agrLabelChart.refresh(token, $(this).val());
	});

    $('#collection-combo').change(function() {
	    labelChart.refresh(token, $('#collection-combo').val());
    });

    $('[data-toggle="popover"]').popover();
});
