const BaseAnnotationTool = cornerstoneTools.import('base/BaseAnnotationTool');
const getNewContext      = cornerstoneTools.import('drawing/getNewContext');
const draw               = cornerstoneTools.import('drawing/draw');
const drawHandles        = cornerstoneTools.import('drawing/drawHandles');
const drawRect           = cornerstoneTools.import('drawing/drawRect');
const fillBox            = cornerstoneTools.import('drawing/fillBox');
const setShadow          = cornerstoneTools.import('drawing/setShadow');

_SelectorToolConfiguration = {
    color: '#fff',
    label: '',
}

class SelectorTool extends BaseAnnotationTool {
    constructor(props = {}) {
        const defaultProps = {
            name: 'Selector',
            supportedInteractionTypes: ['Mouse', 'Touch'],
            configuration: {},
        };

        super(props, defaultProps);
    }

    createNewMeasurement(event) {
        if (!(event && event.currentPoints && event.currentPoints.image)) {
            logger.error(`required event not supplied to tool ${this.name}'s createNewMeasurement`);
            return;
        }

        return {
            visible: true,
            active: true,
            color: _SelectorToolConfiguration.color,
            label: _SelectorToolConfiguration.label,
            handles: {
                start: {
                    x: event.currentPoints.image.x,
                    y: event.currentPoints.image.y,
                    active: false,
                },
                end: {
                    x: event.currentPoints.image.x,
                    y: event.currentPoints.image.y,
                    active: true,
                },
                initialRotation: event.viewport.rotation,
            },
        };
    }

    getPixelSpacing(image) {
        const imagePlane = cornerstone.metaData.get('imagePlaneModule', image.imageId);

        if (imagePlane) {
            return {
                rowPixelSpacing: imagePlane.rowPixelSpacing || imagePlane.rowImagePixelSpacing,
                colPixelSpacing: imagePlane.columnPixelSpacing || imagePlane.colImagePixelSpacing,
            };
        }

        return {
            rowPixelSpacing: image.rowPixelSpacing,
            colPixelSpacing: image.columnPixelSpacing,
        };
    }

    pointNearTool(element, data, coords, interactionType) {
        const hasStartAndEndHandles = data && data.handles && data.handles.start && data.handles.end;

        if (!hasStartAndEndHandles) {
            logger.warn(`invalid parameters supplied to tool ${this.name}'s pointNearTool`);
        }

        if (!hasStartAndEndHandles || data.visible === false) {
            return false;
        }

        const startCanvas = cornerstone.pixelToCanvas(element, data.handles.start);
        const endCanvas = cornerstone.pixelToCanvas(element, data.handles.end);
        const rect = {
            left:   Math.min(startCanvas.x, endCanvas.x),
            top:    Math.min(startCanvas.y, endCanvas.y),
            width:  Math.abs(startCanvas.x - endCanvas.x),
            height: Math.abs(startCanvas.y - endCanvas.y),
        };
        const distanceToPoint = cornerstoneMath.rect.distanceToPoint(rect, coords);

        return distanceToPoint < (interactionType === 'mouse' ? 15 : 25);
    }

    renderToolData(evt) {
        const toolData = cornerstoneTools.getToolState(evt.currentTarget, this.name);

        if (!toolData) {
            return;
        }

        const eventData                            = evt.detail;
        const { image, element }                   = eventData;
        const lineWidth                            = cornerstoneTools.toolStyle.getToolWidth();
        const { handleRadius, drawHandlesOnHover } = this.configuration;
        const context                              = getNewContext(eventData.canvasContext.canvas);
        const { rowPixelSpacing, colPixelSpacing } = this.getPixelSpacing(image);

        const seriesModule                         = cornerstone.metaData.get('generalSeriesModule', image.imageId) || {};

        const modality                             = seriesModule.modality;
        const hasPixelSpacing                      = rowPixelSpacing && colPixelSpacing;

        const configuration = this.configuration;
        
        draw(context, context => {
            for (let i = 0; i < toolData.data.length; i++) {
                const data = toolData.data[i];

                if (data.visible === false) {
                    continue;
                }

                const handleOptions = {
                    color: data.color,
                    drawHandlesIfActive: drawHandlesOnHover,
                    handleRadius,
                    lineWidth: 2,
                };

                setShadow(context, configuration);

                drawRect(context, element, data.handles.start, data.handles.end, { color: data.color, lineWidth: 2 }, 'pixel', data.handles.initialRotation);
                drawHandles(context, eventData, data.handles, handleOptions);
            }
        });
    }
}
