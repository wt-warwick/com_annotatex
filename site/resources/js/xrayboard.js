$.fn.xrayboard = function xrayboard(imageId, preloadIds) {
	var self = this;

	var element = $(this);
	var state;
	var tools   = cornerstoneTools;
	var wado    = cornerstoneWADOImageLoader;
	var web     = cornerstoneWebImageLoader;

	function __construct() {
		initCornerstone();
		addTools();
		addDOMCallbacks();

		state = 'not-selecting';
	}

	function addDOMCallbacks() {
		var ctrlPressed = false;

		element.on('contextmenu', function(event) {
			event.preventDefault();
	        event.stopPropagation();
	        event.stopImmediatePropagation();
			return false;
		});

		(function() {
			var switchingState = false;

			$(window).on("keydown", function(event) {
				if (!switchingState && event.shiftKey && state == 'selecting') {
					switchingState = true;
					self.stopSelection();
				}
			});

			$(window).on("keyup", function(event) {
				if (switchingState && !event.shiftKey) {
					switchingState = false;
					self.startSelection();
				}
			});
		})();
	}

	function addTools() {
		tools.addTool(SelectorTool);
		tools.setToolPassive('Selector', { mouseButtonMask: 1 });

		tools.addTool(tools.WwwcTool)
		tools.setToolActive('Wwwc', { mouseButtonMask: 1 });

		tools.addTool(tools.PanTool)
		tools.setToolActive('Pan', { mouseButtonMask: 2 });

		tools.addTool(tools.ZoomTool)
		tools.setToolActive('Zoom', { mouseButtonMask: 4 });

		tools.addTool(tools.ZoomMouseWheelTool);
		tools.setToolActive('ZoomMouseWheel', {});
	}

	function initCornerstone() {
		if (!imageId.endsWith('.jpg') && !imageId.endsWith('.png')) {
			imageId = 'wadouri:' + imageId;
		}

		wado.external.cornerstone = cornerstone;
		web.external.cornerstone = cornerstone;

		tools.init();
		
		cornerstone.enable(element.get(0));

		cornerstone.loadAndCacheImage(imageId).then(function(image) {
			cornerstone.displayImage(element.get(0), image);
			preload();
			self.trigger('display');
		}).catch(function(e) {
			console.log(arguments);
		});
	}

	self.color = function color(color) {
		if (color === undefined) {
			return _SelectorToolConfiguration.color;
		} else {
			_SelectorToolConfiguration.color = color;
		}
	}

	self.label = function label(label) {
		if (label === undefined) {
			return _SelectorToolConfiguration.label;
		} else {
			_SelectorToolConfiguration.label = label;
		}
	}

	function preload() {
		var i;

		preloadIds.forEach(function(id, index) {
			if (imageId.endsWith('.img')) {
				imageId = 'wadouri:' + imageId;
			}

			cornerstone.loadAndCacheImage(id).then(function() {
				console.log('Pre-loading of "' + id + '" successful.');
			});
		})
	}

	self.state = function state(data) {
		var result;

		if (data === undefined) {
			data = tools.getToolState(element.get(0), 'Selector');

			if (!data) {
				return [];
			}

			data = data.data;
			result = [];

			for (let i = 0; i < data.length; i++) {
				result.push({
					color: data[i].color,
					label: data[i].label,
					handles: {
						start: {
							x: data[i].handles.start.x,
							y: data[i].handles.start.y
						},
						end: {
							x: data[i].handles.end.x,
							y: data[i].handles.end.y
						}
					}
				});
			}

			return result;
		} else {
			tools.clearToolState(element.get(0), 'Selector');

			for (let i = 0; i < data.length; i++) {
				tools.addToolState(element.get(0), 'Selector', {
				    "visible": true,
				    "active": false,
				    "color": data[i].color,
				    "label": data[i].label,
				    "handles": {
				      "start": {
				          "x": data[i].handles.start.x,
				          "y": data[i].handles.start.y,
				          "active": false
				      },
				      "end": {
				          "x": data[i].handles.end.x,
				          "y": data[i].handles.end.y,
				          "active": false
				      },
				      "initialRotation": 0
				    },
				    "invalidated": true
				});
			}

			self.updateImage();
		}
	}

	self.removeSelections = function removeSelections(fn) {
		var state    = self.state();
		var newState = [];

		for (let i = 0; i < state.length; i++) {
			if (!fn(state[i])) {
				newState.push(state[i])
			}
		}

		self.state(newState);
		self.updateImage();
	};

	self.startSelection = function startSelection(color, tag) {
		element.css({cursor: 'crosshair'});
		tools.setToolDisabled('Wwwc', { mouseButtonMask: 1 });
		tools.setToolActive('Selector', { mouseButtonMask: 1 });
		state = 'selecting';
	};

	self.stopSelection = function stopSelection() {
		element.css({cursor: 'default'});
		tools.setToolActive('Wwwc', { mouseButtonMask: 1 });
		tools.setToolPassive('Selector', { mouseButtonMask: 1 });
		state = 'not-selecting';
	};

	self.updateImage = function() {
		cornerstone.updateImage(element.get(0));
	}

	__construct();
	return self;
};
