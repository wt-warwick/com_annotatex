$.fn.switchboard = function switchboard() {
	var self = this;

	var currentColor = '#007bff';
	var style;

	function __construct() {
		if (!self.attr('id')) {
			self.attr('id', id());
		}

		addStyleElement();
		addDOMCallbacks();
	}

	function addDOMCallbacks() {
		self.find('button').click(function(event) {
			$(event.target).toggleClass('active');
			self.trigger('change', [$(event.target)]);
		});
	}

	function addStyleElement() {
		if (style) {
			style.remove();
		}

		$('html head').append(style = $(styleHTML()));
	}

	self.activate = function activate(fn) {
		fn = fn || function() { return true; };

		self.find('button').each(function(index, button) {
			if (fn(button)) {
				if (!$(button).hasClass('active')) {
					$(button).addClass('active');
				}
			}
		});
	};

	self.clear = function clear() {
		self.find('button').removeClass('active');
	};

	self.color = function color(c) {
		currentColor = c;
		addStyleElement();
	}

	self.deactivate = function deactivate(fn) {
		fn = fn || function() { return true; };

		self.find('button').each(function (index, button) {
			if (fn(button)) {
				$(button).removeClass('active');
			}
		});
	};

	self.disable = function disable(fn) {
		fn = fn || function() { return true; };

		self.find('button').each(function(index, button) {
			if (fn(button)) {
				$(button).prop('disabled', true);
			}
		});
	};

	self.enable = function enable(fn) {
		fn = fn || function() { return true; };

		self.find('button').each(function(index, button) {
			if (fn(button)) {
				$(button).prop('disabled', false);
			}
		});
	};

	function id() {
		var i;
		var result = 'id-';
		var chars  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

		for (i = 0; i < 24; i++) {
			result += chars.charAt(Math.floor(Math.random() * chars.length));
		}

		return result;
	}

	function lighten(col, amt) {
	    var usePound = false;
	  
	    if (col[0] == "#") {
	        col = col.slice(1);
	        usePound = true;
	    }
	 
	    var num = parseInt(col, 16);
	    var r = (num >> 16) + amt;
	 
	    if       (r > 255) r = 255;
	    else if  (r < 0)   r = 0;
	 
	    var b = ((num >> 8) & 0x00FF) + amt;
	 
	    if       (b > 255) b = 255;
	    else if  (b < 0)   b = 0;
	 
	    var g = (num & 0x0000FF) + amt;
	 
	    if      (g > 255) g = 255;
	    else if (g < 0)   g = 0;
	 
	    return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);
	}

	function styleHTML() {
		return '<style>' +
		           '#' + self.attr('id') + ' button.active{background:' + currentColor + '}' +
		           '#' + self.attr('id') + ' button.active:hover{background:' + lighten(currentColor, -20) + '}' +
		       '</style>';
	}

	__construct();
	return this;
}