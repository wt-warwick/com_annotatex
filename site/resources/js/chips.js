$.fn.chips = function chips() {
    var self = this;

    self.chips = function add(labels) {
	var chip;
	var i;

	if (!Array.isArray(labels)) {
	    labels = [labels];
	}

	for (i = 0; i < labels.length; i++) {
	    chips_(labels[i]);
	}
    };

    function chips_(label) {
	var id = nextId++;

	chip = $(chipHTML(label))
	chip.data('id', id);
	chip.find('.x').click(function() {
	    self.remove(id);
	});
	self.append(chip);
    }

    function chipHTML(label) {
	return '<span class="badge" style="background:' + currentColor + '">' + label + '<i class="fa fa-close x"></i></span>';
    }

    self.getElement = function getElement(id) {
	return self.getElements().filter(function(index, element) {
	    return $(element).data('id') === id;
	});
    }

    self.getElements = function getElements() {
	return self.find('.chip');
    }

    return this;
}
