$(function() {
	const PAGE_LENGTH = 10;

	var page;
	var search;
    var table;
    var urlParams;

    urlParams = new URLSearchParams(window.location.search);
    page = urlParams.get('page') || 1;
    page = Number.isInteger(Number(page)) && page > 0 ? page : 1;
    search = urlParams.get('search') || '';

    table = $('#main-table').DataTable({
        ajax: function(data, callback, settings) {
            $.ajax({
                data: {
                    [$('#token').attr('name')]: 1,
                    contains: data.search.value,
                    format: 'json',
                    size: data.length,
                    start: data.start,
                    task: 'reports.list'
                },
                success: function(response) {
                    callback({
                        draw: data.draw,
                        data: $.map(response.data.reports, function(report, i) {
                            return [['<button class="btn btn-primary btn-sm fa fa-pencil"></button>',
                                     report.id,
                                     (function() {
                                        var i;
                                        var text = report.text + '<br />';

                                        for (i = 0; i < report.labels.length; i++) {
                                            text += '<span class="badge badge-secondary">' + report.labels[i] + '</span> ';
                                        }

                                        return text;
                                     })(),
                                     (function() {
                                        switch (parseInt(report.n_submissions || 0)) {
                                            case 0:  return '';
                                            case 1:  return '<i class="fa fa-check" style="color:#007bff"></i>';
                                            default: return '<span class="fa-stack" style="color:#007bff">' +
                                                                '<i class="fa fa-check fa-stack-1x" style="margin-left:4px"></i>' +
                                                                '<i class="fa fa-check fa-inverse fa-stack-1x" style="margin-left:-3px;"></i>' +
                                                                '<i class="fa fa-check  fa-stack-1x" style="margin-left:-4px"></i>' +
                                                            '</span>';
                                        }
                                     })(),
                                     report.last_submission_date]];
                        }),
                        recordsTotal: response.data.total,
                        recordsFiltered: response.data.filtered
                    });

                    $('#main-table').find('button').click(function() {
                    	var reportId = $(this).parents('tr').children().eq(1).text();
                    	var returnUrl = window.location.protocol + '//' + window.location.host + window.location.pathname + '?page=' + page + '&search=' + search;

                        window.location = $('#base-link').val() + '&report_id=' + reportId + "&return_url=" + encodeURIComponent(returnUrl);

                        return false;
                    });
                },
                error: function(response) {
                    console.log(response);
                }
            });
        },
        columns: [
            {className: 'text-center align-middle'},
            {className: 'text-left align-middle'},
            {className: 'text-left align-middle'},
            {className: 'text-center align-middle'},
            {className: 'text-center align-middle'}
        ],
        displayStart: (page - 1) * PAGE_LENGTH,
        ordering: false,
        pageLength: PAGE_LENGTH,
        sDom: '<"top"f>rt<"bottom"p>',
        search: { search: search },
        serverSide: true
    });

    $('#main-table').on('page.dt search.dt', function(event) {
	    page = table.page() + 1;
        search = table.search();
	});

    $('#collection').change(function(event) {
        $.ajax({
            data: {
                [$('#token').attr('name')]: 1,
                collection: $('#collection').val(),
                format: 'json',
                task: 'reports.setcollection'
            },
            success: function(response) {
                window.location.reload();
            },
            error: function(response) {
                console.log(response);
            }
        });
    });
});