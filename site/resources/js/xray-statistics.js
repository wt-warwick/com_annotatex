function SubmissionsPerDayChart(canvas, token, userId) {
	var self = this;

	var chart;
	var color = '#007bff';
	var data;

	function construct() {
		data = {
			labels: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
		};
		chart = new Chart(canvas.get(0).getContext('2d'), {
			type: 'bar',
			data: data
		});

		self.refresh(token, userId);
	}

	self.refresh = function refresh(token, userId) {
		$.ajax({
            data: {
                [token]: 1,
                format: 'json',
                task: 'xraystatistics.submissionsperdayoftheweek',
                userId: userId
            },
            success: function(response) {
            	response = $.extend({sunday: 0, monday: 0, tuesday: 0, wednesday: 0, thursday: 0, friday: 0, saturday: 0}, response.data);;

            	data.datasets = [{
					label: 'Submissions',
					backgroundColor: color,
					borderColor: color,
					borderWidth: 1,
					data: [	response.sunday, response.monday, response.tuesday, response.wednesday, response.thursday, response.friday, response.saturday]
				}];
				chart.update();
            },
            error: function(response) {
            	console.log(response);
            }
        })
	}

	construct();
}

function SubmissionsPerSessionTable(element, token_, userId_) {
	var self = this;

	var table;
	var token = token_;
	var userId = userId_;

	function construct() {
		table = element.DataTable({
		    ajax: function(data, callback, settings) {
		        $.ajax({
		            data: {
		                [token]: 1,
		                format: 'json',
		                size: data.length,
		                start: data.start,
		                task: 'xraystatistics.submissionspersession',
		                userId: userId
		            },
		            success: function(response) {
		                callback({
		                    draw: data.draw,
		                    data: $.map(response.data.sessions, function(session, i) {
		                        return [[session.date,
		                                 session.login_time,
		                                 session.last_action_time,
		                                 session.duration,
		                                 session.submissions]];
		                    }),
		                    recordsTotal: response.data.session_count,
		                    recordsFiltered: response.data.session_count
		                });
		            },
		            error: function(response) {
		            	console.log(response);
		            }
		        });
		    },
		    columns: [
		    	{className: 'text-center'},
		    	{className: 'text-center'},
		    	{className: 'text-center'},
		    	{className: 'text-right'},
		    	{className: 'text-right'},
		    ],
		    ordering: false,
		    pageLength: 5,
		    sDom: 't<"bottom"p>',
		    serverSide: true
		});
	}

	self.refresh = function refresh(token_, userId_) {
		token  = token_;
		userId = userId_;

		table.ajax.reload();
	}

	construct();
}

$(function () {
	var token        = $('#token').attr('name');
    var userId       = $('#user-combo').val();

	var dayChart      = new SubmissionsPerDayChart($('#submissions-per-day-of-week'), token, userId);
	var table         = new SubmissionsPerSessionTable($('#submissions-per-session'), token, userId);

    $('#user-combo').change(function() {
    	var userId = $('#user-combo').val();

    	table.refresh(token, userId);
    	dayChart.refresh(token, userId);
    });

    $('[data-toggle="popover"]').popover();
});
