<?php
/**
 * @package	 uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license	 Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * AnnotateX Reports Controller
 *
 * @since  0.0.1
 */
class AnnotateXControllerXRays extends JControllerLegacy
{
	/**
	 * Handle for the 'xrays.list' task.
	 */
	public function list($key = null, $urlVar = null)
	{
		if (!JSession::checkToken('get')) {
			echo new JResponseJson(null, JText::_('JINVALID_TOKEN'), true);
			return;
		}

		$user  = JFactory::getUser();

		$input  = JFactory::getApplication()->input;
		$search = $input->get('contains', NULL, 'STRING');
		$size   = $input->get('size', 10, 'INT');
		$start  = $input->get('start', 0, 'INT');

		if ($size > 200) {
			echo new JResponseJson(null, JText::_('\'size\' parameter is too big.'), true);
			return;
		}

		$model = $this->getModel('XRayList');

		$collection = $model->getCurrentCollection($user);

		$response        = new stdClass();
		$response->xrays = $model->getLabelledXRays($user, $collection, $search, $start, $size);
		$response->total = $model->getLabelledXRayCount($user, $collection, '');

		if ($search == NULL || $search == '') {
			$response->filtered = $response->total;
		} else {
			$response->filtered = $model->getLabelledXRayCount($user, $collection, $search);
		}

		echo new JResponseJson($response);
	}

	public function setcollection($key = null, $urlVar = null)
	{
		if (!JSession::checkToken('get')) {
			echo new JResponseJson(null, JText::_('JINVALID_TOKEN'), true);
			return;
		}

		$user  = JFactory::getUser();

		$input         = JFactory::getApplication()->input;
		$collection_id = $input->get('collection', NULL, 'INTEGER');

		$model = $this->getModel('XRayList');

		if ($model->validateCurrentCollection($user, $collection_id)) {
			$model->setCurrentCollection($user, $collection_id);
			echo new JResponseJson(TRUE);
			return;
		}

		echo new JResponseJson(FALSE);
	}
}