<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * AnnotateX Submissions Controller
 *
 * @since  0.0.1
 */
class AnnotateXControllerSubmissions extends JControllerLegacy
{
	/**
	 * Handle for 'submissions.post' task.
	 */
	public function post($key = null, $urlVar = null)
	{
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$user  = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$model = $this->getModel('AnnotateReports');

		$submission            = new stdClass();
		$submission->user      = $user;
		$submission->report    = $model->getReport($input->get('report_id', NULL, 'STRING'), $user);
		$submission->tags      = json_decode($input->get('tags', NULL, 'RAW'));
		$submission->load_time = $input->get('load_time', NULL, 'INT');

		$return_url            = $input->get('return_url', JUri::getInstance(), 'STRING');

		if (!$model->validateSubmission($submission, $user)) {
			$this->setRedirect(JUri::getInstance(), JText::_('An error occurred, please contact the administrator.'), JText::_('Error'));
			return true;
		}

		$model->saveSubmission($submission);
		$this->setRedirect($return_url, JText::_('Report has been successfully saved'), JText::_('Success'));

		return true;
	}
}
