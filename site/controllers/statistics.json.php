<?php
/**
 * @package	 uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license	 Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * AnnotateX Statistics Controller
 *
 * @since  0.0.1
 */
class AnnotateXControllerStatistics extends JControllerLegacy
{
	/**
	 * Handle for the 'statistics.submissionaveragetimeperreportlength' task.
	 */
	public function averageTimePerReportLength($key = null, $urlVar = null)
	{
		if (!JSession::checkToken('get')) {
			echo new JResponseJson(null, JText::_('JINVALID_TOKEN'), true);
			return;
		}

		$input   = JFactory::getApplication()->input;
		$user_id = $input->get('userId', NULL, 'INT');

		$model   = $this->getModel('Statistics');

		echo new JResponseJson($model->getAverageTimePerReportLength($user_id));
	}

	public function agreementByLabel($key = null, $urlVar = null)
	{
		if (!JSession::checkToken('get')) {
			echo new JResponseJson(null, JText::_('JINVALID_TOKEN'), true);
			return;
		}

		$model    = $this->getModel('Statistics');
		$input    = JFactory::getApplication()->input;
		$user_ids = $input->getVar('userIds', array());

		echo new JResponseJson($model->getAgreementByLabel($user_ids));
	}

	public function reportsInDisagreement($key = null, $urlVar = null)
	{
		if (!JSession::checkToken('get')) {
			echo new JResponseJson(null, JText::_('JINVALID_TOKEN'), true);
			return;
		}

		$input   = JFactory::getApplication()->input;
		$size    = $input->get('size', 10, 'INT');
		$offset  = $input->get('start', 0, 'INT');

		if ($size > 200) {
			echo new JResponseJson(null, JText::_('\'size\' parameter is too big.'), true);
			return;
		}

		$model                  = $this->getModel('Statistics');

		$response               = new stdClass();
		$response->reports      = $model->getReportsInDisagreement($offset, $size);
		$response->report_count = $model->getReportsInDisagreementCount();

		echo new JResponseJson($response);
	}

	/**
	 * Handle for the 'statistics.submissionspersession' task.
	 */
	public function submissionsPerSession($key = null, $urlVar = null)
	{
		if (!JSession::checkToken('get')) {
			echo new JResponseJson(null, JText::_('JINVALID_TOKEN'), true);
			return;
		}

		$input   = JFactory::getApplication()->input;
		$user_id = $input->get('userId', NULL, 'INT');
		$size    = $input->get('size', 10, 'INT');
		$offset  = $input->get('start', 0, 'INT');

		if ($size > 200) {
			echo new JResponseJson(null, JText::_('\'size\' parameter is too big.'), true);
			return;
		}

		$model = $this->getModel('Statistics');

		$response                = new stdClass();
		$response->sessions      = $model->getSubmissionsPerSession($user_id, $size, $offset);
		$response->session_count = $model->getSessionCount($user_id);

		echo new JResponseJson($response);
	}

	/**
	 * Handle for the 'statistics.submissionsperdayoftheweek' task.
	 */
	public function submissionsPerDayOfTheWeek($key = null, $urlVar = null)
	{
		if (!JSession::checkToken('get')) {
			echo new JResponseJson(null, JText::_('JINVALID_TOKEN'), true);
			return;
		}

		$input   = JFactory::getApplication()->input;
		$user_id = $input->get('userId', NULL, 'INT');

		$model   = $this->getModel('Statistics');

		echo new JResponseJson($model->getSubmissionsPerDayOfTheWeek($user_id));
	}

	/**
	 * Handle for the 'statistics.labeldistribution' task.
	 */
	public function labelDistribution($key = null, $urlVar = null)
	{
		if (!JSession::checkToken('get')) {
			echo new JResponseJson(null, JText::_('JINVALID_TOKEN'), true);
			return;
		}

		$input         = JFactory::getApplication()->input;
		$collection_id = $input->get('collectionId', NULL, 'INT');
		$model         = $this->getModel('Statistics');

		echo new JResponseJson($model->getLabelDistribution($collection_id));
	}
}
