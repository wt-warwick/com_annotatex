<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * AnnotateX Submissions Controller
 *
 * @since  0.0.1
 */
class AnnotateXControllerXRaySubmissions extends JControllerLegacy
{
	/**
	 * Handle for 'xraysubmissions.post' task.
	 */
	public function post($key = null, $urlVar = null)
	{
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$user  = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$model = $this->getModel('AnnotateXRays');

        $submission             = new stdClass();
        $submission->user       = $user;
        $submission->collection = $model->getCollection($user, $input->get('collection_id', NULL, 'INTEGER'));
        $submission->xray       = $model->getLabelledXRay($user, $input->get('xray_id', NULL, 'STRING'), $submission->collection);
        $submission->tags       = json_decode($input->get('tags', NULL, 'STRING'));
        $submission->load_time  = $input->get('load_time', NULL, 'INT');
        $return_url             = $input->get('return_url', JUri::getInstance(), 'STRING');

        if (!$model->validateSubmission($submission, $user)) {
            $this->setRedirect(JUri::getInstance(), JText::_('An error occurred, please contact the administrator.'), JText::_('Error'));
            return true;
        }

        $model->saveSubmission($submission);

        $this->setRedirect($return_url, JText::_('Report has been successfully saved'), JText::_('Success'));

		return true;
	}

    public function postpone($key = null, $urlVar = null)
    {
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$user  = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$model = $this->getModel('AnnotateXRays');
        $return_url             = $input->get('return_url', JUri::getInstance(), 'STRING');

        $action = new stdClass();
        $action->user       = $user;
        $action->collection = $model->getCollection($user, $input->get('collection_id', NULL, 'INTEGER'));
        $action->xray       = $model->getLabelledXRay($user, $input->get('xray_id', NULL, 'STRING'), $action->collection);

        if (!$model->validatePostponeAction($action, $user)) {
            $this->setRedirect(JUri::getInstance(), JText::_('An error occurred, please contact the administrator.'), JText::_('Error'));
            return true;
        }

        $model->savePostponeAction($action);
        $this->setRedirect($return_url, JText::_('Report has been succesfully postoned'), JText::_('Success'));
    }
}
