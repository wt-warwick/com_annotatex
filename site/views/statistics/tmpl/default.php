<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');?>

<div class="container">
	<div class="row">
		<div class="col-6">
			<h4>Report Statistics</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<h6>User Working Time</h6>
		</div>
	</div><?php

	if (!$this->users or !count($this->users)) {?>
		<p>No user found.</p><?php
	} else {?>
		<div class="row mb-3">
			<div class="col-12">
				<div class="table-responsive">
					<table class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th class="text-center align-middle" rowspan="2" style="min-width:240px">User</th>
								<th class="text-center" colspan="4">Reports</th>
								<th class="text-center" colspan="2">Annotations</th>
								<th class="text-center" colspan="4">Time per Report</th>
								<th class="text-center" colspan="3">Working Time</th>
							</tr>
							<tr>
								<th class="text-center">#&nbsp;Assigned</th>
								<th class="text-center">#&nbsp;Submitted</th>
								<th class="text-center">#&nbsp;Resubmitted</th>
								<th class="text-center">Completed&nbsp;(%)</th>
								<th class="text-center">#&nbsp;Sentences</th>
								<th class="text-center">#&nbsp;Labels</th>
								<th class="text-center">Min.</th>
								<th class="text-center">Max</th>
								<th class="text-center">Avg.</th>
								<th class="text-center">Dev.</th>
								<th class="text-center" style="min-width:90px">
									<span data-container="body"
									      data-content="Sum of the differences between report load time and submit time."
									      data-toggle="popover"
									      data-trigger="hover"
									      style="cursor:pointer;"
									      title="Total Net Time Spent">
									      <i class="fa fa-question-circle"></i>&nbsp;Net
									</span>
								</th>
								<th class="text-center" style="min-width:90px">
									<span data-container="body"
				                          data-content="Sum of the differences between login time and the time of last submission for each user session."
				                          data-toggle="popover"
				                          data-trigger="hover"
				                          style="cursor:pointer;"
				                          title="Total Working Time Spent">
				                        <i class="fa fa-question-circle"></i>&nbsp;Total
				                    </span>
								</th>
								<th class="text-center" style="min-width:90px">
									<span data-container="body"
				                          data-content="Estimate of the working hours the user still has to do to submit all reports."
				                          data-toggle="popover"
				                          data-trigger="hover"
				                          style="cursor:pointer;"
				                          title="Estimated Remaining Working Time">
				                        <i class="fa fa-question-circle"></i>&nbsp;Left
				                    </span>
								</th>
							</tr>
						</thead>
						<tbody><?php

							foreach ($this->users as $user) {
								$perc = $user->report_count ? number_format($user->submission_count * 100 / $user->report_count, 2, '.', '') : 0.00;
								$min  = number_format($user->min_time, 2, '.', '').'s';
								$max  = number_format($user->max_time, 2, '.', '').'s';
								$avg  = number_format($user->average_time, 2, '.', '').'s';
								$dev  = number_format($user->standard_deviation, 2, '.', '').'s';
								$net  = number_format((float) ($user->average_time * $user->submission_count / (60 * 60)), 2, '.', '').'h';
								$tot  = number_format((float) $user->estimated_hours / (60 * 60), 2, '.', '').'h';

								if ($user->submission_count) {
									$hold = (float) $user->estimated_hours / (60 * 60);
									$left = number_format(($hold * $user->report_count / $user->submission_count) - $hold, 2, '.', '').'h';
								} else {
									$left = '&infin;';
								}?>

								<tr>
									<td><?php echo $user->name;?></td>
									<td class="text-right"><?php echo $user->report_count;?></td>
									<td class="text-right"><?php echo $user->submission_count;?></td>
									<td class="text-right"><?php echo $user->resubmission_count;?></td>
									<td class="text-right<?php echo $perc == 100 ? ' text-success' : ''?>"><?php echo $perc;?></td>
									<td class="text-right"><?php echo $user->submitted_sentences;?></td>
									<td class="text-right"><?php echo $user->submitted_labels;?></td>
									<td class="text-right"><?php echo $min;?></td>
									<td class="text-right"><?php echo $max;?></td>
									<td class="text-right"><?php echo $avg;?></td>
									<td class="text-right"><?php echo $dev;?></td>
									<td class="text-right"><?php echo $net;?></td>
									<td class="text-right"><?php echo $tot;?></td>
									<td class="text-right"><?php echo $left;?></td>
								</tr><?php
							}?>

						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-6">
				<h4>Labels</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<h6>Label Distribution</h6>
			</div>
		</div>
		<form class="row">
			<div class="col-12">
				<div class="form-group">
					<select class="form-control" id="collection-combo"><?php
						foreach ($this->collections as $collection) {?>
							<option value="<?php echo $collection->id;?>"><?php echo $collection->title;?></option><?php
						}?>
					</select>
					<small id="collections-help" class="form-text text-muted">Select a collection from the list to get the statistics.</small>
				</div>
			</div>
		</form>
		<div class="row mb-5">
			<div class="col-12">
				<canvas id="label-distribution" style="width:100%;height:640px"></canvas>
			</div>
		</div>

		<div class="row">
			<div class="col-6">
				<h4>Agreement</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-12 mb-2">
				<h6>Agreement by Report</h6>
			</div>
		</div>
		<div class="row mb-5">
			<div class="col-12 mb-3">
				<form>
					<input id="users" type="hidden" value="<?php echo htmlspecialchars(json_encode($this->users));?>"/>

					<select class="custom-select" id="agreement-by-label-combo" style="height:72px" multiple><?php
						foreach ($this->users as $user) {?>
							<option value="<?php echo $user->id;?>"><?php echo $user->name;?></option><?php
						}?>
					</select>
					<small id="collections-help" class="form-text text-muted">Select at least two users from the list above holding the CTRL key.</small>
				</form>
			</div>
			<div class="col-12">
				<div style="max-height:400px;overflow-y:scroll;">
					<canvas id="agreement-by-label" style="width:100%;height:2560px"></canvas>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<h6>Reports in Disagreement</h6>
			</div>
		</div>
		<div class="row mb-5">
			<div class="col-12">
				<table class="table table-bordered table-hover table-striped" id="reports-in-disagreement">
					<thead>
						<tr>
							<th class="text-center">ID</th>
							<th class="text-center">Content</th>
							<th class="text-center">Disagreement</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<h4>Per User Statistics</h4>
			</div>
		</div>
		<form class="row">
			<div class="col-12">
				<div class="form-group">
					<select class="form-control" id="user-combo"><?php
						foreach ($this->users as $user) {?>
							<option value="<?php echo $user->id;?>"><?php echo $user->name;?></option><?php
						}?>
					</select>
					<small id="radiologists-help" class="form-text text-muted">Select a user from the list to get the statistics.</small>
				</div>
			</div>

			<input id="token" type="hidden" name="<?php echo JSession::getFormToken();?>" value="1"/>
		</form>

		<div class="row mb-3">
			<div class="col-md-6">
				<h6>Submissions per Day of the Week</h6>
				<div style="width:100%;">
					<canvas id="submissions-per-day-of-week"></canvas>
				</div>
			</div>
			<div class="col-md-6">
				<h6>Average Time per Report Length</h6>
				<div style="width:100%;">
					<canvas id="average-time-per-report-length"></canvas>
				</div>
			</div>
		</div>

		<div class="row mb-5">
			<div class="col-12">
				<h6>Submissions per Session</h6>
			</div>
			<div class="col-12">
				<table class="table table-bordered table-hover table-striped" id="submissions-per-session">
					<thead>
						<tr>
							<th class="text-center">Date</th>
							<th class="text-center">Session Start</th>
							<th class="text-center">Last Submission</th>
							<th class="text-center">Duration (s)</th>
							<th class="text-center"># Submissions</th>
						</tr>
					</thead>
				</table>
			</div>
		</div><?php
	}?>
</div>
