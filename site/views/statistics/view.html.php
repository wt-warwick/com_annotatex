<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for the AnnotateX Component
 *
 * @since  0.0.1
 */
class AnnotateXViewStatistics extends JViewLegacy
{
	/**
	 * Display the FullList view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		$this->initDocument();

		$model = $this->getModel();

		$user_ids            = JAccess::getUsersByGroup(10);
		$report_counts       = $model->getReportCount($user_ids);
		$submission_counts   = $model->getSubmittedReportCount($user_ids);
		$resubmission_counts = $model->getResubmittedReportCount($user_ids);
		$submitted_sentences = $model->getSubmittedSentenceCount($user_ids);
		$submitted_labels    = $model->getSubmittedLabelCount($user_ids);
		$average_times       = $model->getSubmissionTimeStatistics($user_ids);
		$estimated_hours     = $model->getEstimatedHours($user_ids);

		$this->collections = $model->getCollections();
		$this->users       = array();

		foreach ($user_ids as $user_id) {
			$user                      = JFactory::getUser($user_id);

			if ($user->block) {
				continue;
			}

			$user->report_count        = isset($report_counts[$user_id]) ? $report_counts[$user_id]['count'] : 0;
			$user->submission_count    = isset($submission_counts[$user_id]) ? $submission_counts[$user_id]['count'] : 0;
			$user->resubmission_count  = isset($resubmission_counts[$user_id]) ? $resubmission_counts[$user_id]['count'] : 0;
			$user->submitted_sentences = isset($submitted_sentences[$user_id]) ? $submitted_sentences[$user_id]['count'] : 0;
			$user->submitted_labels    = isset($submitted_labels[$user_id]) ? $submitted_labels[$user_id]['count'] : 0;
			$user->min_time            = isset($average_times[$user_id]) ? $average_times[$user_id]['min'] : 0;
			$user->max_time            = isset($average_times[$user_id]) ? $average_times[$user_id]['max'] : 0;
			$user->average_time        = isset($average_times[$user_id]) ? $average_times[$user_id]['average'] : 0;
			$user->standard_deviation  = isset($average_times[$user_id]) ? $average_times[$user_id]['dev'] : 0;
			$user->estimated_hours     = isset($estimated_hours[$user_id]) ? $estimated_hours[$user_id]['seconds'] : 0;

			array_push($this->users, $user);
		}

		parent::display();
	}

	private function initDocument() {
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('User Statistics'));
		$document->addStyleSheet('https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css');
		$document->addStyleSheet('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css');

		$document->addScript('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js');
		$document->addScript('https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js');
		$document->addScript('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js');

		$document->addScript(JUri::root().'components/com_annotatex/resources/js/statistics.js');
	}
}
