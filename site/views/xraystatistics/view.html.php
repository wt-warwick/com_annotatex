<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for the AnnotateX Component
 *
 * @since  0.0.1
 */
class AnnotateXViewXRayStatistics extends JViewLegacy
{
	function display($tpl = null)
	{
		$this->initDocument();

		$model = $this->getModel();

		$user_ids            = JAccess::getUsersByGroup(10);
		$xray_counts         = $model->getXRayCount($user_ids);
		$submission_counts   = $model->getSubmittedXRayCount($user_ids);
		$resubmission_counts = $model->getResubmittedXRayCount($user_ids);
		$submitted_shapes    = $model->getSubmittedShapeCount($user_ids);
		$average_times       = $model->getSubmissionTimeStatistics($user_ids);
		$estimated_hours     = $model->getEstimatedHours($user_ids);

		$this->collections = $model->getCollections();
		$this->users       = array();

		foreach ($user_ids as $user_id) {
			$user                     = JFactory::getUser($user_id);

   			if ($user->block) {
				continue;
			}

			$user->xray_count         = isset($xray_counts[$user_id]) ? $xray_counts[$user_id]['count'] : 0;
			$user->submission_count   = isset($submission_counts[$user_id]) ? $submission_counts[$user_id]['count'] : 0;
			$user->resubmission_count = isset($resubmission_counts[$user_id]) ? $resubmission_counts[$user_id]['count'] : 0;
			$user->submitted_shapes   = isset($submitted_shapes[$user_id]) ? $submitted_shapes[$user_id]['count'] : 0;
			$user->min_time           = isset($average_times[$user_id]) ? $average_times[$user_id]['min'] : 0;
			$user->max_time           = isset($average_times[$user_id]) ? $average_times[$user_id]['max'] : 0;
			$user->average_time       = isset($average_times[$user_id]) ? $average_times[$user_id]['average'] : 0;
			$user->standard_deviation = isset($average_times[$user_id]) ? $average_times[$user_id]['dev'] : 0;
			$user->estimated_hours    = isset($estimated_hours[$user_id]) ? $estimated_hours[$user_id]['seconds'] : 0;

			array_push($this->users, $user);
		}

		parent::display();
	}

	private function initDocument() {
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('User Statistics'));
		$document->addStyleSheet('https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css');
		$document->addStyleSheet('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css');

		$document->addScript('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js');
		$document->addScript('https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js');
		$document->addScript('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js');

		$document->addScript(JUri::root().'components/com_annotatex/resources/js/xray-statistics.js');
	}
}
