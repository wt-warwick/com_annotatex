<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');?>

<div class="container">
	<div class="row">
		<div class="col-6">
			<h4>X-Ray Statistics</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<h6>User Working Time</h6>
		</div>
	</div><?php

	if (!$this->users or !count($this->users)) {?>
		<p>No user found.</p><?php
	} else {?>
		<div class="row mb-3">
			<div class="col-12">
				<div class="table-responsive">
					<table class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th class="text-center align-middle" rowspan="2" style="min-width:240px">User</th>
								<th class="text-center" colspan="4">Reports</th>
								<th class="text-center" colspan="1">Annotations</th>
								<th class="text-center" colspan="4">Time per Report</th>
								<th class="text-center" colspan="3">Working Time</th>
							</tr>
							<tr>
								<th class="text-center">#&nbsp;Assigned</th>
								<th class="text-center">#&nbsp;Submitted</th>
								<th class="text-center">#&nbsp;Resubmitted</th>
								<th class="text-center">Completed&nbsp;(%)</th>
								<th class="text-center">#&nbsp;Shapes</th>
								<th class="text-center">Min.</th>
								<th class="text-center">Max</th>
								<th class="text-center">Avg.</th>
								<th class="text-center">Dev.</th>
								<th class="text-center" style="min-width:90px">
									<span data-container="body"
									      data-content="Sum of the differences between x-ray load time and submit time."
									      data-toggle="popover"
									      data-trigger="hover"
									      style="cursor:pointer;"
									      title="Total Net Time Spent">
									      <i class="fa fa-question-circle"></i>&nbsp;Net
									</span>
								</th>
								<th class="text-center" style="min-width:90px">
									<span data-container="body"
				                          data-content="Sum of the differences between login time and the time of last submission for each user session."
				                          data-toggle="popover"
				                          data-trigger="hover"
				                          style="cursor:pointer;"
				                          title="Total Working Time Spent">
				                        <i class="fa fa-question-circle"></i>&nbsp;Total
				                    </span>
								</th>
								<th class="text-center" style="min-width:90px">
									<span data-container="body"
				                          data-content="Estimate of the working hours the user still has to do to submit all x-rays."
				                          data-toggle="popover"
				                          data-trigger="hover"
				                          style="cursor:pointer;"
				                          title="Estimated Remaining Working Time">
				                        <i class="fa fa-question-circle"></i>&nbsp;Left
				                    </span>
								</th>
							</tr>
						</thead>
						<tbody><?php

							foreach ($this->users as $user) {
								$perc = $user->xray_count ? number_format($user->submission_count * 100 / $user->xray_count, 2, '.', '') : 0.00;
								$min  = number_format($user->min_time, 2, '.', '').'s';
								$max  = number_format($user->max_time, 2, '.', '').'s';
								$avg  = number_format($user->average_time, 2, '.', '').'s';
								$dev  = number_format($user->standard_deviation, 2, '.', '').'s';
								$net  = number_format((float) ($user->average_time * $user->submission_count / (60 * 60)), 2, '.', '').'h';
								$tot  = number_format((float) $user->estimated_hours / (60 * 60), 2, '.', '').'h';

								if ($user->submission_count) {
									$hold = (float) $user->estimated_hours / (60 * 60);
									$left = number_format(($hold * $user->xray_count / $user->submission_count) - $hold, 2, '.', '').'h';
								} else {
									$left = '&infin;';
								}?>

								<tr>
									<td><?php echo $user->name;?></td>
									<td class="text-right"><?php echo $user->xray_count;?></td>
									<td class="text-right"><?php echo $user->submission_count;?></td>
									<td class="text-right"><?php echo $user->resubmission_count;?></td>
									<td class="text-right<?php echo $perc == 100 ? ' text-success' : ''?>"><?php echo $perc;?></td>
									<td class="text-right"><?php echo $user->submitted_shapes;?></td>
									<td class="text-right"><?php echo $min;?></td>
									<td class="text-right"><?php echo $max;?></td>
									<td class="text-right"><?php echo $avg;?></td>
									<td class="text-right"><?php echo $dev;?></td>
									<td class="text-right"><?php echo $net;?></td>
									<td class="text-right"><?php echo $tot;?></td>
									<td class="text-right"><?php echo $left;?></td>
								</tr><?php
							}?>

						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<h4>Per User Statistics</h4>
			</div>
		</div>
		<form class="row">
			<div class="col-12">
				<div class="form-group">
					<select class="form-control" id="user-combo"><?php
						foreach ($this->users as $user) {?>
							<option value="<?php echo $user->id;?>"><?php echo $user->name;?></option><?php
						}?>
					</select>
					<small id="radiologists-help" class="form-text text-muted">Select a user from the list to get the statistics.</small>
				</div>
			</div>

			<input id="token" type="hidden" name="<?php echo JSession::getFormToken();?>" value="1"/>
		</form>

		<div class="row mb-3">
			<div class="col-md-6">
				<h6>Submissions per Day of the Week</h6>
				<div style="width:100%;">
					<canvas id="submissions-per-day-of-week"></canvas>
				</div>
			</div>
		</div>

		<div class="row mb-5">
			<div class="col-12">
				<h6>Submissions per Session</h6>
			</div>
			<div class="col-12">
				<table class="table table-bordered table-hover table-striped" id="submissions-per-session">
					<thead>
						<tr>
							<th class="text-center">Date</th>
							<th class="text-center">Session Start</th>
							<th class="text-center">Last Submission</th>
							<th class="text-center">Duration (s)</th>
							<th class="text-center"># Submissions</th>
						</tr>
					</thead>
				</table>
			</div>
		</div><?php
	}?>
</div>
