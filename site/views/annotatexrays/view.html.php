<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for the AnnotateX Component
 *
 * @since  0.0.1
 */
class AnnotateXViewAnnotateXRays extends JViewLegacy
{
	/**
	 * Display the AnnotateXRays view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		$this->initDocument();

		$user  = JFactory::getUser();
		$model = $this->getModel();

		$input                  = JFactory::getApplication()->input;
		$xray_id                = $input->get('xray_id', NULL, 'STRING');
		$collection_id          = $input->get('collection_id', NULL, 'INTEGER');
        $previous_submission_id = $input->get('previous_submission_id', NULL, 'INTEGER');

		$this->collection          = $collection_id == NULL ? $model->getCurrentCollection($user) : $model->getCollection($user, $collection_id);
		$this->collections         = $model->getCollections($user);
		$this->collections_enabled = empty($xray_id);
		$this->labels              = $model->getLabels();

		if (empty($this->collection)) {
			parent::display('empty');
			return;
		}

		if ($xray_id && $collection_id) {
			$this->labelled_xray         = $model->getLabelledXRay($user, $xray_id, $this->collection);
            $this->labelled_xray->labels = $this->getLabels($model, $this->labelled_xray);
            
			$this->latest_submission = $model->getLatestSubmission($user, $this->labelled_xray);
			$this->preload_xrays     = array();

			if (!$this->labelled_xray) {
				JError::raiseError(404, JText::_("Page Not Found"));
				return;
			}
		} else {
			$this->labelled_xray         = $model->getNextUnsubmittedLabelledXRay($user, $this->collection);
            $this->labelled_xray->labels = $this->getLabels($model, $this->labelled_xray);
			$this->preload_xrays         = $model->getPreloadXRays($user, $this->collection);

			for ($i = 0; $i < count($this->preload_xrays); $i++) {
				$this->preload_xrays[$i] = JUri::root().$this->preload_xrays[$i];
			}

			$this->latest_submission = NULL;
		}

		$this->xray_count             = $model->getLabelledXRayCount($this->collection, NULL);
		$this->submitted_xray_count   = $model->getSubmittedLabelledXRayCount($user, $this->collection);
		$this->report                 = $model->getReport($this->labelled_xray->id, $user);
        $this->previous_submission_id = $model->getPreviousSubmissionId($user, $previous_submission_id);
        $xray_and_collection          = $model->getPreviousSubmittedXRayIdAndCollection($user, $this->previous_submission_id);
        $this->previous_xray_id       = isset($xray_and_collection) ? $xray_and_collection->xray : NULL;
        $this->previous_collection_id = isset($xray_and_collection) ? $xray_and_collection->collection : NULL;
        $this->postpone_enabled       = !isset($this->latest_submission);
        $this->report_annotations     = $model->getAnnotations($this->labelled_xray);
		$this->return_url             = $input->get('return_url', JUri::getInstance(), 'STRING');

		if (count($errors = $this->get('Errors'))) {
			JLog::add(implode('<br/>', $errors), JLog::WARNING, 'jerror');
			return false;
		}

		if (!$this->labelled_xray) {
			parent::display('complete');
		} else {
			parent::display($tpl);
		}
	}

    private function getLabels($model, $labelled_xray) {
        $default_labels = $model->getDefaultLabels();

        for ($i = 0; $i < count($this->labelled_xray->labels); $i++) {
            $found = false;
                
            for ($j = 0; $j < count($default_labels); $j++) {
                if ($this->labelled_xray->labels[$i]->id == $default_labels[$j]->id) {
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                array_push($default_labels, $this->labelled_xray->labels[$i]);
            }
        }

        return $default_labels;
    }

	/**
	 * Set document metadata, stylesheets and scripts.
	 */
	private function initDocument()
	{
		$document = JFactory::getDocument();

		$document->setTitle(JText::_('Annotate X-Ray'));

		$document->addStyleSheet(JUri::root().'components/com_annotatex/resources/css/annotate-x-rays.css');

		$document->addScript('https://unpkg.com/hammerjs@2.0.8/hammer.js');
		$document->addScript('https://unpkg.com/cornerstone-math@0.1.6/dist/cornerstoneMath.min.js');
		$document->addScript('https://unpkg.com/cornerstone-core@2.3.0/dist/cornerstone.min.js');
		$document->addScript('https://unpkg.com/dicom-parser@1.8.3/dist/dicomParser.min.js');
		$document->addScript('https://unpkg.com/cornerstone-wado-image-loader@3.0.0/dist/cornerstoneWADOImageLoader.min.js');
		$document->addScript('https://unpkg.com/cornerstone-web-image-loader@2.1.1/dist/cornerstoneWebImageLoader.min.js');
		$document->addScript('https://unpkg.com/cornerstone-tools@3.18.3/dist/cornerstoneTools.js');
		$document->addScript(JUri::root().'components/com_annotatex/resources/js/selectortool.js');

		$document->addScript(JUri::root().'components/com_annotatex/resources/js/radioboard.js');
		$document->addScript(JUri::root().'components/com_annotatex/resources/js/xrayboard.js');
		$document->addScript(JUri::root().'components/com_annotatex/resources/js/switchboard.js');

		// This script should be included after all the others, so keep it here.
		$document->addScript(JUri::root().'components/com_annotatex/resources/js/annotate-x-rays.js');
	}
}
