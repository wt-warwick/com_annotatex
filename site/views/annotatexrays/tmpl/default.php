<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');?>

<div class="container pb-3">
	<div class="row">
		<div class="col-md-9">
			<h4>X-Ray ID: <?php echo $this->labelled_xray->id; ?></h4>
		</div>
		<div class="col-md-3">
			<form id="collection-form">
				<div class="form-group">
				    <select class="form-control form-control-sm" <?php echo !$this->collections_enabled ? 'disabled="disabled" ' : '';?>id="collection"><?php
				    	for ($i = 0; $i < count($this->collections); $i++) {?>
				    		<option<?php echo $this->collections[$i]->id == $this->collection->id ? ' selected="selected"' : '';?>
				    		        value="<?php echo $this->collections[$i]->id;?>"><?php echo $this->collections[$i]->title;?></option><?php
				    	}?>
				    </select>
				</div>

				<input id="token" type="hidden" name="<?php echo JSession::getFormToken();?>" value="1"/>
			</form>
		</div>
	</div>
</div>

<div class="radioboard labels">
	<div class="container">
		<div class="row">
			<div class="col-12"><?php
				for ($i = 0; $i < count($this->labelled_xray->labels); $i++) {
					$label = $this->labelled_xray->labels[$i];?>

					<button class="btn btn-default label"
							data-container="body"
		                    data-content="<?php echo htmlspecialchars('<ul><li>'.implode('</li><li>', explode('#', $label->description))).'</li></ul>';?>"
							data-delay="{&quot;show&quot;:1000,&quot;hide&quot;:0}"
							data-html="true"
		                    data-label="<?php echo $label->id;?>"
		                    data-placement="bottom"
		                    data-title="<?php echo $label->title;?>"
		                    data-toggle="popover"
		                    data-trigger="hover"
		                    title="<?php echo $label->title;?>"
		                    type="button"><?php
		                echo $label->title;?>
		                <i class="delete fa fa-close"></i>
		            </button><?php
				}?>

				<button class="btn btn-default more"
				        type="button">
					<i class="fa fa-plus"></i>
				</button>

				<button class="btn btn-default info"
				        type="button">
					<i class="fa fa-info"></i>
				</button><?php

                    if ($this->postpone_enabled) {?>
                        <button class="btn btn-default postpone"
                                onclick="onPostponeButtonClick()"
                       	        type="button">
                            <i class="fa fa-calendar"></i>
                        </button><?php
                    }?>

			</div>
		</div>
	</div>
</div>

<div class="controls">
	<div class="container">
		<div class="row">
			<div class="col-2 offset-10">
                <button type="button" class="btn btn-default btn-block btn-secondary" onclick="onPreviousSubmissionButtonClick()">
                    Previous Submission
                </button>
		        <button type="button" class="btn btn-default btn-block btn-success" onclick="onSubmitButtonClick()">
		            <i class="fa fa-paper-plane" aria-hidden="true"></i> Submit X-Ray
		        </button>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid x-ray-container">
	<div class="row">
		<form action="<?php echo JUri::current();?>" class="col-12" id="main-form" method="POST">
				<div id="enabled-element"></div>

                                                                                                                                       
                <input id="base-link" name="base-link" type="hidden" value="<?php echo JRoute::_('index.php?option=com_annotatex&view=annotatexrays');?>" />
                <input name="collection_id" type="hidden" value="<?php echo $this->labelled_xray->collection->id;?>"/>
				<input id="image-id" type="hidden" value="<?php echo JUri::root().$this->labelled_xray->image_path;?>"/>
				<input id="latest-submission" type="hidden" value="<?php echo htmlspecialchars(json_encode($this->latest_submission));?>"/>
                <input type="hidden" value="<?php echo htmlspecialchars($this->previous_xray_id);?>" id="previous-xray-id"/>
                <input type="hidden" value="<?php echo htmlspecialchars($this->previous_collection_id);?>" id="previous-collection-id"/>
				<input name="load_time" type="hidden" value="<?php echo (new DateTime())->getTimestamp();?>"/>
				<input id="tags" name="tags" type="hidden" value=""/>
				<input name="task" type="hidden" value="xraysubmissions.post" />
				<input name="return_url" type="hidden" value="<?php echo htmlspecialchars($this->return_url);?>"/>
				<input id="token" type="hidden" name="<?php echo JSession::getFormToken();?>" value="1"/>
				<input name="xray_id" type="hidden" value="<?php echo $this->labelled_xray->id;?>"/>
				<input id="preload-ids" type="hidden" value="<?php echo htmlspecialchars(json_encode($this->preload_xrays));?>"/><?php

				$labels = array();

				foreach ($this->labels as $assortment => $data) {
					for ($i = 0; $i < count($data); $i++) {
						$label              = new stdClass();
						$label->title       = $data[$i]->title;
						$label->description = $data[$i]->description;

						$labels[$data[$i]->id] = $label;
					}
				}?>

				<input id="labels" name="labels" type="hidden" value="<?php echo htmlspecialchars(json_encode($labels));?>"/>
                <input id="report-annotations" type="hidden" value="<?php echo htmlspecialchars(json_encode($this->report_annotations));?>"/>
		</form>

		<form action="<?php echo JUri::current();?>" class="col-12" id="postpone-form" method="POST">
				<input name="collection_id" type="hidden" value="<?php echo $this->labelled_xray->collection->id;?>"/>
               	<input type="hidden" value="<?php echo htmlspecialchars($this->previous_submission_id);?>" id="previous-submission-id"/>
				<input name="task" type="hidden" value="xraysubmissions.postpone" />
				<input name="return_url" type="hidden" value="<?php echo htmlspecialchars($this->return_url);?>"/>
				<input id="token" type="hidden" name="<?php echo JSession::getFormToken();?>" value="1"/>
				<input name="xray_id" type="hidden" value="<?php echo $this->labelled_xray->id;?>"/>
		</form>
	</div>
</div>

<div class="modal fade" id="more-modal" tabindex="-1" role="dialog" aria-labelledby="more-modal-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="more-modal-title">Add Labels</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
			</div>

			<div class="modal-body collection-container switchboard"><?php
				const BOOTSTRAP_COLS = 12;

				function echoButtons($labels, $n_columns) {
					foreach ($labels as $i => $label) {?>
						<div class="col-md-<?php echo BOOTSTRAP_COLS / $n_columns;?>">
							<button class="btn btn-block btn-default label"
									data-content="<?php echo htmlspecialchars('<ul><li>'.implode('</li><li>', explode('#', $label->description))).'</li></ul>';?>"
									data-delay="{&quot;show&quot;:0,&quot;hide&quot;:0}"
									data-html="true"
									data-label="<?php echo $label->id;?>"
									data-placement="bottom"
									data-toggle="popover"
									data-trigger="manual"
									title="<?php echo $label->title;?>"
									type="button"><?php
								echo $label->title;?>
							</button>
						</div><?php
					}
				}?>

				<div class="row">
					<div class="col-md-6 collection-container">
						<div class="row collection"><?php
							echoButtons($this->labels['C'], 2);?>
						</div>
					</div>
					
					<div class="col-md-3 collection-container">
						<div class="row collection"><?php
							echoButtons($this->labels['V'], 1);?>
						</div>
						<div style="margin-top: 28px" class="row collection"><?php
							echoButtons($this->labels['M'], 1);?>
						</div>
					</div>

					<div class="col-md-3 collection-container">
						<div class="row collection"><?php
							echoButtons($this->labels['B'], 1);?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3 collection-container">
						<div class="row collection"><?php
							echoButtons($this->labels['D'], 1);?>
						</div>
					</div>

					<div class="col-md-3 collection-container">
						<div class="row collection"><?php
							echoButtons($this->labels['H'], 1);?>
						</div>
					</div>

					<div class="col-md-3 collection-container">
						<div class="row collection"><?php
							echoButtons($this->labels['F'], 1);?>
						</div>
					</div>

					<div class="col-md-3 collection-container">
						<div class="row collection"><?php
							echoButtons($this->labels['J'], 1);?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6 collection-container">
						<div class="row collection"><?php
							echoButtons($this->labels['I'], 2);?>
						</div>
					</div>

					<div class="col-md-3 collection-container">
						<div class="row collection"><?php
							echoButtons($this->labels['E'], 1);?>
						</div>
					</div>

					<div class="col-md-3 collection-container">
						<div class="row collection"><?php
							echoButtons($this->labels['K'], 1);?>
						</div>
					</div>

					<div class="col-12 collection-container">
						<div class="row collection"><?php
							echoButtons(array_filter($this->labels['A'], function($label) {
								return $label->id == 'object';
							}), 2);
							echoButtons($this->labels['Z'], 2);?>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-primary ok-button">OK</button>
			</div>
        </div>
    </div>
</div>

<div class="modal fade" id="info-modal" tabindex="-1" role="dialog" aria-labelledby="info-modal-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="info-modal-title">Report</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-12">
						<p><?php echo $this->report->text;?></p>
					</div>
				</div>
                <div class="border-top my-3"></div>
                <div class="row">
                    <div class="col-12"><?php
                        foreach ($this->report_annotations as $label => $sentences) {?>
                            <div class="badge badge-primary" data-sentences="<?php echo htmlspecialchars(json_encode($sentences));?>"><?php echo $label;?></div><?php
                        }?>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>
