<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');?>

<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="jumbotron">
				<h1 class="display-4">:(</h1>
				<p class="lead">No report has been assigned to you.</p>
				
				<hr class="my-4">
				<p>If you'd like to contribute, contact us.</p>

				<p class="lead">
					<a class="btn btn-primary btn-lg" href="mailto:giovanni.montana@warwick.ac.uk" role="button">Drop an Email</a>
				</p>
			</div>
		</div>
	</div>
</div>