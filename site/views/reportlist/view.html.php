<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for the AnnotateX Component
 *
 * @since  0.0.1
 */
class AnnotateXViewReportList extends JViewLegacy
{
	/**
	 * Display the FullList view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		$this->initDocument();

		$user  = JFactory::getUser();

		$model = $this->getModel();

		$this->collection  = $model->getCurrentCollection($user);
		$this->collections = $model->getCollections($user);

		if (empty($this->collection)) {
			parent::display('empty');
		} else {
			parent::display();
		}
	}

	private function initDocument() {
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('Report List'));

		$document->addStyleSheet('https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css');

		$document->addScript('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js');
		$document->addScript('https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js');
		$document->addScript(JUri::root().'components/com_annotatex/resources/js/report-list.js');
	}
}