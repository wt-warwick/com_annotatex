<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');?>

<div class="container">
	<div class="row">
		<div class="col-md-9">
			<h4>Report Full List</h4>
		</div>
		<div class="col-md-3">
			<form id="collection-form">
				<div class="form-group">
				    <select class="form-control form-control-sm" id="collection" name="collection"><?php
				    	for ($i = 0; $i < count($this->collections); $i++) {?>
				    		<option <?php echo $this->collections[$i]->id == $this->collection->id ? 'selected="selected"' : '';?>
				    		        value="<?php echo $this->collections[$i]->id;?>"><?php echo $this->collections[$i]->title;?></option><?php
				    	}?>
				    </select>
				</div>
				
				<input id="token" type="hidden" name="<?php echo JSession::getFormToken();?>" value="1"/>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<form id="main-form">
				<table class="table table-bordered table-striped" id="main-table" style="width:100%">
					<thead>
						<tr>
							<th></th>
							<th class="text-center">Report ID</th>
							<th class="text-center">Contents</th>
							<th class="text-center">State</th>
							<th class="text-center">Submitted</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>

				<input id="base-link" name="base-link" type="hidden" value="<?php echo JRoute::_('index.php?option=com_annotatex&view=annotatereports');?>" />
			</form>
		</div>
	</div>
</div>