<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');?>

<div class="container">
	<div class="row">
		<div class="col-md-6">
			<h4>Report ID: <?php echo $this->report->id;?> (<?php echo $this->submitted_report_count;?>/<?php echo $this->report_count;?>)</h4>
		</div>
		<div class="offset-md-3 col-md-3">
			<form>
				<div class="form-group">
				    <select class="form-control form-control-sm" id="collection" name="collection" <?php echo $this->collections_enabled ? '' : 'disabled="disabled"'?>><?php
				    	for ($i = 0; $i < count($this->collections); $i++) {?>
				    		<option <?php echo $this->collections[$i]->id == $this->collection->id ? 'selected="selected"' : '';?>
				    		        value="<?php echo $this->collections[$i]->id;?>"><?php echo $this->collections[$i]->title;?></option><?php
				    	}?>
				    </select>
				</div>
			</form>
		</div>
	</div>
</div>

<form action="<?php echo JUri::current();?>" id="main-form" method="POST">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p class="report"><?php echo $this->report->text;?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p class="instructions"></p>
			</div>
		</div>
	</div>

	<div class="container switchboard"><?php
	    const BOOTSTRAP_COLS = 12;

	    function echoButtons($labels, $n_columns) {
			for ($i = 0; $i < count($labels); $i++) {
	        	$label = $labels[$i];?>

	            <div class="col-md-<?php echo BOOTSTRAP_COLS / $n_columns;?>">
	                <button class="btn btn-block btn-default label"
	                        data-content="<?php echo htmlspecialchars('<ul><li>'.implode('</li><li>', explode('#', $label->description))).'</li></ul>';?>"
	                        data-delay="{&quot;show&quot;:0,&quot;hide&quot;:0}"
	                        data-html="true"
	                        data-label="<?php echo $label->id;?>"
	                        data-placement="top"
	                        data-toggle="popover"
	                        data-trigger="manual"
	                        title="<?php echo $label->title;?>"
	                        type="button"><?php
	                    echo $label->title;?>
	                </button>
	            </div><?php
	        }
	    }?>

	    <div class="row">
	    	<div class="col-md-6 collection-container">
	    		<div class="row collection"><?php
			    	echoButtons($this->labels['C'], 2);?>
		    	</div>
		    </div>
			
			<div class="col-md-3 collection-container">
	    		<div class="row collection"><?php
			    	echoButtons($this->labels['V'], 1);?>
		    	</div>
				<div style="margin-top: 31px;" class="row collection"><?php
					echoButtons($this->labels['M'], 1);?>
				</div>
		    </div>

		    <div class="col-md-3 collection-container">
		    	<div class="row collection"><?php
		    		echoButtons($this->labels['B'], 1);?>
		    	</div>
		    </div>
	    </div>

	    <div class="row">
	    	<div class="col-md-3 collection-container">
	    		<div class="row collection"><?php
	    			echoButtons($this->labels['D'], 1);?>
	    		</div>
	    	</div>

	    	<div class="col-md-3 collection-container">
	    		<div class="row collection"><?php
	    			echoButtons($this->labels['H'], 1);?>
	    		</div>
	    	</div>

	    	<div class="col-md-3 collection-container">
	    		<div class="row collection"><?php
	    			echoButtons($this->labels['F'], 1);?>
	    		</div>
	    	</div>

	    	<div class="col-md-3 collection-container">
	    		<div class="row collection"><?php
	    			echoButtons($this->labels['J'], 1);?>
	    		</div>
	    	</div>
	    </div>

	    <div class="row">
	    	<div class="col-md-6 collection-container">
	    		<div class="row collection"><?php
	    			echoButtons($this->labels['I'], 2);?>
	    		</div>
	    	</div>

	    	<div class="col-md-3 collection-container">
	    		<div class="row collection"><?php
	    			echoButtons($this->labels['E'], 1);?>
	    		</div>
	    	</div>

	    	<div class="col-md-3 collection-container">
	    		<div class="row collection"><?php
	    			echoButtons($this->labels['K'], 1);?>
	    		</div>
	    	</div>
	    </div>

	    <div class="row">
	    	<div class="col-md-12 collection-container">
	    		<div class="row collection"><?php
	    			echoButtons(array_merge($this->labels['A'], $this->labels['L']), 4);?>
	    		</div>
	    	</div>
	    </div>
	</div>

	<hr/>

	<div class="container control-buttons">
	    <div class="row">
	        <div class="col-md-3">
	            <button type="button" class="btn btn-default btn-block" style="background:#ef5350" onclick="onRestartButtonClick()">
	                <i class="fa fa-refresh" aria-hidden="true"></i> Start again
	            </button>
	        </div>

	        <div class="col-md-3">
	            <button type="button" class="btn btn-default btn-block" style="background:#ffa726" onclick="onResetLabelsButtonClick()">
	                <i class="fa fa-times-rectangle" aria-hidden="true"></i> Remove Current Selection
	            </button>
	        </div>

	        <div class="col-md-3">
	            <button type="button"
	                    class="btn btn-default btn-block"
	                    style="background:#90a4ae"
	                    onclick="onPreviousButtonClick()"
	                    <?php echo !$this->previous_submission_id ? 'disabled="disabled"' : '';?>>
	                <i class="fa fa-arrow-left" aria-hidden="true"></i> Previous Submission
	            </button>
	        </div>

	        <div class="col-md-3">
	            <button type="button" class="btn btn-default btn-block" style="background:#64dd17" onclick="onSubmitButtonClick()">
	                <i class="fa fa-paper-plane" aria-hidden="true"></i> Submit Report
	            </button>
	        </div>
	    </div>
	</div>

	<hr/>

	<div class="container">
	    <div class="row justify-content-md-center">
	    	<div class="col-md-4 text-center">
	            <span><kbd><kbd>CTRL</kbd>+<kbd>CLK</kbd></kbd> to select the entire sentence</span>
	        </div>
	        <div class="col-md-4 text-center">
	            <span>Press <kbd><kbd>CTRL</kbd>+<kbd>V</kbd></kbd> on a tag to show more info</span>
	        </div>
	        <div class="col-md-4 text-center">
	            <span>Press <kbd><kbd>CTRL</kbd>+<kbd>S</kbd></kbd> to submit the report</span>
	        </div>
	    </div>
	</div><?php

	$submission_string = $this->latest_submission ? htmlspecialchars(json_encode($this->latest_submission->annotation)) : null;?>

	<input id="base-link" name="base-link" type="hidden" value="<?php echo JRoute::_('index.php?option=com_annotatex&view=annotatereports');?>" />
	<input type="hidden" value="<?php echo htmlspecialchars(json_encode($this->expected_labelling));?>" id="expected-labels"/>
	<input name="latest_submission" type="hidden" value="<?php echo $submission_string;?>" id="latest-submission" />
	<input name="load_time" type="hidden" value="<?php echo (new DateTime())->getTimestamp();?>" />
	<input type="hidden" value="<?php echo htmlspecialchars($this->previous_submission_id);?>" id="previous-submission-id"/>
	<input type="hidden" value="<?php echo htmlspecialchars($this->previous_report_id);?>" id="previous-report-id"/>
	<input name="report_id" type="hidden" value="<?php echo $this->report->id;?>" />
	<input name="return_url" type="hidden" value="<?php echo htmlspecialchars($this->return_url);?>"/>
	<input name="tags" type="hidden" value="" id="tags" />
	<input name="task" type="hidden" value="submissions.post" />
	<input id="token" type="hidden" name="<?php echo JSession::getFormToken();?>" value="1"/>
</form>
