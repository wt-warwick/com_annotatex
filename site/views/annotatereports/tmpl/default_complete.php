<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');?>

<div class="container">
	<div class="row">
		<div class="offset-md-9 col-md-3">
			<form>
				<div class="form-group">
				    <select class="form-control form-control-sm" id="collection" name="collection" <?php echo $this->collections_enabled ? '' : 'disabled="disabled"'?>><?php
				    	for ($i = 0; $i < count($this->collections); $i++) {?>
				    		<option <?php echo $this->collections[$i]->id == $this->collection->id ? 'selected="selected"' : '';?>
				    		        value="<?php echo $this->collections[$i]->id;?>"><?php echo $this->collections[$i]->title;?></option><?php
				    	}?>
				    </select>
				</div>

				<input id="token" type="hidden" name="<?php echo JSession::getFormToken();?>" value="1"/>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div class="jumbotron">
				<h1 class="display-4">:)</h1>
				<p class="lead">Congratulations!</p>
				
				<hr class="my-4">
				<p>You completed each and every report in the <?php echo $this->collection->title;?> collection.</p>

				<p class="lead">
					<a class="btn btn-primary btn-lg" href="mailto:giovanni.montana@warwick.ac.uk" role="button">Drop an Email</a>
				</p>
			</div>
		</div>
	</div>
</div>