<?php
/**
 * @package     uk.ac.warwick
 * @subpackage  com_annotatex
 *
 * @copyright   Copyright (C) 2019 WMG. All rights reserved.
 * @license     Proprietary License.
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for the AnnotateX Component
 *
 * @since  0.0.1
 */
class AnnotateXViewAnnotateReports extends JViewLegacy
{
	/**
	 * Display the AnnotateNow view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		$this->initDocument();

		$input      = JFactory::getApplication()->input;
		$report_id  = $input->get('report_id', NULL, 'STRING');

		$user  = JFactory::getUser();
		$this->user = $user;

		$model = $this->getModel();

		$this->collection          = $model->getCurrentCollection($user);
		$this->collections         = $model->getCollections($user);
		$this->collections_enabled = empty($report_id);

		if (empty($this->collection)) {
			parent::display('empty');
			return;
		}

		if ($report_id) {
			$this->report = $model->getReport($report_id, $user);

			if (!$this->report) {
				JError::raiseError(404, JText::_("Page Not Found"));
			}

			$this->latest_submission = $model->getLatestSubmission($report_id, $user);
		} else {
			$this->report            = $model->getNextUnsubmittedReport($user, $this->collection);
			$this->latest_submission = NULL;
		}

		$this->expected_labelling = $model->getExpectedSentenceLabels($user, array_map(function ($sentence) { return trim($sentence); }, explode('.', $this->report->text)));

		$previous_submission_id = $input->get('previous_submission_id', NULL, 'INT');

		$this->previous_submission_id = $model->getPreviousSubmissionId($this->user, $previous_submission_id);
		$this->previous_report_id     = $model->getPreviousSubmittedReportId($this->user, $this->previous_submission_id);
		$this->report_count           = $model->getReportCount($this->collection);
		$this->submitted_report_count = $model->getSubmittedReportCount($user, $this->collection);
		$this->labels                 = $model->getLabels();
		$this->return_url             = $input->get('return_url', JUri::getInstance(), 'STRING');

		if (count($errors = $this->get('Errors'))) {
			JLog::add(implode('<br/>', $errors), JLog::WARNING, 'jerror');
			return false;
		}

		if (!$this->report) {
			parent::display('complete');
		} else {
			parent::display($tpl);
		}
	}

	/**
	 * Set document metadata, stylesheets and scripts.
	 */
	private function initDocument()
	{
		$document = JFactory::getDocument();

		$document->setTitle(JText::_('Annotate Reports'));

		$document->addStyleSheet('https://fonts.googleapis.com/css?family=Ubuntu+Mono&amp;display=swap');
		$document->addStyleSheet(JUri::root().'components/com_annotatex/resources/css/annotate-reports.css');

		$document->addScript(JUri::root().'components/com_annotatex/resources/js/banner.js');
		$document->addScript(JUri::root().'components/com_annotatex/resources/js/selectable.js');
		$document->addScript(JUri::root().'components/com_annotatex/resources/js/switchboard.js');

		// This script should be included after all the others, so keep it here.
		$document->addScript(JUri::root().'components/com_annotatex/resources/js/annotate-reports.js');
	}
}
