# com_annotatex
Joomla! 3.9.6 component for the AnnotateX project.
The AnnotateX project is comprised of three repositories, this one, `tpl_annotatex` that contains the source
code for the template and `annotatex-scripts` containing various backup and extraction scripts.

## Current Installation
**Before asking for credentials to IT, read the whole paragraph.**

AnnotateX is currently installed on `apmup.lnx.warwick.ac.uk/live`.

To access the server you ask for credentials to IT. Your username will be 'u[badge number]'. As for university
policies, you won't be super user in that machine. And that's a real pain.

AnnotateX is developed as Joomla! extension. The Joomla! installation is currently located at
`/var/www/html/live`. All the other folders inside `/var/www/html/` are outdated versions and should be removed,
unfortunately we're not sudoers.

There will probably be a problem with filesystem permissions. All the files in `/var/www/html/live` are currently
owned by the user `u1874286` and group `admin`. While asking for credentials would be a good idea to also ask
to reown all the files.

There's a MySQL server, that can be accessed with:

```
mysql dbjoomla -uannotate_user -pAnnChestXray19
```

The project is developed using the Joomla! framework, written in PHP and can be run on an Apache servers. The
default installation folder would be `/var/www/html`; download Joomla! (or resume a backup) and put all the files
there. To install Joomla! follow the official guide
[https://docs.joomla.org/J3.x:Installing_Joomla](https://docs.joomla.org/J3.x:Installing_Joomla).

## Understanding the code
The repository contains the E/R schema for the database in png format.

There's a really nice tutorial on how to build a new component for Joomla!
[at this page](https://docs.joomla.org/J3.x:Developing_an_MVC_Component/Developing_a_Basic_Component). The tutorial
will be useful to understand code organization. This tutorial is almost all I followed to build the component and
should be sufficient by itself.

## Build
To build the project you need to create a zip with all the relevant source files. For convenience, a `build.sh`
script has been included. To build the project:

```
chmod +x build.sh && ./build.sh
```

The script also adds an empty HTML file in each folder to avoid some common attacks.

## Install
Once you built the project log in to the Joomla! administrator panel and upload the zip file. See the official
Joomla! documentation [https://docs.joomla.org/Installing_an_extension](https://docs.joomla.org/Installing_an_extension)

If something broke really badly and you're trying to install everything from scratch, please upload a MySQL
backup before installing the extension.

## Backup
A daily backup of the MySQL database and a monthly backup of the entire Joomla! installation folder are performed
by a crontab on `ds-amsterdam` under the user `root`. The backups are stored in:

```
/BigData/annotatex/backup
```

To see the crontab type:

```
sudo crontab -l
```

The backup scripts are stored in another repository called `annotatex-scripts`.

## Scripts
The queries used to extract data from the database are saved in `annotatex-scripts` project.

## Tables
The name of the SQL tables used by the componet start with `ax_`. To show all the tables type:

```
SHOW TABLES LIKE 'ax%';
```

Below, a description of each table for reports:

* `ax_annotation`: an annotated sentence (multiple annotated sentences are contained in a submission); the
  table is normalized and only contains the sentence; the labels are contained in the table `ax_annotation_label`;
* `ax_annotation_label`: for each annotated sentence this table contains its labels;
* `ax_assignment`: given a user and a collection of reports, this table contains a record if the user has
  been assigned the collection;
* `ax_assortment`: an assortment is a grouping of labels, as for the document where we defined the labels;
* `ax_collection`: a collection is a grouping of reports; a collection is assigned to a user;
* `ax_collection_report`: this table is the relationship between collections and reports; a record is present in
  this table if the report is part of the collection;
* `ax_label`: this table contains all the labels;
* `ax_report`: this table contains all the reports;
* `ax_submission`: a submission is generated when the user clicks on the submit button; a submission contains
  multiple annotated sentences (`ax_annotation` table) and each sentence can be annotated with multiple labels
  (`ax_annotation_label` table);

Below, a description of each table for xrays:

* `ax_labelled_xray`: this table is actually a misnomer and it should be called `ax_prelabelled_xray`; an xray
  is manually prelabelled by us and put in this table; the user will see the set of prelabels on the top of the
  screen while annotating an xray; an xray can be part of many collections, and if this is the case, the same
  xray may have different prelabels for each collection it is part of;
* `ax_pending_xray`: a user may be unsure about annotating an xray and he may want to postpone the annotation;
  this table contains the xray that have been postponed;
* `ax_xray`: this table contains a set of xrays; an xray can be part of many xray collections;
* `ax_xray_annotation`: an annotation is a single region (square) on the xray; a xray annotation is part of a
  xray submission;
* `ax_xray_assignment`: given a user and a collection of prelabelled xrays, this table contains a record if the
  collection was assigned to the user;
* `ax_xray_collection`: a xray collection is a set of xrays;
* `ax_xray_labelling`: this table is actually a misnomer and it should be called `ax_xray_prelabelling`; an
  xray is prelabelled and then assigned to a user; the prelabelling can contain multiple labels for a single
  xray and this table contains the mapping;
* `ax_xray_submission`: a submission is the act of pressing the submit button; a submission contains multiple
  annotations;

Below a description of the other tables:

* `ax_user_option`: this table contains some user options, such as the default collection;

## Adding a New User
A new user can be added by the Joomla! administrator panel. You can access the administrator panel at
[http://apmup.lnx.warwick.ac.uk/administrator](http://apmup.lnx.warwick.ac.uk/live/administrator).

You can follow the official Joomla! documentation for adding a new user at
[https://docs.joomla.org/Adding_a_new_user](https://docs.joomla.org/Adding_a_new_user).

Remember to add the user to the `Radiologist` group.

## Assigning a Report/X-Ray Collection to a User
A collection can be assigned to a user by simply adding a new record to the table `ax_assignment` or
`ax_xray_assignment`. If this is the first collection for the user, don't forget to also add a record to
the table `ax_user_option`; this table contains the default report and x-ray collection for a given user
and a record must be present.

To assign a report collection to a user:

```
INSERT INTO ax_assignment (user, collection) VALUES (777, 99);
```

You can get the id of a user with:

```
SELECT id, username FROM jmt_users;
```

## Adding or Removing Labels
To add or remove a label you just need to add it to the `ax_label` table. These labels are mainly used in
two places:

* The "annotate report" page;
* The "add label" button in the "annotate xray" page.

If you add a new label you probably want to touch the code of these two pages in order for the new label to
show up. The two files are: `site/views/annotatereports/tmpl/default.php` and
`site/views/annotatexrays/tmpl/default.php`.

## Adding New Reports
Before adding a report it's important to backup the entire database: it's easy to mess things up and although
we have daily backups we don't want to delete the entire day's work of a user.

Reports are normally extracted with Python procedure and Yashin is the one who normally takes care of it.
The reports are given in a CSV format and I normally use Emacs or Notepad++ macros to transform the CSV
into a series of `INSERT` statements.

Don't forget to add the reports to a new collection in the `ax_collection` table.

To add a new report into a new collection:

```
INSERT INTO ax_collection (id, title, description) VALUES (99, 'NEW_COLLECTION', 'Exhaustive description.');
INSERT INTO ax_report (id, collection, content) VALUES ('RJ12345', 99, 'All normal'); -- the collection field is redundant and unused
INSERT INTO ax_collection_report(collection, report) VALUES (99, 'RJ12345');
```

## Adding New X-Rays
It's not really straightforward to add new X-Rays to the application. You need to:

* Create a xray collection (`ax_xray_collection`);
* Add new records to the `ax_xray` table (id, path to the image);
* Copy the images in the correct path under (`/var/www/html/live/media/dicoms`);
* Add a pair [xray, collection] into the table `ax_labelled_xray`;
* Decide a prelabelling and add the relative records into `ax_xray_labelling`;
* Assign the xray collection to a user inserting a record into `ax_xray_assignment`;

There are SQL and Node.js scripts for these steps in the repo `annotatex-scripts`.

An example of the queries you have to write in order to insert a single xray:

```
INSERT INTO ax_xray_collection (id, title, description) VALUES (99, 'MY_COLLECTION', 'This collection was obtained with...');
-- for all the xrays in the collection, do
INSERT INTO ax_xray (id, image_path) VALUES ('RJ1234', '/media/dicoms/RJ1234.dcm');
-- copy the image into /var/www/html/live/media/dicoms/RJ1234.dcm
INSERT INTO ax_labelled_xray (xray, collection) VALUES ('RJ1234', 99);
-- for each prelabel of the current xray, do
INSERT INTO ax_xray_labelling (xray, collection, label) VALUES ('RJ1234', 99, 'pneumothorax');
```

To assign the collection to a user write:

```
INSERT INTO ax_xray_assignment (user, collection) VALUES (777, 99);
```

You can get the id of a user with:

```
SELECT id, username FROM jmt_users;
```
