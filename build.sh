#!/bin/bash

function main() {
        secure && pack && unsecure;
}

function pack() {
        zip -q -r $(pwd | xargs basename) . -x $(basename "$0");
}

function secure() {
        local HTML="<!DOCTYPE html><html><head><title></title></head><body></body></html>";

        find . -type d | while read CWD; do
                if [ ! -e "$CWD/index.html" ]; then
                        echo $HTML > "$CWD/index.html";
                fi;
        done;
}

function unsecure() {
        find . -name index.html -exec rm {} \;;
}

main;
